CFILES := $(shell find src/ -type f -regex ".*\(\.c\|\.cpp\|\.cc\)" -not -path "src/main.cpp" | tr "\n" " ")
HFILES := $(shell find src/ -type f -regex ".*\(\.h\|\.hpp\)" -not -path "src/main.cpp" | tr "\n" " ")
LIB_NAME :=symcalc
MAKEFILES :=bison.mak TeX.mak
TEST_NAME :=symcalc
TEST_FILE :=examples/cli.cpp
TEST_LIBS :=
#`pkg-config gtk+-3.0 --libs` -rdynamic

LINUX_CC = gcc
LINUX_CXX = g++
LINUX_AR = ar

WIN32_CC = i686-w64-mingw32-gcc
WIN32_CXX = i686-w64-mingw32-g++
WIN32_AR = i686-w64-mingw32-ar

WIN64_CC = x86_64-w64-mingw32-gcc
WIN64_CXX = x86_64-w64-mingw32-g++
WIN64_AR = x86_64-w64-mingw32-ar

CFLAGS =-fexceptions
#`pkg-config gtk+-3.0 --cflags` -rdynamic
CXXFLAGS = -std=c++11 -fexceptions
#`pkg-config gtk+-3.0 --cflags` -rdynamic
AR_MODE = -rcs

LINUX_DIR :=linux
WIN32_DIR :=win32
WIN64_DIR :=win64

#INTERNAL : DON NOT CHANGE

obj_target = obj/${2}/$(addsuffix .o,$(basename ${1}))	#$(patsubst %.c,%.o,$(notdir ${1}))
test_target = test/${2}/${1}		#$(patsubst %.c,%.o,$(notdir ${1}))
lib_target = lib/${2}/lib${1}.a
hdr_target = include/${2}/${1}

objlinux.c :=
objlinux.cpp :=
objlinux.cc :=
liblinux.a :=
objwin32.o :=
objwin64.o :=
libwin32.a :=
libwin64.a :=
hdr.h :=
define obj
  $(call obj_target,${1},${2}) : ${1} | obj/${2}/
  obj${2}$(suffix ${1}) += $(call obj_target,${1},${2})
endef

define test
  $(call test_target,${TEST_NAME},${2}) : ${1} lib/${2}/lib${LIB_NAME}.a |test/${2}/
  test${2}$(suffix ${1}) += $(call test_target,${TEST_NAME},${2})
endef

define lib
  $(call lib_target,${1},${2}) : $(foreach src,${CFILES},$(call obj_target,${src},${2})) | lib/${2}/
  lib${2}.a += $(call lib_target,${1},${2})
endef

define hdr
  $(call hdr_target,${1},${2}) : ${1} | include/${2}/
  hdr.h += $(call hdr_target,${1},${2})
endef

define SOURCES
  $(foreach src,${1},$(eval $(call obj,${src},${2})))
endef

define HEADERS
  $(foreach src,${1},$(eval $(call hdr,${src},${2})))
endef

all : linux headers tex cli
#win32 win64

examples: gui cli

gui: linux headers examples/gui.cpp
	$(LINUX_CXX) -o examples/gui examples/gui.cpp $(CXXFLAGS) `pkg-config gtk+-3.0 --cflags` -rdynamic -L./lib/linux -I./include/linux -l${LIB_NAME} ${TEST_LIBS} `pkg-config gtk+-3.0 --libs` -rdynamic

cli: linux headers examples/cli.cpp
	$(LINUX_CXX) -o examples/cli examples/cli.cpp $(CXXFLAGS) -L./lib/linux -I./include/linux -l${LIB_NAME} ${TEST_LIBS}

linux: $(call lib_target,${LIB_NAME},${LINUX_DIR}) $(call test_target,${TEST_NAME},${LINUX_DIR})

win32: $(call lib_target,${LIB_NAME},${WIN32_DIR}) $(call test_target,${TEST_NAME},${WIN32_DIR})

win64: $(call lib_target,${LIB_NAME},${WIN64_DIR}) $(call test_target,${TEST_NAME},${WIN64_DIR})

include $(MAKEFILES)

headers: $(foreach hdr,${HFILES},$(call hdr_target,${hdr},${LINUX_DIR}))\
         $(foreach hdr,${HFILES},$(call hdr_target,${hdr},${WIN32_DIR}))\
         $(foreach hdr,${HFILES},$(call hdr_target,${hdr},${WIN64_DIR}))

$(eval $(call test,${TEST_FILE},${LINUX_DIR}))
$(eval $(call test,${TEST_FILE},${WIN32_DIR}))
$(eval $(call test,${TEST_FILE},${WIN64_DIR}))

$(eval $(call SOURCES,${CFILES},${LINUX_DIR}))
$(eval $(call HEADERS,${HFILES},${LINUX_DIR}))
$(eval $(call lib,${LIB_NAME},${LINUX_DIR}))

$(eval $(call SOURCES,${CFILES},${WIN32_DIR}))
$(eval $(call HEADERS,${HFILES},${WIN32_DIR}))
$(eval $(call lib,${LIB_NAME},${WIN32_DIR}))

$(eval $(call SOURCES,${CFILES},${WIN64_DIR}))
$(eval $(call HEADERS,${HFILES},${WIN64_DIR}))
$(eval $(call lib,${LIB_NAME},${WIN64_DIR}))

${objlinux.c} : % :
	@echo Compiling $@
	@$(LINUX_CC) -c -o $@ $< $(CFLAGS) -I./include/linux/

${objlinux.cpp} : % :
	@echo Compiling $@
	@$(LINUX_CXX) -c -o $@ $< $(CXXFLAGS) -I./include/linux/

${objlinux.cc} : % :
	@echo Compiling $@
	@$(LINUX_CXX) -c -o $@ $< $(CXXFLAGS) -I./include/linux/

${liblinux.a} : % :
	@echo Creating library $@
	$(LINUX_AR) $(AR_MODE) $@ $^

${hdr.h}: % :
	@echo Copying header $@
	@cp $< $@

${objwin32.c} : % :
	@echo Compiling $@
	@$(WIN32_CC) -c -o $@ $< $(CFLAGS) -I./include/win32/

${objwin32.cpp} : % :
	@echo Compiling $@
	@$(WIN32_CXX) -c -o $@ $< $(CXXFLAGS) -I./include/win32/

${objwin32.cc} : % :
	@echo Compiling $@
	@$(WIN32_CXX) -c -o $@ $< $(CXXFLAGS) -I./include/win32/

${libwin32.a} : % :
	@echo Creating library $@
	$(WIN32_AR) $(AR_MODE) $@ $^

${objwin64.c} : % :
	@echo Compiling $@
	@$(WIN64_CC) -c -o $@ $< $(CFLAGS) -I./include/win64/

${objwin64.cpp} : % :
	@echo Compiling $@
	@$(WIN64_CXX) -c -o $@ $< $(CXXFLAGS) -I./include/win64/

${objwin64.cc} : % :
	@echo Compiling $@
	@$(WIN64_CXX) -c -o $@ $< $(CXXFLAGS) -I./include/win64/

${libwin64.a} : % :
	@echo Creating library $@
	$(WIN64_AR) $(AR_MODE) $@ $^

${testlinux.c} : % :
	@echo Building $@
	$(LINUX_CC) -o $@ $< $(CFLAGS) -L./lib/linux -I./include/linux -l${LIB_NAME} ${TEST_LIBS}

${testlinux.cpp} : % :
	@echo Building $@
	$(LINUX_CXX) -o $@ $< $(CXXFLAGS) -L./lib/linux -I./include/linux -l${LIB_NAME} ${TEST_LIBS}

${testlinux.cc} : % :
	@echo Building $@
	$(LINUX_CXX) -o $@ $< $(CXXFLAGS) -L./lib/linux -I./include/linux -l${LIB_NAME} ${TEST_LIBS}

${testwin32.c} : % :
	@echo Building $@
	$(WIN32_CC) -o $@ $< $(CFLAGS) -L./lib/win32 -I./include/win32 -l${LIB_NAME} ${TEST_LIBS}

${testwin32.cpp} : % :
	@echo Building $@
	$(WIN32_CXX) -o $@ $< $(CXXFLAGS) -L./lib/win32 -I./include/win32 -l${LIB_NAME} ${TEST_LIBS}

${testwin32.cc} : % :
	@echo Building $@
	$(WIN32_CXX) -o $@ $< $(CXXFLAGS) -L./lib/win32 -I./include/win32 -l${LIB_NAME} ${TEST_LIBS}

${testwin64.c} : % :
	@echo Building $@
	$(WIN64_CC) -o $@ $< $(CFLAGS) -L./lib/win64 -I./include/win64 -l${LIB_NAME} ${TEST_LIBS}

${testwin64.cpp} : % :
	@echo Building $@
	$(WIN64_CXX) -o $@ $< $(CXXFLAGS) -L./lib/win64 -I./include/win64 -l${LIB_NAME} ${TEST_LIBS}

${testwin64.cc} : % :
	@echo Building $@
	$(WIN64_CXX) -o $@ $< $(CXXFLAGS) -L./lib/win64 -I./include/win64 -l${LIB_NAME} ${TEST_LIBS}

include/win32/: include/
	@mkdir -p include/win32

include/win64/: include/
	@mkdir -p include/win64

include/linux/: include/
	@mkdir -p include/linux

include/:
	@mkdir -p include

lib/linux/: lib/
	@mkdir -p lib/linux

lib/win32/: lib/
	@mkdir -p lib/win32

lib/win64/: lib/
	@mkdir -p lib/win64

lib/:
	@mkdir -p lib

obj/win64/: obj/
	@mkdir -p obj/win64
	@find src/ -type d -exec mkdir -p -- obj/win64/{} \;

obj/win32/: obj/
	@mkdir -p obj/win32
	@find src/ -type d -exec mkdir -p -- obj/win32/{} \;

obj/linux/: obj/
	@mkdir -p obj/linux
	@echo copying folder structure
	@find src/ -type d -exec mkdir -p -- obj/linux/{} \;

obj/:
	@mkdir -p obj

test/linux/:
	@mkdir -p test/linux

test/win32/:
	@mkdir -p test/win32

test/win64/:
	@mkdir -p test/win64

test/:
	@mkdir -p test

.PHONY: all

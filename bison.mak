src/impl_lexer.c: src/implementation.l
	lex -o src/impl_lexer.c src/implementation.l

src/impl_parser.c src/impl_parser.h: src/implementation.y
	yacc -d -v src/implementation.y -o src/impl_parser.c

src/sym_lexer.c: src/language.l
	lex -o src/sym_lexer.c src/language.l

src/sym_parser.c src/sym_parser.h: src/language.y
	yacc -d -v src/language.y -o src/sym_parser.c


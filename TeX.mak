
tex: TeX/builtin.rules linux TeX/article.pdf

TeX/article.pdf: TeX/article.tex TeX/symcalc.sty
	cd TeX && pdflatex -shell-escape article.tex && cd ..

TeX/builtin.rules: builtin.rules
	cp builtin.rules TeX/

doc/bultin.rules: builtin.rules
	cp builtin.rules doc/

doc/bultin.rules.impl: builtin.rules.impl
	cp builtin.rules.impl doc/

TeX/symcalc.sty: TeX/symcalc.sty.template
	@echo "Creating symcalc.sty"
	python3 templates.py TeX/symcalc.sty.template TeX/symcalc.sty "path=$(realpath ./test/linux/symcalc)"
	python3 templates.py TeX/symcalc.sty.template doc/symcalc.sty "path=$(realpath ./test/linux/symcalc)"
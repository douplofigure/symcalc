import sys

inf = open(sys.argv[1])
#of = open(sys.argv[2])

config = {}

for arg in sys.argv[3:]:
    ls = arg.split("=")
    config[ls[0].strip()] = ls[1].strip()

print(config)

inData = inf.read()
string = ""
i = 0
while i < len(inData):
    c = inData[i]
    if c == "%":
        foundMatch = False
        j = 1
        while not foundMatch:
            try:
                val = config[inData[i+1:i+j]]
                string += val
                i+=j-1
                foundMatch = True
            except:
                foundMatch = False
                j += 1
    else:
        string += c
    i += 1

inf.close()
of = open(sys.argv[2], "w")
of.write(string)
of.close()

#ifndef TEXTINTERFACE_H
#define TEXTINTERFACE_H

#include <gtk/gtk.h>
#include <map>
#include <sstream>
#include <queue>
#include <vector>

#include <stdio.h>
#include <thread>

class TextInterface
{
    public:
        TextInterface(GtkTextView * view);
        virtual ~TextInterface();

        static TextInterface * getFromWidget(GtkWidget * widget);
        static TextInterface * getActiveInterface();

        bool keyEvent(GdkEventKey * key);
        //std::queue<char>& getQueue();

        void pullStreamContents(std::stringstream& stream);
        void clearCommandBuffer();
        void commandFinished();
        bool updateText();

    protected:

    private:

        int prevCommandIndex;
        int cmdCount;
        bool commandRunning;
        bool hasPromt;
        std::thread commandThread;
        std::vector<std::string> prevCommands;

        GtkTextView * view;

        GtkTextBuffer * buffer;
        GtkTextMark * commandStart;

        std::string textToAdd;

        std::stringstream outStream;
        std::stringstream errStream;

        static std::map<GtkWidget *, TextInterface *> interfaces;
        static TextInterface * activeInterface;

};

extern "C" {
    extern void parseFile(FILE * file);
    gboolean on_text_edit(GtkWidget * widget, GdkEventKey * key, gpointer data);
    gboolean on_import_file_btn(GtkWidget * widget, gpointer data);

}

#endif // TEXTINTERFACE_H

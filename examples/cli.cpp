#include <iostream>
#include <stdio.h>
#include <fstream>

#include <signal.h>

#include <src/symcalc.h>

char * correctConstants(char * str) {

    int i = 0;
    while (str[i]) {

        if (str[i] == '$')
            str[i] = '#';
        if (str[i] == ';')
            str[i] = '#';

        i++;
    }

    return str;

}

static int runTerminal;

void exitHandler(int i) {

    runTerminal = 0;

}

int main(int argc, char ** argv) {

    setlocale(LC_NUMERIC, "en_US");

    bool loadBuiltin = 1;
    bool invokeTeX = 0;
    bool printHeader = 1;
    bool isSingleCommand = 0;

    std::vector<std::string> filesToLoad(0);
    std::ofstream outLocation;

    char lastOption = 0;
    for (int i = 1; i < argc; ++i) {

        if (argv[i][0] == '-') {
            lastOption = argv[i][1];
            if (lastOption=='n') {
                lastOption = 0;
                loadBuiltin = 0;
            } else if (lastOption == 'i') {
                lastOption = 0;
                printHeader = 0;
            }
            continue;
        }

        if (lastOption) {

            switch(lastOption) {

                case 'o':
                    std::cout << "To use this output with TeX run" << std::endl;
                    std::cout << "\tpdflatex " << std::string(argv[i]) << "/output.tex" << std::endl;
                    outLocation = std::ofstream(std::string(argv[i]).append("/output.tex"));
                    ioctl::setOutStream(outLocation);
                    break;

                case 'c':
                    isSingleCommand = 1;
                    if (filesToLoad.size() == 0) {
                        filesToLoad = std::vector<std::string>(1);
                    }
                    filesToLoad[0] = std::string(correctConstants(argv[i]));
                    break;

            }

            lastOption = 0;

            continue;
        }
        filesToLoad.push_back(std::string(argv[i]));

    }

    symcalc::init(loadBuiltin);

    if (argc < 2) {

        symcalc::setOutputTeX(false);

        std::string cmd;
        int cmdNum = 0;
        runTerminal = 1;

        struct sigaction sigIntHandler;
        sigIntHandler.sa_handler = exitHandler;
        sigemptyset(&sigIntHandler.sa_mask);
        sigIntHandler.sa_flags = 0;

        sigaction(SIGINT, &sigIntHandler, NULL);

        while (runTerminal) {

            std::cout << "[" << cmdNum << "]>>> ";
            std::getline(std::cin, cmd);

            symcalc::executeCommand(cmd);
            cmdNum++;

        }
        std::cout << "Cleaning up" << std::endl;
        symcalc::cleanup();
        return 0;
    }

    symcalc::setOutputTeX(true);

    if (printHeader) {
        ioctl::out() << "\\documentclass[11pt]{article}" << std::endl;

        ioctl::out() << "\\usepackage{amsmath}" << std::endl;
        ioctl::out() << "\\usepackage{listings}" << std::endl;

        ioctl::out() << "\\usepackage{amsfonts}" << std::endl;
        ioctl::out() << "\\usepackage{amssymb}" << std::endl;
        ioctl::out() << "\\usepackage{mathtools}" << std::endl;
        ioctl::out() << "\\usepackage{float}" << std::endl;
        ioctl::out() << "\\usepackage{graphicx}" << std::endl;
        ioctl::out() << "\\usepackage{epstopdf}" << std::endl;
        ioctl::out() << "\\usepackage[usenames]{color}" << std::endl;
        ioctl::out() << "\\usepackage[miktex]{gnuplottex}" << std::endl;

        ioctl::out() << "\\makeatletter" << std::endl;
        ioctl::out() << "\\renewcommand*\\env@matrix[1][\\arraystretch]{%\n\\edef\\arraystretch{#1}%\n\\hskip -\\arraycolsep\n\\let\\@ifnextchar\\new@ifnextchar\n  \\array{*\\c@MaxMatrixCols c}}\n\\makeatother" << std::endl;

        ioctl::out() << "\\begin{document}" << std::endl;
    }

    if (!isSingleCommand) {
        for (int i = 0; i < filesToLoad.size(); ++i) {

            symcalc::loadFile(filesToLoad[i]);

        }
    } else {
        symcalc::executeCommand(filesToLoad[0]);
    }

    if (printHeader)
        ioctl::out() << "\\end{document}" << std::endl;

    symcalc::cleanup();
    return 0;
}

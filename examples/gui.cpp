#include "textinterface.h"

#include <iostream>
#include <src/symcalc.h>

std::map<GtkWidget *, TextInterface *> TextInterface::interfaces;
TextInterface * TextInterface::activeInterface;

std::string entryPromt(">>> ");

extern "C" {
    gboolean on_window_destroy(GtkWidget * window, gpointer data) {

        gtk_widget_destroy(window);
        gtk_main_quit();
        return TRUE;

    }
}

TextInterface::TextInterface(GtkTextView * view) {

    this->view = view;
    this->buffer = gtk_text_view_get_buffer(view);
    GtkTextIter pos;
    gtk_text_buffer_get_start_iter(buffer, &pos);
    this->commandStart = gtk_text_buffer_create_mark(buffer, nullptr, &pos, true);

    cmdCount = 0;

    GtkTextIter cursor;
    gtk_text_buffer_get_end_iter(buffer, &cursor);
    std::string promt = std::string("[").append(std::to_string(cmdCount)).append("]").append(entryPromt);

    gtk_text_buffer_insert(buffer, &cursor, promt.c_str(), promt.length());
    gtk_text_buffer_move_mark(buffer, commandStart, &cursor);
    gtk_text_buffer_place_cursor(buffer, &cursor);

    interfaces[GTK_WIDGET(view)] = this;
    prevCommandIndex = -1;
    activeInterface = this;
    commandRunning = false;
    hasPromt = true;

    ioctl::setErrStream(errStream);
    ioctl::setOutStream(outStream);

    symcalc::setPrintCallback([&] () -> void {
        this->pullStreamContents(outStream);
    });

}

TextInterface::~TextInterface()
{
    //dtor
}

/*std::queue<char>& TextInterface::getQueue() {
    return commandBuffer;
}*/

void TextInterface::pullStreamContents(std::stringstream& stream) {

    std::string data = stream.str();
    stream.str(std::string());
    textToAdd.append(data);

}

void TextInterface::clearCommandBuffer() {
    symcalc::clearBuffer();
}

TextInterface * TextInterface::getActiveInterface() {
    return activeInterface;
}

gboolean update_text(gpointer data) {
    TextInterface * interface = (TextInterface *) data;
    return interface->updateText();
}

void TextInterface::commandFinished() {

    commandRunning = false;

}

bool TextInterface::updateText() {

    bool cmdStatus = commandRunning;

    GtkTextIter command;
    gtk_text_buffer_get_iter_at_mark(buffer, &command, this->commandStart);
    GtkTextIter cursor;
    gtk_text_buffer_get_end_iter(buffer, &cursor);

    gtk_text_buffer_get_end_iter(buffer, &cursor);
    gtk_text_buffer_insert(buffer, &cursor, textToAdd.c_str(), textToAdd.length());
    textToAdd = std::string();
    if (!cmdStatus) {

        std::string promt = std::string("[").append(std::to_string(++cmdCount)).append("]").append(entryPromt);

        gtk_text_buffer_insert(buffer, &cursor, promt.c_str(), promt.length());
        hasPromt = true;
    }
    gtk_text_buffer_get_end_iter(buffer, &cursor);
    gtk_text_buffer_move_mark(buffer, commandStart, &cursor);
    gtk_text_buffer_place_cursor(buffer, &cursor);
    gtk_text_view_scroll_mark_onscreen(this->view, commandStart);

    return cmdStatus;
}

void executeCommand(TextInterface * interface) {
    try {
        symcalc::executeBuffer();
    } catch (std::runtime_error e) {
        std::stringstream sstr;
        sstr << e.what() << std::endl;
        TextInterface::getActiveInterface()->pullStreamContents(sstr);
    }
    interface->commandFinished();
}

bool TextInterface::keyEvent(GdkEventKey * key) {

    if (commandRunning) return true;

    GtkTextIter command;
    gtk_text_buffer_get_iter_at_mark(buffer, &command, this->commandStart);
    GtkTextIter cursor;
    gtk_text_buffer_get_end_iter(buffer, &cursor);

    std::string commandStr;

    if (key->type == GDK_KEY_PRESS) {

        switch (key->keyval) {

            case GDK_KEY_Return:
                commandStr = std::string(gtk_text_buffer_get_slice(buffer, &command, &cursor, false));
                symcalc::pushCommand(commandStr);
                activeInterface = this;
                gtk_text_buffer_insert(buffer, &cursor, "\n", 1);
                commandRunning = true;
                hasPromt = false;
                g_idle_add(update_text, this);
                if (commandThread.joinable())
                    this->commandThread.join();
                commandThread = std::thread(executeCommand, this);
                prevCommandIndex = -1;
                this->prevCommands.insert(prevCommands.begin(), commandStr);
                return TRUE;

            case GDK_KEY_Up:

                if (this->prevCommandIndex >= (int) this->prevCommands.size() - 1)
                    return TRUE;
                else {
                    prevCommandIndex++;

                    gtk_text_buffer_delete(buffer, &command, &cursor);
                    gtk_text_buffer_insert(buffer, &command, prevCommands[prevCommandIndex].c_str(), prevCommands[prevCommandIndex].length());

                    return TRUE;
                }

            case GDK_KEY_Down:

                if (this->prevCommandIndex < 1 || !prevCommands.size()) {

                    if (prevCommandIndex == 0) {
                        prevCommandIndex--;
                        gtk_text_buffer_delete(buffer, &command, &cursor);
                    }

                    return TRUE;
                } else {
                    prevCommandIndex--;

                    gtk_text_buffer_delete(buffer, &command, &cursor);
                    gtk_text_buffer_insert(buffer, &command, prevCommands[prevCommandIndex].c_str(), prevCommands[prevCommandIndex].length());

                    return TRUE;
                }

            case GDK_KEY_Left:
            case GDK_KEY_BackSpace:
                gtk_text_buffer_get_iter_at_mark(buffer, &cursor, gtk_text_buffer_get_insert(buffer));
                return gtk_text_iter_equal(&cursor, &command);

        }


    }

    gtk_text_buffer_get_end_iter(buffer, &cursor);

    return FALSE;

}

TextInterface * TextInterface::getFromWidget(GtkWidget * w) {

    if (interfaces.find(w) == interfaces.end()) return nullptr;

    return interfaces[w];

}

gboolean on_text_edit(GtkWidget * widget, GdkEventKey * key, gpointer data) {
    return TextInterface::getFromWidget(widget)->keyEvent(key);
}


gboolean on_import_file_btn(GtkWidget * widget, gpointer data) {

    GtkWidget * dialog;
    GtkFileChooserAction action = GTK_FILE_CHOOSER_ACTION_OPEN;

    int res;

    dialog = gtk_file_chooser_dialog_new("Open File", GTK_WINDOW(widget), action, "Cancel", GTK_RESPONSE_CANCEL, "Open", GTK_RESPONSE_ACCEPT, NULL);
    res = gtk_dialog_run(GTK_DIALOG(dialog));

    if (res == GTK_RESPONSE_ACCEPT) {
        char * fname;
        GtkFileChooser * chooser = GTK_FILE_CHOOSER(dialog);
        fname = gtk_file_chooser_get_filename(chooser);

        FILE * f = fopen(fname, "r");
        if (!f) return FALSE;
        fclose(f);

        if (TextInterface::getActiveInterface())
            g_idle_add(update_text, TextInterface::getActiveInterface());
        symcalc::loadFile(std::string(fname));


        g_free(fname);

    }

    gtk_widget_destroy(dialog);

}


int main(int argc, char ** argv) {

    setlocale(LC_NUMERIC, "en_US");
    symcalc::init(true);
    symcalc::setOutputTeX(false);

    gtk_init(&argc, &argv);
    GtkBuilder * builder = gtk_builder_new();
    if (!gtk_builder_add_from_file(builder, "window.glade", NULL)) {
        throw std::runtime_error("cannot create gtk builder from file");
    }

    ioctl::NullBuffer nullBuffer;
    std::ostream nullStream(&nullBuffer);
    ioctl::setOutStream(nullStream);
    ioctl::setErrStream(nullStream);

    GtkWidget * window = GTK_WIDGET(gtk_builder_get_object(builder, "window"));
    gtk_builder_connect_signals(builder, NULL);

    TextInterface * textInterface = new TextInterface(GTK_TEXT_VIEW(gtk_builder_get_object(builder, "editor")));

    gtk_widget_show_all(window);
    gtk_main();

    std::cout << "Programm shutting down" << std::endl;

    symcalc::cleanup();
    return 0;

}

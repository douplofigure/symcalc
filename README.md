# Symcalc

## Description

A programme that tries to help you in your maths problems. This tool will try to simplify your mathematical expressions using a set of rules you can define. It uses a set of operators, functions and symbols to represent mathematical equations and then applies rules to these representations in order to simplify them as much as it can. Then it may try to give you a numeric value for the expression.

The rules for most common operators and some common functions are predefined, but they need not be loaded.

## Building

Before any target can be build, please install lex and yacc (flex and bison may work as well) as they are used to generate the parsers for the languages.

### For Linux (and Mac-OS)

#### Command-line

Running

    make
will build the library and create an executable providing a command-line-interface. This executable is located at ./examples/cli. This command will also setup the style file for TeX and adjust the dependencies inside it.

#### With gui

To enable the gui, you will need to have the gtk-3 libraries installed with your package manager (The build process will use pkg-config to locate them). Once the libraries are installed running

    make gui
will produce an executable located at ./examples/gui. When executed this will open a gui with an entry prompt.

Please note that the files *builtin.rules* and *builtin.rules.impl* must be located in the directory from wich you start the application.

### Cross compiling for Windows

You should be able to compile the library with mingw32 by running

    make win32
    
or 

    make win64
    
depending on your target platform's support of 32 or 64 bit respectively. This will generate the library (.a file) and create an executable (no ending) at *./test/win32/symcalc* or *./test/win64/symcalc/* providing a cli.

### Windows natively

I do not know how to compile code under Windows and therefor am not able to provide any targets in the makefile. All files that need to be compiled and packaged for the library are located inside *./src/* the other sources for command-line interface and gui are located inside *examples*.

#ifndef VECTOR_H
#define VECTOR_H

#include "expression.h"


class Vector : public Expression
{
    public:
        Vector(std::vector<std::shared_ptr<Expression>> coords);
        Vector(std::vector<Expression *> coords);
        virtual ~Vector();

        std::shared_ptr<Expression> simplfy(std::vector<Rule*> rules);
        std::string format();
        int getComplexityScore();
        bool canCompute(std::map<std::string, ComputeResult>& varValues);
        ComputeResult getValue(std::map<std::string, ComputeResult>& varValues);

        void writeToStream(std::ostream& stream);
        Expression::Type getType();

        int getDimension();

    protected:

    private:

};

#endif // VECTOR_H

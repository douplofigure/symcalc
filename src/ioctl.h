#ifndef IOCTL_H_INCLUDED
#define IOCTL_H_INCLUDED

#include <iostream>
#include <streambuf>

namespace ioctl {

    class NullBuffer : public std::streambuf {
        public:
          int overflow(int c) { return c; }
    };

    std::ostream& out();
    std::ostream& err();

    bool cli();
    const char * wd();

    void setOutStream(std::ostream& stream);
    void setErrStream(std::ostream& stream);

};

#endif // IOCTL_H_INCLUDED

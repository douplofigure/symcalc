#ifndef IMPLEMENTATION_H
#define IMPLEMENTATION_H

#ifdef __cplusplus
extern "C" {

#endif // __cplusplus

typedef struct input_provider_t InputProvider;

void implSetInput(InputProvider * func);

int implInput(InputProvider * inputProvider, char * buffer, int max_size);

#ifdef __cplusplus
}
#include <string>
#include <map>
#include <functional>
#include <vector>

#include "implfunction.h"


namespace Implementation {

class Script {

    public:
        Script();
        virtual ~Script();

        static Script * load(std::string data);
        static Script * load(std::istream& data);

        void addFunction(std::string name, Implementation::Function * f);

        Implementation::Function * getFunction(std::string name){
            if (this->functions.find(name) == this->functions.end()) {
                return nullptr;
            }
            return this->functions[name];
        }

    private:
        std::map<std::string, Implementation::Function *> functions;

};

}

#endif // __cplusplus

#endif // IMPLEMENTATION_H

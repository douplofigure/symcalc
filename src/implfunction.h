#ifndef IMPL_FUNCTION_H
#define IMPL_FUNCTION_H

#ifndef DEFAULT_FUNC_TYPE
#define DEFAULT_FUNC_TYPE double
#endif //DEFAULT_FUNC_TYPE

#include <vector>
#include <map>
#include <iostream>

#include "computeresult.h"

namespace Implementation {

typedef enum operation_type_e {

    OP_NONE,
    OP_ADD,
    OP_SUB,
    OP_MUL,
    OP_DIV,
    OP_MOD,
    OP_JMP,
    OP_J0,
    OP_MOV,
    OP_POW,
    OP_CMP_L,
    OP_CMP_G,
    OP_CMP_E,
    OP_CMP_LE,
    OP_CMP_GE

} OperationType;

typedef struct operation_t {

    OperationType type;
    int s1;
    int s2;
    int dest;

} Operation;

}

std::ostream& operator<<(std::ostream& stream, Implementation::Operation op);

namespace Implementation {

template <typename T> int operationExecute(Operation& op, std::vector<T>& data, int pos) {

    switch (op.type) {

        case OP_ADD:
            data[op.dest] = data[op.s1] + data[op.s2];
            break;

        case OP_SUB:
            data[op.dest] = data[op.s1] - data[op.s2];
            break;

        case OP_MUL:
            data[op.dest] = data[op.s1] * data[op.s2];
            break;

        case OP_DIV:
            data[op.dest] = data[op.s1] / data[op.s2];
            break;

        case OP_JMP:
            return op.dest;
            break;

        case OP_J0:
            return (data[op.s1] == (T) 0) ? op.dest : pos+1;

        case OP_MOV:
            data[op.dest] = data[op.s1];
            break;

        case OP_POW:
            data[op.dest] = pow(data[op.s1], data[op.s2]);
            break;

        case OP_CMP_E:
            data[op.dest] = (data[op.s1] == data[op.s2]);
            break;

        case OP_CMP_LE:
            data[op.dest] = (data[op.s1] <= data[op.s2]);
            break;

        case OP_CMP_GE:
            data[op.dest] = (data[op.s1] >= data[op.s2]);
            break;

        case OP_CMP_L:
            data[op.dest] = (data[op.s1] < data[op.s2]);
            break;

        case OP_CMP_G:
            data[op.dest] = (data[op.s1] > data[op.s2]);
            break;

        default:
            break;

    }

    return pos+1;
}

class Function {

    public:
        Function();
        Function(int argc);
        virtual ~Function();

        void setInstructions(std::vector<Operation>& ops);

        template <typename T> T operator() (std::vector<T>& args) {
            args.resize(neededSpace);

            for (int i = 0; i < operations.size();) {
                i = operationExecute<T>(this->operations[i], args, i);
            }

            return args[0];
        }

        int getArgumentCount();

    private:

        int argumentCount;
        int neededSpace;
        std::vector<Operation> operations;


};

}

#endif // FUNCTION_H

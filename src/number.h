#ifndef NUMBER_H
#define NUMBER_H

#include <ostream>
#include <string>
#include <memory>

class Expression;

class Number
{
    public:

        typedef enum number_type_e {
            NUM_NONE,
            NUM_N,
            NUM_Z,
            NUM_Q,
            //NUM_ROOT,
            NUM_R,
            NUM_C,
            NUM_C_EULER
        } NumberType;

        Number();
        Number(unsigned long ul);
        Number(int i);
        Number(long l);
        Number(int p, int q);
        Number(long p, long q);
        Number(double l);
        Number(double p, double q);
        Number(NumberType t, ...);
        virtual ~Number();

        NumberType getType();
        void toStream(std::ostream& stream);
        std::string toTeXFormat();
        void toType(NumberType destType);

        std::shared_ptr<Expression> asExpression();



        Number operator+(Number n);
        Number operator-(Number n);
        Number operator*(Number n);
        Number operator/(Number n);

        operator int ();
        operator double ();
        operator bool ();

        bool operator== (long i);
        bool operator== (Number n);
        bool operator< (Number n);
        bool operator> (Number n);
        bool operator<= (Number n);
        bool operator>= (Number n);

        bool isNegative();

        Number inverse();

        static Number fromString(std::string str);

        static Number power(Number x, Number y);

    protected:

    private:

        void simplfyFraction();

        NumberType type;

        union NumberValue {
            unsigned long n;
            struct FracValue {
                long p;
                long q;
            } frac;
            struct ComplexValue {
                double real;
                double img;
            } comp;
        } val;


};

Number pow(Number x, Number y);

std::ostream& operator<<(std::ostream& stream, Number num);

#endif // NUMBER_H

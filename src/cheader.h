#ifndef CHEADER_H_INCLUDED
#define CHEADER_H_INCLUDED

typedef enum elem_type_e {

    ELEM_OP_INLINE,
    ELEM_OP,
    ELEM_SYMBOL,
    ELEM_FUNCTION

} ELEMENT_TYPE;

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

void * createExpressionInline(void * op, ...);
void * createExpressionSymbol(char * symbol);
void * createExpression(void * op, void * expList);
void * createExpressionFunc(char * fn, void * expList);
void * createExpressionPar(void * exp);
void * createExpressionInt(long int val);
void * createExpressionDouble(double val);
void * expListCreate(int c, ...);
void expListAdd(void * ls, int c, ...);

void * createRule(void * ls);
void * createWritingRule(void * exp1, void * exp2);
void ruleAddCondition(void * r, void * exp);

void expPrint(void * exp);

void expressionSimplify(void * exp);
void expressionExpand(void * exp);

void * declareFunction(char * name, void * expList);
void * declareFunctionSets(char * name, void * s1, void * s2);
void setFunctionExpression(void * func, void * varLs, void * exp);
void setFunctionExpressionOrValue(void * func, void * expList, void * exp);
void functionSetExpression(void * func, void * exp);
void functionSetValue(void * func, void * expList, void * val);

void * createVectorExpression(void * expList);
void * setApplyOperator(void * op, void * left, void * right);

//void declareFunction(char * name, void * varExp, void * exp);
void * setCreateList(void * expList);
void * setCreateCondition(void * left, void * right);
void * numsetCreate(char * symbol);

/// Returns element type and puts pointer to corresponding element in data
int getElementType(char * str, void ** data);

void * declareOperator(char * symbol, int opCount, int prec, int complexity, int isInline);
void operatorSetFormat(void * op, char * str);
void symbolSetFormat(char * op, char * str);
void operatorAddImplementation(char * symbol, char * funcName);

void expressionPlot(char * s, void * l, void * h, void * e);
void expressionPlotList(char * s, void * l, void * h, void * e);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // CHEADER_H_INCLUDED

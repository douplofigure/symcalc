#ifndef AST_H_INCLUDED
#define AST_H_INCLUDED

typedef struct ast_t AST;

typedef enum ast_op_type {

    AST_OP_NONE = 0,
    AST_OP_SYMBOL = 1,

    AST_OP_CALL = 256,
    AST_FUNC_DEF,
    AST_OP_LEQ,
    AST_OP_GEQ,
    AST_OP_EQ

} AST_OP_TYPE;

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

AST * astCreate(AST_OP_TYPE type, ...);
void astDelete(AST * ast);
AST * astAddChildren(AST * ast, int cCount, ...);

AST_OP_TYPE astGetType(AST * ast);
AST ** astGetChildren(AST * ast, int * cCount);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // AST_H_INCLUDED

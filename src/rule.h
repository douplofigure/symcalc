#ifndef RULE_H
#define RULE_H

#include <vector>
#include <memory>
#include <map>

class Expression;

class Rule {
    public:
        Rule(std::vector<std::shared_ptr<Expression>> equivalent);
        Rule(std::vector<Expression *> equivalent);
        Rule(std::vector<std::shared_ptr<Expression>> equivalent, bool b);
        virtual ~Rule();

        //Returns true if rule can be applied.
        virtual bool matches(std::shared_ptr<Expression> exp);
        virtual bool matches(Expression * exp);

        virtual bool conditionsMet(std::map<std::string, Expression *>& correspondance);
        virtual bool conditionsMet(std::map<std::string, std::shared_ptr<Expression>>& correspondance);
        void setCondition(std::shared_ptr<Expression> exp);

        virtual std::vector<std::shared_ptr<Expression>> applyAll(std::shared_ptr<Expression> exp);
        virtual std::vector<std::shared_ptr<Expression>> applyAll(Expression * exp);

        static std::vector<Rule *> rules;
        static void deleteAll();

    protected:

        Rule();

    private:

        std::vector<std::shared_ptr<Expression>> equivExpr;
        std::shared_ptr<Expression> condition;

};

class WritingRule : public Rule {

    public:
        WritingRule(std::shared_ptr<Expression> left, std::shared_ptr<Expression> right);
        WritingRule(Expression * left, Expression * right);
        virtual ~WritingRule();

        bool matches(std::shared_ptr<Expression> exp);
        bool matches(Expression * exp);
        std::vector<std::shared_ptr<Expression>> applyAll(std::shared_ptr<Expression> exp);
        std::vector<std::shared_ptr<Expression>> applyAll(Expression * exp);

    private:
        std::shared_ptr<Expression> left;
        std::shared_ptr<Expression> right;

};

#endif // RULE_H

%{

extern int getLexerInput(char * buffer, int size);

#define YY_INPUT(buf, result, max_size) {result = getLexerInput(buf, max_size);}

#include "cheader.h"
#include "locale.h"
int processLexeme(char * str);
#include "sym_parser.h"

int line = 1;

%}

%option prefix="sym"

%%

"operator" 				{return KW_OPERATOR;}
"rule"					{return KW_RULE;}
"simplify"				{return KW_SIMPLIFY;}
"expand"				{return KW_EXPAND;}
"numset"				{return KW_NUMSET;}
"TeX_format"			{return KW_TEX_FORMAT;}
"let"                   {return KW_LET;}
"plot"                  {return KW_PLOT;}
"->"                    {return OP_TO;}
[0-9]+\.[0-9]+				{setlocale(LC_NUMERIC, "en_US");symlval.d = strtod(symtext, NULL); return CONST_FLOAT;}
\-[0-9]+\.[0-9]+			{setlocale(LC_NUMERIC, "en_US");symlval.d = atof(symtext); return CONST_FLOAT;}
[0-9]+\.[0-9]+"e"[0-9]+		{setlocale(LC_NUMERIC, "en_US");symlval.d = atof(symtext); return CONST_FLOAT;}
\-[0-9]+\.[0-9]+"e"[0-9]+	{setlocale(LC_NUMERIC, "en_US");symlval.d = atof(symtext); return CONST_FLOAT;}
[0-9]+"e"[0-9]+				{setlocale(LC_NUMERIC, "en_US");symlval.d = atof(symtext); return CONST_FLOAT;}
\-[0-9]+"e"[0-9]+			{setlocale(LC_NUMERIC, "en_US");symlval.d = atof(symtext); return CONST_FLOAT;}
[0-9]+					    {symlval.i = atol(symtext); return CONST_INT;}
\-[0-9]+				    {symlval.i = atol(symtext); return CONST_INT;}
"true"					{symlval.i = 1; return CONST_BOOL;}
"false"					{symlval.i = 0; return CONST_BOOL;}
"IF"                    {return KW_IF;}
"conditional"           {return KW_CONDITIONAL;}
":="					{return OP_DEF;}
[:\,\;\{\}\(\)\[\]\_]					{return *symtext;}
\".*\"					{symlval.symbol = strdup(symtext); return CONST_STR;}
[^\t\n\r\f ]			{return processLexeme(symtext);}
\#[0-9]+				{return processLexeme(symtext);}
\#\-[0-9]+				{return processLexeme(symtext);}
\#[0-9]+\.[0-9]+				{return processLexeme(symtext);}
\#\-[0-9]+\.[0-9]+				{return processLexeme(symtext);}
\#[0-9]+\.[0-9]+"e"[0-9]+				{return processLexeme(symtext);}
\#\-[0-9]+\.[0-9]+"e"[0-9]+				{return processLexeme(symtext);}
\#[0-9]+"e"[0-9]+				{return processLexeme(symtext);}
\#\-[0-9]+"e"[0-9]+				{return processLexeme(symtext);}
\#[a-zA-Z]+				{return processLexeme(symtext);}
[a-zA-Z\_\[\]\\\!\=\<\>]+[a-zA-Z\_\[\]\\\!0-9\=]*		{return processLexeme(symtext);}
[\t\r\f ]	{}
"\n"		{line++;}
%%

int processLexeme(char * str) {

	void * data;
	int type = getElementType(str, &data);

	switch (type) {

		case ELEM_OP_INLINE:
			symlval.op = data;
			return OPERATOR_INLINE;

		case ELEM_OP:
			symlval.op = data;
			return OPERATOR;

		case ELEM_FUNCTION:
			symlval.op = data;
			return FUNCTION;

		case ELEM_SYMBOL:
			symlval.symbol = (char*) data;
			return SYMBOL;

		default:
			fprintf(stderr, "Unknown token '%s'\n", str);
			exit(1);

	}

}


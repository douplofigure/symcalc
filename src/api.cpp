#include "symcalc.h"

#include <queue>
#include <string>
#include <iostream>
#include <stdio.h>
#include "expression.h"
#include "rule.h"

#include "number.h"
#include <thread>
#include <istream>
#include <fstream>
#include <sstream>
#include <vector>
#include <sys/stat.h>
#include <sys/types.h>

#include "ioctl.h"

#include "computeresult.h"
#include "implementation.h"
#include "function.h"
#include "plotting.h"

#include "gnuplot_i/gnuplot_i.h"

using namespace symcalc;

bool usesPrintCallBack = false;
std::function<void(void)> printCallBack;

std::queue<char> theQueue;
std::ostream * outStream = &std::cout;
std::ostream * errStream = &std::cerr;
std::string workingPath = "./";
bool isCli = false;

int errCode = 0;

Implementation::Script * implScript;

extern "C" void parseFile(FILE * file);

std::queue<char>& operator<<(std::queue<char>& q, std::string str);

extern "C" {

    extern int line;

    void logError(int line, char * msg, char * buffer) {

        std::stringstream sstr;
        sstr << "Error on line " << line+1 << " : " << std::string(msg) << " near '" << std::string(buffer) << "'" << std::endl;
        ioctl::err() << sstr.str();
        errCode = 1;
        while (!theQueue.empty()) {
            theQueue.pop();
        }
        /*if (TextInterface::getActiveInterface()) {
            TextInterface::getActiveInterface()->pullStreamContents(sstr);
            //TextInterface::getActiveInterface()->clearCommandBuffer();
            TextInterface::getActiveInterface()->commandFinished();
        }*/


    }

    int getLexerInput(char * buffer, int max_size) {

        if (theQueue.empty()) {
            buffer[0] = -1;
            return 0;
        }

        buffer[0] = theQueue.front();
        if (buffer[0] == '$') {
            buffer[0] = '#';
        }
        buffer[1] = 0;
        theQueue.pop();
        return 1;

    }

}

void ioctl::setOutStream(std::ostream& stream) {

    outStream = &stream;

}

void ioctl::setErrStream(std::ostream& stream) {

    errStream = &stream;

}

bool ioctl::cli() {
    return isCli;
}

const char * ioctl::wd() {
    return workingPath.c_str();
}

std::ostream& ioctl::out() {
    return *outStream;
}

std::ostream& ioctl::err() {
    return *errStream;
}

void putFileInQueue(FILE * file) {

    line = 0;

    errCode = 0;
    char linebuffer[1024];
    while (fgets(linebuffer, 1023, file)) {
        //fputs(linebuffer, internalBuffer);
        theQueue << std::string(linebuffer);
    }

    parseFile(NULL);

    /*if (!errCode && TextInterface::getActiveInterface()) {
        std::stringstream sstr;
        sstr << std::endl << "File loaded successfully " << std::endl;
        //TextInterface::getActiveInterface()->pullStreamContents(sstr);
    }*/

}

Implementation::Script * getImplementationScript() {
    return implScript;
}

void setupOutputDir(char * dir) {

    struct stat st = {0};

    if (stat(dir, &st) == -1) {
        mkdir(dir, 0700);
    }

    workingPath = std::string(dir);
}

void symcalc::init(bool loadBuiltins) {

    setlocale(LC_NUMERIC, "en_US");

    BuiltinFunction::createBuiltins();

    if (loadBuiltins) {
        std::ifstream implStream;
        implStream.open("builtin.rules.impl");

        implScript = Implementation::Script::load(implStream);

        FILE * file = fopen("builtin.rules", "r");
        putFileInQueue(file);
        fclose(file);
    }

}

void symcalc::loadFile(std::string fname) {
    FILE * file = fopen(fname.c_str(), "r");
    putFileInQueue(file);
    fclose(file);
}

void symcalc::executeCommand(std::string command) {

    theQueue << command;
    parseFile(NULL);

}

void symcalc::pushCommand(std::string command) {

    theQueue << command;

}

void symcalc::executeBuffer() {

    parseFile(NULL);

}

void symcalc::setOutputTeX(bool TeX) {
    isCli = TeX;
}

void symcalc::setPrintCallback(std::function<void(void)> callBack) {
    printCallBack = callBack;
    usesPrintCallBack = true;
}
namespace symcalc {
    void onPrint() {
        if(usesPrintCallBack)
            printCallBack();
    }
}

void symcalc::clearBuffer() {
    while (!theQueue.empty()) {
        theQueue.pop();
    }
}

void symcalc::cleanup() {
    Rule::deleteAll();
    Function::deleteAll();
    Operator::deleteDefaults();
    Plotter::cleanup();
}

std::queue<char>& operator<<(std::queue<char>& q, std::string str) {
    for (int i = 0; i < str.length(); ++i) {
        q.push(str[i]);
    }
    return q;
}

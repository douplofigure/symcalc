#include "implementation.h"

#include "ast.h"
#include <string.h>
#include "implfunction.h"

using namespace Implementation;

struct input_provider_t {

    std::function<int(char*,int)> func;

};

extern "C" void implSetInputFunction(InputProvider * provider);
extern "C" AST * scriptExecute();

int implInput(InputProvider * provider, char * buffer, int max_size) {
    return provider->func(buffer, max_size);
}

std::ostream& operator<<(std::ostream& stream, Operation op) {

    stream << "vars[" << op.s1 << "] " << op.type << " vars[" << op.s2 << "] -> vars[" << op.dest << "]";
    return stream;

}

Script::Script() {

}

Script::~Script() {

    for (auto it = this->functions.begin(); it != this->functions.end(); ++it)
        delete it->second;

}

void Script::addFunction(std::string name, Implementation::Function * f) {
    this->functions[name] = f;
}

Implementation::Function::Function() {

}

Implementation::Function::Function(int argc) {
    this->argumentCount = argc;
    this->neededSpace = argc;
}

Implementation::Function::~Function() {

}

void Function::setInstructions(std::vector<Operation>& ops) {

    this->operations = std::vector<Operation>(ops.size());

    for (int i = 0; i < ops.size(); ++i) {

        if (ops[i].type != OP_JMP && ops[i].type != OP_J0 && ops[i].dest >= neededSpace) {
            neededSpace = ops[i].dest+1;
        }

        if (ops[i].s1 >= neededSpace) {
            neededSpace = ops[i].s1+1;
        }

        if (ops[i].s2 >= neededSpace) {
            neededSpace = ops[i].s2+1;
        }

        this->operations[i] = ops[i];
    }

}

int Function::getArgumentCount() {
    return argumentCount;
}

void printAST(AST * ast, int indent=0) {

    int cCount = 0;
    AST ** children = astGetChildren(ast, &cCount);
    std::string indentString("");
    for (int i = 0; i < indent; ++i) indentString.append("    ");
    std::cout << indentString << "AST " << astGetType(ast) << " : { " << std::endl;

    if (astGetType(ast) == AST_OP_SYMBOL) {
        std::cout << indentString << "symbol='" << std::string((char*) children) << "'" << std::endl;
    } else {
        for (int i = 0; i < cCount; ++i) {
            printAST(children[i], indent+1);
        }
    }

    std::cout << indentString << "}" << std::endl;

}

typedef enum symbol_table_entry_type_e {

    SYMBOL_VAR,
    SYMBOL_FUNCTION

} SYMBOL_TYPE;

typedef struct symbol_table_entry_t {

    ///The type of the symbol
    SYMBOL_TYPE type;
    /// Variable -> location in memory, Function -> number of arguments
    int memLoc;

} SYMBOL_ENTRY;

typedef struct symbol_table_t {

     std::map<std::string, SYMBOL_ENTRY> symbolTable;
     struct symbol_table_t * parent;

} SYMBOL_TABLE;

static int tmpCount = 0;
Operation operationsFromAST(AST * ast, std::vector<Operation>& ops, SYMBOL_TABLE * table, int freePos);

SYMBOL_ENTRY lookupSymbol(std::string symbol, SYMBOL_TABLE * table) {

    if (table->symbolTable.find(symbol) == table->symbolTable.end()){
        if (table->parent) return lookupSymbol(symbol, table->parent);
        else throw std::runtime_error(std::string("No such symbol: ").append(symbol));
    }

    return table->symbolTable[symbol];

}

Operation getOperationOperator(AST * ast, std::vector<Operation>& ops, SYMBOL_TABLE * table, int freePos) {

    int childCount;
    AST ** children = astGetChildren(ast, &childCount);

    Operation op;

    switch (astGetType(ast)) {
        case '+':
            op.type = OP_ADD;
            op.s1 = operationsFromAST(children[0], ops, table, freePos+1).dest;
            op.s2 = operationsFromAST(children[1], ops, table, freePos+2).dest;
            op.dest = freePos;
            break;

        case '-':
            op.type = OP_SUB;
            op.s1 = operationsFromAST(children[0], ops, table, freePos+1).dest;
            op.s2 = operationsFromAST(children[1], ops, table, freePos+2).dest;
            op.dest = freePos;
            break;

        case '*':
            op.type = OP_MUL;
            op.s1 = operationsFromAST(children[0], ops, table, freePos+1).dest;
            op.s2 = operationsFromAST(children[1], ops, table, freePos+2).dest;
            op.dest = freePos;
            break;

        case '/':
            op.type = OP_DIV;
            op.s1 = operationsFromAST(children[0], ops, table, freePos+1).dest;
            op.s2 = operationsFromAST(children[1], ops, table, freePos+2).dest;
            op.dest = freePos;
            break;

        case '^':
            op.type = OP_POW;
            op.s1 = operationsFromAST(children[0], ops, table, freePos+1).dest;
            op.s2 = operationsFromAST(children[1], ops, table, freePos+2).dest;
            op.dest = freePos;
            break;

        case '<':
            op.type = OP_CMP_L;
            op.s1 = operationsFromAST(children[0], ops, table, freePos+1).dest;
            op.s2 = operationsFromAST(children[1], ops, table, freePos+2).dest;
            op.dest = freePos;
            break;

        case '>':
            op.type = OP_CMP_G;
            op.s1 = operationsFromAST(children[0], ops, table, freePos+1).dest;
            op.s2 = operationsFromAST(children[1], ops, table, freePos+2).dest;
            op.dest = freePos;
            break;

        case AST_OP_EQ:
            op.type = OP_CMP_E;
            op.s1 = operationsFromAST(children[0], ops, table, freePos+1).dest;
            op.s2 = operationsFromAST(children[1], ops, table, freePos+2).dest;
            op.dest = freePos;
            break;

        case AST_OP_LEQ:
            op.type = OP_CMP_LE;
            op.s1 = operationsFromAST(children[0], ops, table, freePos+1).dest;
            op.s2 = operationsFromAST(children[1], ops, table, freePos+2).dest;
            op.dest = freePos;
            break;

        case AST_OP_GEQ:
            op.type = OP_CMP_GE;
            op.s1 = operationsFromAST(children[0], ops, table, freePos+1).dest;
            op.s2 = operationsFromAST(children[1], ops, table, freePos+2).dest;
            op.dest = freePos;
            break;

    }

    return op;

}

Operation operationsFromFunction(AST * ast, std::vector<Operation>& ops, SYMBOL_TABLE * table) {

    int cCount, tmp;
    AST ** children = astGetChildren(ast, &cCount);

    SYMBOL_TABLE * nTable = new SYMBOL_TABLE();
    nTable->parent = table;

    int argc;
    AST ** argTrees = astGetChildren(children[1], &argc);

    nTable->symbolTable[std::string((char*) astGetChildren(children[0], &tmp))] = (SYMBOL_ENTRY) {SYMBOL_FUNCTION, argc};

    for (int i = 0; i < argc; ++i) {
        nTable->symbolTable[std::string((char*) astGetChildren(argTrees[i], &tmp))] = (SYMBOL_ENTRY) {SYMBOL_VAR, i};
    }

    Operation lOp = operationsFromAST(children[2], ops, nTable, argc);
    Operation mv;
    mv.dest = 0;
    mv.s1 = lOp.dest;
    mv.s2 = 0;
    mv.type = OP_MOV;
    ops.push_back(mv);

    delete nTable;

    return mv;

}

Operation operationsFromAST(AST * ast, std::vector<Operation>& ops, SYMBOL_TABLE * table, int freePos) {

    int childCount;
    AST ** children = astGetChildren(ast, &childCount);

    Operation op;
    op.s1 = 0;
    op.s2 = 0;
    op.type = OP_NONE;
    op.dest = 0;

    switch (astGetType(ast)) {

        case '+':
        case '-':
        case '*':
        case '/':
        case '%':
        case '^':
        case '<':
        case '>':
        case AST_OP_EQ:
        case AST_OP_GEQ:
        case AST_OP_LEQ:
            op = getOperationOperator(ast, ops, table, freePos);
            break;

        case AST_OP_SYMBOL:
            op.dest = lookupSymbol(std::string((char*)children), table).memLoc;
            return op;

        case AST_FUNC_DEF:
            return operationsFromFunction(ast, ops, table);

        case AST_OP_NONE:
            for (int i = 0; i < childCount; ++i) {
                op = operationsFromAST(children[i], ops, table, freePos);
            }
            return op;



    }

    ops.push_back(op);

    return op;

}

Script * loadScript(InputProvider * provider) {

    implSetInputFunction(provider);
    AST * ast = scriptExecute();

    SYMBOL_TABLE * table = new SYMBOL_TABLE();

    int funcCount = 0;
    AST ** functions = astGetChildren(ast, &funcCount);

    Script * script = new Script();

    for (int i = 0; i < funcCount; ++i) {
        int cCount, tmp;
        AST ** children = astGetChildren(functions[i], &cCount);

        std::string name((char*) astGetChildren(children[0], &tmp));
        std::vector<Operation> operations;
        operationsFromFunction(functions[i], operations, table);

        Function * f = new Function();
        f->setInstructions(operations);

        /*std::cerr << name << std::endl;
        for (int j = 0; j < operations.size(); ++j) {
            std::cerr << operations[j] << std::endl;
        }*/

        script->addFunction(name, f);

    }

    delete provider;
    delete table;

    //printAST(ast);

    astDelete(ast);

    return script;

}

Script * Script::load(std::string data) {

    InputProvider * provider = new InputProvider();

    provider->func = [&data] (char * buffer, int max_size) -> int {

        if (!data.length()) {
            buffer[0] = EOF;
            return 0;
        } else if (max_size >= data.length()) {
            for (int i = 0; i < data.length(); ++i) {
                buffer[i] = data.c_str()[i];
            }
            int tmp = data.length();
            data = std::string("");
            return tmp;
        } else {

            for (int i = 0; i < max_size; ++i) {
                buffer[i] = data.c_str()[i];
            }

            data = data.substr(max_size);
            return max_size;

        }

    };

    return loadScript(provider);

}

Script * Script::load(std::istream& data) {

    InputProvider * provider = new InputProvider();

    provider->func = [&data] (char * buffer, int max_size) -> int {

        if (data.eof()) {
            buffer[0] = EOF;
            return 0;
        }

        data.read(buffer, max_size-1);
        int s = strlen(buffer);
        buffer[s] = 0;
        return s;

    };

    return loadScript(provider);

}

#include "plotting.h"

#include <algorithm>
#include <fstream>

#include "ioctl.h"

gnuplot_ctrl * Plotter::gnuplotCtl;
int Plotter::graphCount;
std::vector<std::string> Plotter::filesToDelete;

std::string toTeXString(std::string str) {

    std::string out;
    out.reserve(str.size());

    for (const char c : str) {

        if (c == '\\') {
            out += "\\\\";
        } else {
            out += c;
        }

    }

    return out;

}

void Plotter::cleanup() {
    for (int i = 0; i < filesToDelete.size(); ++i) {
        std::string fname = filesToDelete[i];
        system(std::string("rm ").append(fname).c_str());
    }
}

void Plotter::plotExpression(std::string symbol, ComputeResult xMin, ComputeResult xMax, int lines, Expression * exp) {

    if (!gnuplotCtl) {
        gnuplotCtl = gnuplot_init();
    }

    std::map<std::string, ComputeResult> varValues;
    //std::vector<double> xvals(lines+1);
    //std::vector<double> yvals(lines+1);
    //plot x, #0, #2 * #pi : sin(x)

    std::string fname = std::string("tmp_plot_").append(std::to_string(graphCount));
    std::ofstream file(fname);

    if (ioctl::cli()) {
        file << "x $" << toTeXString(exp->format()) << "$";
    } else {
        file << "x exp";
    }

    file << std::endl;

    for (int i = 0; i < lines+1; ++i) {

        ComputeResult x = ComputeResult(Number((double) i / (double) lines)) * (xMax - xMin) + xMin;
        varValues[symbol] = x;

        if (!exp->canCompute(varValues))
            std::cerr << "Unable to compute expression" << std::endl;

        ComputeResult y = exp->getValue(varValues);

        file << x << " " << y << std::endl;

    }

    char * oldLocale = setlocale(LC_NUMERIC, NULL);
    setlocale(LC_NUMERIC, "en_US");

    gnuplot_resetplot(gnuplotCtl);
    gnuplot_cmd(gnuplotCtl, "set colorsequence classic");
    if (ioctl::cli()) {
        gnuplot_cmd(gnuplotCtl, "set term epslatex color");
        gnuplot_cmd(gnuplotCtl, "set output \"%s/tmpgraph%d.tex\"", ioctl::wd(),  graphCount);
    }
    gnuplot_setstyle(gnuplotCtl, "lines");
    std::string str = exp->format();
    //gnuplot_plot_xy(gnuplotCtl, xvals.data(), yvals.data(), lines+1, (char*) "Expression");
    //gnuplot_cmd(gnuplotCtl, "set key outside");
    gnuplot_cmd(gnuplotCtl, "plot '%s' using 1:2 with lines title columnheader", fname.c_str());

    setlocale(LC_NUMERIC, oldLocale);

    if (ioctl::cli()) {

        gnuplot_close(gnuplotCtl);
        gnuplotCtl = NULL;

        //system("ps2pdf -dEPSCrop tmpgraph.eps tmpgraph.pdf");

        ioctl::out() << "\\begin{figure}[H]" << std::endl;
        ioctl::out() << "  \\input{" << std::string(ioctl::wd()) << "/tmpgraph" << graphCount << ".tex}" << std::endl;
        ioctl::out() << "  \\caption{$" << str << "$}" << std::endl;
        ioctl::out() << "\\end{figure}" << std::endl;

        //system("rm tmpgraph.tex tmpgraph.eps");
    }
    //free(oldLocale);

    filesToDelete.push_back(fname);

    graphCount++;

}

void Plotter::plotExpressions(std::string symbol, ComputeResult xMin, ComputeResult xMax, int lines, std::vector<Expression *> expressions) {

    if (!gnuplotCtl) {
        gnuplotCtl = gnuplot_init();
    }

    std::map<std::string, ComputeResult> varValues;
    //std::vector<double> xvals(lines+1);
    //std::vector<double> yvals(lines+1);
    //plot x, #0, #2 * #pi : sin(x)

    std::string fname = std::string("tmp_plot_").append(std::to_string(graphCount));
    std::ofstream file(fname);

    std::cout << "fname = " << fname << std::endl;
    file << "x";
    for (int i = 0; i < expressions.size(); ++i) {
        if (ioctl::cli()) {
            file << " $" << toTeXString(expressions[i]->format()) << "$";
        } else {
            file << " exp" << i;
        }
    }

    file << std::endl;

    for (int i = 0; i < lines+1; ++i) {

        ComputeResult x = ComputeResult(Number((double) i / (double) lines)) * (xMax - xMin) + xMin;
        varValues[symbol] = x;

        file << x;
        for (int j = 0; j < expressions.size(); ++j) {

            Expression * exp = expressions[j];

            if (!exp->canCompute(varValues))
                std::cerr << "Unable to compute expression" << std::endl;

            ComputeResult y = exp->getValue(varValues);

            file << " " << y;

        }

        file << std::endl;

    }

    file.close();

    char * oldLocale = setlocale(LC_NUMERIC, NULL);
    setlocale(LC_NUMERIC, "en_US");

    gnuplot_resetplot(gnuplotCtl);
    gnuplot_cmd(gnuplotCtl, "set colorsequence classic");
    if (ioctl::cli()) {
        gnuplot_cmd(gnuplotCtl, "set term epslatex color");
        gnuplot_cmd(gnuplotCtl, "set output \"%s/tmpgraph%d.tex\"", ioctl::wd(),  graphCount);
    }
    gnuplot_setstyle(gnuplotCtl, "lines");
    //std::string str = exp->format();
    //gnuplot_plot_xy(gnuplotCtl, xvals.data(), yvals.data(), lines+1, (char*) "Expression");

    gnuplot_cmd(gnuplotCtl, "plot for [i=2:%d] '%s' using 1:i with lines title columnheader", expressions.size()+1, fname.c_str());

    setlocale(LC_NUMERIC, oldLocale);

    if (ioctl::cli()) {

        gnuplot_close(gnuplotCtl);
        gnuplotCtl = NULL;

        //system("ps2pdf -dEPSCrop tmpgraph.eps tmpgraph.pdf");

        ioctl::out() << "\\begin{figure}" << std::endl;
        ioctl::out() << "  \\input{" << std::string(ioctl::wd()) << "/tmpgraph" << graphCount << ".tex}" << std::endl;
        //ioctl::out() << "  \\caption{$" << str << "$}" << std::endl;
        ioctl::out() << "\\end{figure}" << std::endl;

        //system("rm tmpgraph.tex tmpgraph.eps");
    }
    //free(oldLocale);

    filesToDelete.push_back(fname);

    graphCount++;

}

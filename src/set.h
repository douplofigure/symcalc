#ifndef SET_H
#define SET_H

#include <string>

#include "number.h"
#include "expression.h"
#include "cheader.h"

class Set : public Expression {

    public:
        Set(std::string vName, std::shared_ptr<Expression> right);
        Set(std::shared_ptr<Expression> left, std::shared_ptr<Expression> right);
        virtual ~Set();

        virtual bool contains(std::shared_ptr<Expression> exp);
        int getComplexityScore();

        std::string format();
        virtual std::shared_ptr<Expression> simplfy(std::vector<Rule*> rules);
        virtual std::shared_ptr<Expression> getBestNeighbor(std::vector<Rule*> rules);
        virtual std::vector<std::shared_ptr<Expression>> getNeighbors(std::vector<Rule*> rules);
        void writeToStream(std::ostream& stream);
        bool applyRules();

        virtual int getDimension();
        virtual Type getType();

        virtual std::shared_ptr<Expression> createCopy() override;

        static Set * unite(std::shared_ptr<Set> a, std::shared_ptr<Set> b);
        static Set * diff(std::shared_ptr<Set> a, std::shared_ptr<Set> b);
        static Set * cartesian(std::shared_ptr<Set> a, std::shared_ptr<Set> b);


    protected:

        Set();

    private:

        std::shared_ptr<Expression> leftCond;
        std::shared_ptr<Expression> rightCond;
        std::string varName;
        int dimension;

};

class ProductSet : public Set {

    public:
        ProductSet(std::shared_ptr<Set> left, std::shared_ptr<Set> right);
        virtual ~ProductSet();

        int getDimension();
        std::shared_ptr<Expression> createCopy();
        bool contains(Expression * exp);
        int getComplexityScore();
        std::string format();
        std::shared_ptr<Expression> simplfy(std::vector<Rule*> rules);
        void writeToStream(std::ostream& stream);

    private:
        std::shared_ptr<Set> leftOp;
        std::shared_ptr<Set> rightOp;

};

class NumSet : public Set {

    public:
        NumSet(Number::NumberType maxType);
        virtual ~NumSet();

        int getComplexityScore();
        bool contains(Expression * exp);
        std::string format();
        std::shared_ptr<Expression> simplfy(std::vector<Rule *> rules);
        std::shared_ptr<Expression> getBestNeighbor(std::vector<Rule *> rules);
        std::vector<std::shared_ptr<Expression>> getNeighbors(std::vector<Rule *> rules);
        void writeToStream(std::ostream& stream);

        std::shared_ptr<Expression> createCopy() override;
        int getDimension();

    private:

        Number::NumberType maxType;


};

#endif // SET_H

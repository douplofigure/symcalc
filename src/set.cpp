#include "set.h"

#include <sstream>
#include <iostream>

#include "operator.h"
#include "expression.h"
#include "number.h"
#include "vector.h"

Set::Set(std::string name, std::shared_ptr<Expression> right){
    this->varName = name;
    this->leftCond = nullptr;
    this->rightCond = right;

    this->dimension = 1;
}

Set::Set(std::shared_ptr<Expression> left, std::shared_ptr<Expression> right) {
    this->leftCond = left;
    this->rightCond = right;
    if (left->getOperator() && left->getOperator() != Operator::getFromSymbol("in")) throw std::runtime_error("Left side of set needs to be operator 'in'!");

    if (!left->getOperator()) {
        this->varName = left->getSymbol();
    } else {
        this->varName = left->getOperands()[0]->getSymbol();
    }

    this->dimension = 1;

}

Set::Set() {
    this->leftCond = nullptr;
    this->rightCond = nullptr;
    this->dimension = 1;
}

Set::~Set()
{
    //dtor
}

void Set::writeToStream(std::ostream& stream) {

    stream << "{";
    if (this->leftCond) {
        this->leftCond->writeToStream(stream);
    } else {
        stream << this->varName;
    }

    stream << " : ";

    this->rightCond->writeToStream(stream);
    stream << "}";

}

int Set::getDimension() {
    return this->dimension;
}

std::string Set::format() {

    std::stringstream stream;
    //std::cout << "formatting set" << std::endl;

    if (leftCond) {

        stream << "\\left \\{" << leftCond->format() << " : " << rightCond->format() << "\\right \\}";

    } else {
        stream << "\\left \\{" << varName << " : " << rightCond->format() << "\\right \\}";
    }

    return stream.str();

}

std::shared_ptr<Expression> Set::simplfy(std::vector<Rule*> rule) {
    //std::cout << "simplifying a set" << std::endl;
    if (!leftCond) {
        return std::shared_ptr<Expression>(new Set(this->varName, this->rightCond->simplfy(rule)));
    }

    return std::shared_ptr<Expression>(new Set(this->leftCond->simplfy(rule), this->rightCond->simplfy(rule)));
}

bool Set::contains(std::shared_ptr<Expression> exp) {

    std::map<std::string, ComputeResult> m;
    m[this->varName] = exp->getValue(m);

    if (!leftCond)
        return rightCond->getValue(m).n;

    return leftCond->getValue(m).n && rightCond->getValue(m).n;

}

std::shared_ptr<Expression> Set::getBestNeighbor(std::vector<Rule*> rules) {
    std::cout << "Set neighbor" << std::endl;
    return this->simplfy(rules);
}

std::vector<std::shared_ptr<Expression>> Set::getNeighbors(std::vector<Rule *> rules) {
    std::cout << "NumSet neighbor" << std::endl;
    std::vector<std::shared_ptr<Expression>> res(1);
    res[0] = leftCond ? std::shared_ptr<Expression>(new Set(leftCond, rightCond)) : std::shared_ptr<Expression>(new Set(varName, rightCond));
    return res;
}

int Set::getComplexityScore() {
    if (!leftCond) {
        return rightCond->getComplexityScore() + 1;
    }
    return leftCond->getComplexityScore() + rightCond->getComplexityScore() + 1;
}

bool Set::applyRules() {
    //std::cout << "Set does not need rules" << std::endl;
    return false;
}

std::shared_ptr<Expression> Set::createCopy() {

    if (!leftCond) {
        return std::shared_ptr<Expression>(new Set(this->varName, rightCond));
    }

    return std::shared_ptr<Expression>(new Set(leftCond, rightCond));

}

Set * Set::cartesian(std::shared_ptr<Set> a, std::shared_ptr<Set> b) {
    return new ProductSet(a, b);
}

Set * Set::unite(std::shared_ptr<Set> a, std::shared_ptr<Set> b) {

    std::string name("x");
    Expression * xInA = new Expression(Operator::getFromSymbol("in"), new Expression(nullptr, "x"), a);
    Expression * xInB = new Expression(Operator::getFromSymbol("in"), new Expression(nullptr, "x"), b);

    std::shared_ptr<Expression> right = std::shared_ptr<Expression>(new Expression(Operator::getFromSymbol("|"), xInA, xInB));

    return  new Set(name, right);

}

Set * Set::diff(std::shared_ptr<Set> a, std::shared_ptr<Set> b) {

    std::string name("x");
    Expression * xInA = new Expression(Operator::getFromSymbol("in"), new Expression(nullptr, "x"), a);
    Expression * xInB = new Expression(Operator::getFromSymbol("in"), new Expression(nullptr, "x"), b);

    std::shared_ptr<Expression> right = std::shared_ptr<Expression>(new Expression(Operator::getFromSymbol("&"), xInA, new Expression(Operator::getFromSymbol("not"), xInB)));

    return new Set(name, right);

}

Expression::Type Set::getType() {
    return Expression::SET;
}

NumSet::NumSet(Number::NumberType type) {
    this->maxType = type;
}

NumSet::~NumSet() {

}

std::shared_ptr<Expression> NumSet::simplfy(std::vector<Rule *> rules) {
    return std::shared_ptr<Expression>(new NumSet(this->maxType));
}

int NumSet::getComplexityScore() {
    return 1;
}

void NumSet::writeToStream(std::ostream& stream) {

    stream << "numset(";

    switch (this->maxType) {

        case Number::NUM_N:
            stream << "N";
            break;

        case Number::NUM_Z:
            stream << "Z";
            break;

        case Number::NUM_Q:
            stream << "Q";
            break;

        case Number::NUM_R:
            stream << "R";
            break;

        case Number::NUM_C_EULER:
        case Number::NUM_C:
            stream << "C";
            break;

    }

    stream << ")";

}

bool NumSet::contains(Expression * exp) {
    std::map<std::string, ComputeResult> m;
    return exp->getValue(m).n.getType() <= maxType;
}

std::string NumSet::format() {

    switch (this->maxType) {

        case Number::NUM_N:
            return "\\mathbb{N}";

        case Number::NUM_Z:
            return "\\mathbb{Z}";

        case Number::NUM_Q:
            return "\\mathbb{Q}";

        case Number::NUM_R:
            return "\\mathbb{R}";

        case Number::NUM_C_EULER:
        case Number::NUM_C:
            return "\\mathbb{C}";

    }

}
std::shared_ptr<Expression> NumSet::getBestNeighbor(std::vector<Rule *> rules) {
    return std::shared_ptr<Expression>(new NumSet(this->maxType));
}

void * setCreateCondition(void * left, void * right) {
    return new Set(std::shared_ptr<Expression>((Expression *) left), std::shared_ptr<Expression>((Expression *) right));
}

std::vector<std::shared_ptr<Expression>> NumSet::getNeighbors(std::vector<Rule *> rules) {
    std::cout << "NumSet neighbor" << std::endl;
    std::vector<std::shared_ptr<Expression>> res(1);
    res[0] = std::shared_ptr<Expression>(new NumSet(this->maxType));
    return res;
}

void * setCreateList(void * expList) {
    EXP_LIST * ls = (EXP_LIST *) expList;

    std::shared_ptr<Expression> exp = std::shared_ptr<Expression>(new Expression(Operator::getFromSymbol("=="), new Expression(nullptr, "a"), ls->expressions[0]));

    for (int i = 1; i < ls->expressions.size(); ++i) {
        exp = std::shared_ptr<Expression>(new Expression(Operator::getFromSymbol("|"), exp, new Expression(Operator::getFromSymbol("=="), new Expression(nullptr, "a"), ls->expressions[i])));
    }

    //delete ls;

    return new Set("a", exp);

}

void * numsetCreate(char * symbol) {

    switch(symbol[0]) {

        case 'N':
            return new NumSet(Number::NUM_N);

        case 'Z':
            return new NumSet(Number::NUM_Z);

        case 'Q':
            return new NumSet(Number::NUM_Q);

        case 'R':
            return new NumSet(Number::NUM_R);

        case 'C':
            return new NumSet(Number::NUM_C_EULER);

    }

    free(symbol);

}

std::shared_ptr<Expression> NumSet::createCopy() {
    return std::shared_ptr<Expression>( new NumSet(this->maxType));
}

int NumSet::getDimension() {

    return 1;

}

ProductSet::ProductSet(std::shared_ptr<Set> a, std::shared_ptr<Set> b) {

    this->leftOp = a;
    this->rightOp = b;

}

ProductSet::~ProductSet() {

}

int ProductSet::getDimension() {
    return leftOp->getDimension() + rightOp->getDimension();
}

std::shared_ptr<Expression> ProductSet::createCopy() {
    return std::shared_ptr<Expression>( new ProductSet(std::dynamic_pointer_cast<Set>(leftOp->createCopy()), std::dynamic_pointer_cast<Set>(rightOp->createCopy())));
}

bool ProductSet::contains(Expression * exp) {

    if (exp->getType() == Expression::VECTOR) return false;
    Vector * vec = (Vector *) exp;
    if (vec->getDimension() != getDimension()) return false;

    bool b = this->rightOp->contains(vec->getOperands().back());
    if (vec->getOperands().size() <= 2) {
        b &= this->leftOp->contains(vec->getOperands()[0]);
    } else {
        std::vector<std::shared_ptr<Expression>> toTest(vec->getOperands().size()-1);
        for (int i = 0; i < vec->getOperands().size()-1; ++i) {
            toTest[i] = vec->getOperands()[i];
        }
        b &= this->leftOp->contains(std::shared_ptr<Expression>(new Vector(toTest)));
    }

    return b;
}

int ProductSet::getComplexityScore() {
    return this->leftOp->getComplexityScore() + this->rightOp->getComplexityScore();
}

std::string ProductSet::format() {
    std::stringstream stream;

    stream << "\\left (" << this->leftOp->format() << " \\times " << this->rightOp->format() << " \\right )";

    return stream.str();
}

std::shared_ptr<Expression> ProductSet::simplfy(std::vector<Rule*> rules) {
    return std::shared_ptr<Expression>(new ProductSet(std::dynamic_pointer_cast<Set>(this->leftOp->simplfy(rules)), std::dynamic_pointer_cast<Set>(this->rightOp->simplfy(rules))));
}

void ProductSet::writeToStream(std::ostream& stream) {

    this->leftOp->writeToStream(stream);
    stream << " * ";
    this->rightOp->writeToStream(stream);

}

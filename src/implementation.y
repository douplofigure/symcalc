%name-prefix "impl"

%code requires {
    #include "ast.h"
}

%{
#include "ast.h"
static AST * ast;
%}

%union {
    char * symbol;
    AST * ast;
    char c;
}

%token<symbol> NAME
%token KW_FUNC
%token<c> OPERATOR

%type <ast> file;
%type <ast> function
%type <ast> instruction
%type <ast> instructionList
%type <ast> codeBlock
%type <ast> nameList
%type <ast> comparison

%%

file:
    function                                            {$$ = astCreate(AST_OP_NONE, 1, $1); ast = $$;}
    |file function                                      {$$ = astAddChildren($1, 1, $2); ast = $$;}
    ;

function:
    KW_FUNC NAME '(' nameList ')' codeBlock             {$$ = astCreate(AST_FUNC_DEF, astCreate(AST_OP_SYMBOL, $2), $4, $6);}
    ;

nameList :
    NAME                                                {$$ = astCreate(AST_OP_NONE, 1, astCreate(AST_OP_SYMBOL, $1));}
    | nameList ',' NAME                                 {$$ = astAddChildren($1, 1, astCreate(AST_OP_SYMBOL, $3));}
    ;

codeBlock:
    '{' instructionList '}'                                 {$$ = $2;}
    ;

instructionList:
    instruction                                                        {$$ = astCreate(AST_OP_NONE, 1, $1);}
    |instructionList instruction                                       {$$ = astAddChildren($1, 1, $2);}
    ;

instruction:
    NAME                                                               {$$ = astCreate(AST_OP_SYMBOL, $1);}
    |'(' instruction ')'                                               {$$ = $2;}
    |instruction OPERATOR instruction                                  {$$ = astCreate($2, $1, $3);}
    |comparison                                                        {$$ = $1;}
    ;

comparison:
    instruction '<' instruction                     {$$ = astCreate('<', $1, $3);}
    |instruction '>' instruction                    {$$ = astCreate('>', $1, $3);}
    |instruction '<' '=' instruction                {$$ = astCreate(AST_OP_LEQ, $1, $4);}
    |instruction '>' '=' instruction                {$$ = astCreate(AST_OP_GEQ, $1, $4);}
    |instruction '=' '=' instruction                {$$ = astCreate(AST_OP_EQ, $1, $4);}
    ;


%%

void implerror( char * msg ) {

}

int implwrap() {
    return 1;
}

AST * scriptExecute() {
    implparse();
    impllex_destroy();
    return ast;
}

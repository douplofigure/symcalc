
%option prefix="impl"
%{
#include "impl_parser.h"
#include "implementation.h"

static InputProvider * inputFunc;
static char * copyString(char * str);

#define YY_INPUT(buf, result, max_size) {result = implInput(inputFunc, buf, max_size);}

%}

%%

"func"                  {return KW_FUNC;}
[A-Za-z][A-Za-z0-9_]*   {impllval.symbol = strdup(impltext); return NAME;}
[\+\*\/\-\^]            {impllval.c = *impltext; return OPERATOR;}
[\(\)\[\]\{\}\,\<\>=]        {return *impltext;}

[ \t\n]                   {}


%%

void implSetInputFunction(InputProvider * func) {
    inputFunc = func;
}

char * copyString(char * str) {

    /*char * dest = (char*) malloc(sizeof(char) * (strlen(str) + 1));
    strcpy(dest, str);
    dest[strlen(str)] = 0;
    return dest;*/

}

#include "ast.h"

#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

struct ast_t {

    AST_OP_TYPE type;
    int childCount;
    AST ** children;

};

static int astGetOpArgCount(AST_OP_TYPE type) {

    if (type > AST_OP_SYMBOL && type < AST_OP_CALL) {
        return 2;
    }

    switch(type) {

        case '+':
        case '-':
        case '*':
        case '/':
        case '<':
        case '>':
        case AST_OP_LEQ:
        case AST_OP_GEQ:
        case AST_OP_EQ:
            return 2;

        case AST_OP_CALL:
            return 2;

        case AST_FUNC_DEF:
            return 3;

        default:
            return 0;

    }

}

AST * astCreate(AST_OP_TYPE type, ...) {

    va_list ls;
    va_start(ls, type);

    AST * ast = malloc(sizeof(AST));
    ast->type = type;

    if (!type) {

        ast->childCount = va_arg(ls, int);
        ast->children = malloc(sizeof(AST *) * ast->childCount);
        for (int i = 0; i < ast->childCount; ++i) {

            ast->children[i] = va_arg(ls, AST*);

        }

        return ast;


    } else if (type == AST_OP_SYMBOL) {
        char * symbolName = va_arg(ls, char*);
        ast->childCount = strlen(symbolName);
        ast->children = symbolName;
        return ast;
    }

    ast->childCount = astGetOpArgCount(type);
    ast->children = malloc(sizeof(AST *) * ast->childCount);

    for (int i = 0; i < ast->childCount; ++i) {

        ast->children[i] = va_arg(ls, AST*);

    }

    return ast;

}

AST * astAddChildren(AST * ast, int count, ...) {

    va_list ls;
    va_start(ls, count);

    ast->children = realloc(ast->children, (ast->childCount+count) * sizeof(AST*));
    for (int i = ast->childCount; i < ast->childCount+count; ++i) {
        ast->children[i] = va_arg(ls, AST *);
    }
    ast->childCount += count;
    return ast;

}

AST_OP_TYPE astGetType(AST * ast) {
    if (!ast) return AST_OP_NONE;
    return ast->type;
}

AST ** astGetChildren(AST * ast, int * count) {

    if (!ast) return NULL;

    *count = ast->childCount;
    return ast->children;

}

void astDelete(AST * ast) {

    if (ast->type == AST_OP_SYMBOL) {
        free(ast->children);
        free(ast);
        return;
    }

    for (int i = 0; i < ast->childCount; ++i) {
        astDelete(ast->children[i]);
    }
    free(ast->children);
    free(ast);

}

#include "number.h"

#include <math.h>
#include <stdarg.h>
#include <iostream>
#include <limits>
#include <sstream>

#include "expression.h"

Number::Number() {
    this->type = NUM_NONE;
}

Number::Number(unsigned long ul) {
    this->type = NUM_N;
    this->val.n = ul;
}

Number::Number(long l) {
    this->type = NUM_Z;
    this->val.frac.p = l;
    this->val.frac.q = 1;
}

Number::Number(long p, long q) {
    this->type = NUM_Q;
    this->val.frac.p = p;
    this->val.frac.q = q;
    simplfyFraction();
}

Number::Number(double l) {
    this->type = NUM_R;
    this->val.comp.real = l;
    this->val.comp.img = 0;
}

Number::Number(double p, double q) {
    this->type = NUM_C;
    this->val.comp.real = p;
    this->val.comp.img = q;
}

Number::Number(int i) : Number((long) i) {
}

Number::Number(int p, int q) : Number((long) p, (long) q) {
}

Number::Number(NumberType t, ...){
    this->type = t;
    va_list vl;
    va_start(vl, t);

    switch (t) {

        case NUM_N:
            val.n = va_arg(vl, unsigned long);
            break;

        case NUM_Z:
            val.frac.p = va_arg(vl, long);
            val.frac.q = 1;
            break;

        case NUM_Q:
            val.frac.p = va_arg(vl, long);
            val.frac.q = va_arg(vl, long);
            break;

        case NUM_R:
            val.comp.real = va_arg(vl, double);
            val.comp.img = 0;
            break;

        case NUM_C:
        case NUM_C_EULER:
            val.comp.real = va_arg(vl, double);
            val.comp.img = va_arg(vl, double);
            break;

        default:
            throw std::runtime_error("Unknown NumberType");

    }

    va_end(vl);

}

Number::~Number()
{
    //dtor
}

Number::NumberType Number::getType() {
    return type;
}

void Number::toType(NumberType destType) {

    if (destType < type && !(destType == NUM_C && type == NUM_C_EULER))
        throw std::runtime_error("Cannot convert type to lower type!");

    if (destType == NUM_C && type == NUM_C_EULER) {
        double ang = val.comp.img;
        double len = val.comp.real;
        val.comp.real = len * cos(ang);
        val.comp.img = len * sin(ang);
        type = NUM_C;
        return;
    }

    //Same type, nothing to be done.
    if (destType == type) return;

    double tmp;
    double ang;
    double len;

    if (destType == type + 1) {

        switch (destType) {

            case NUM_Z:
                val.frac.p = val.n;
                val.frac.q = 1;
                break;

            case NUM_Q:
                break;

            case NUM_R:
                tmp = (double) val.frac.p / (double) val.frac.q;
                val.comp.real = tmp;
                val.comp.img = 0;
                break;

            case NUM_C:
                break;

            case NUM_C_EULER:
                len = sqrt((val.comp.real * val.comp.real) + (val.comp.img * val.comp.img));
                ang = atan2(val.comp.img, val.comp.real);
                val.comp.real = len;
                val.comp.img = ang;
                break;

            default:
                throw std::runtime_error("Unknown number type");

        }
        type = destType;
        return;

    }

    toType((NumberType)(destType-1));
    toType(destType);

}

Number Number::operator+(Number n) {

    Number tmp(*this);
    NumberType resType = type;
    if (n.type > type) {
        resType = n.type;
        tmp.toType(resType);
    } else {
        n.toType(resType);
    }

    switch (resType) {

        case NUM_N:
            return Number(tmp.val.n + n.val.n);

        case NUM_Z:
            return Number(tmp.val.frac.p + n.val.frac.p);

        case NUM_Q:
            return Number(tmp.val.frac.p * n.val.frac.q + tmp.val.frac.q * n.val.frac.p, tmp.val.frac.q * n.val.frac.q);

        case NUM_R:
            return Number(tmp.val.comp.real + n.val.comp.real);

        case NUM_C:
            return Number(tmp.val.comp.real + n.val.comp.real, tmp.val.comp.img + n.val.comp.img);

        case NUM_C_EULER:
            tmp.toType(NUM_C);
            n.toType(NUM_C);
            return tmp + n;

        default:
            throw std::runtime_error("Unimplemented operation");

    }

}

Number Number::operator-(Number n) {

    Number tmp(*this);
    NumberType resType = type;
    if (n.type > type) {
        resType = n.type;
        tmp.toType(resType);
    } else {
        n.toType(resType);
    }

    switch (resType) {

        case NUM_N:
            return Number(tmp.val.n - n.val.n);

        case NUM_Z:
            return Number(tmp.val.frac.p - n.val.frac.p);

        case NUM_Q:
            return Number(tmp.val.frac.p * n.val.frac.q - tmp.val.frac.q * n.val.frac.p, tmp.val.frac.q * n.val.frac.q);

        case NUM_R:
            return Number(tmp.val.comp.real - n.val.comp.real);

        case NUM_C:
            return Number(tmp.val.comp.real - n.val.comp.real, tmp.val.comp.img - n.val.comp.img);

        case NUM_C_EULER:
            tmp.toType(NUM_C);
            n.toType(NUM_C);
            return tmp - n;

        default:
            throw std::runtime_error("Unimplemented operation");

    }

}

Number Number::operator*(Number n) {

    Number tmp(*this);
    NumberType resType = type;
    //std::cout << type << " " << n.type << std::endl;
    if (n.type > type) {
        resType = n.type;
        tmp.toType(resType);
    } else {
        n.toType(resType);
    }

    switch (resType) {

        case NUM_N:
            return Number(tmp.val.n * n.val.n);

        case NUM_Z:
            return Number(tmp.val.frac.p * n.val.frac.p);

        case NUM_Q:
            return Number(tmp.val.frac.p * n.val.frac.p, tmp.val.frac.q * n.val.frac.q);

        case NUM_R:
            return Number(tmp.val.comp.real * n.val.comp.real);

        case NUM_C:
            return Number(tmp.val.comp.real * n.val.comp.real - tmp.val.comp.img * n.val.comp.img, tmp.val.comp.img * n.val.comp.real + tmp.val.comp.real * n.val.comp.img);

        case NUM_C_EULER:
            return Number(NUM_C_EULER, tmp.val.comp.real * n.val.comp.real, tmp.val.comp.real + n.val.comp.real);

        default:
            throw std::runtime_error("Unimplemented operation");

    }

}

bool Number::operator==(long i) {

    switch (type) {

        case NUM_N:
            return val.n == i;

        case NUM_Z:
            return val.frac.p == i;

        case NUM_Q:
            return val.frac.q == 1 && val.frac.p == i;

        default:
            return false;

    }

}

bool Number::operator==(Number n) {

    return ((*this) - n) == (long) 0;

}

bool Number::isNegative() {

    switch (type) {

        case NUM_N:
            return false;

        case NUM_Z:
        case NUM_Q:
            return val.frac.p * val.frac.q < 0;

        case NUM_R:
            return val.comp.real < 0.0;

        default:
            false;

    }

}

bool Number::operator<(Number n) {
    return (*this-n).isNegative();
}

bool Number::operator>(Number n) {
    return (n-*this).isNegative();
}

bool Number::operator<=(Number n) {
    return (*this-n).isNegative() || *this == n;
}

bool Number::operator>=(Number n) {
    return (n-*this).isNegative() || *this == n;
}

Number Number::operator/(Number n) {

    return (*this) * n.inverse();

}

Number Number::inverse() {

    double tmp;

    //std::cout << "Inverting " << this->val.frac.p << "/" << this->val.frac.q << std::endl;

    switch (type) {

        case NUM_N:
            return Number((long)1, val.n);

        case NUM_Z:
        case NUM_Q:
            return Number(val.frac.q, val.frac.p);

        case NUM_R:
            return Number(1.0 / val.comp.real);

        case NUM_C:
            tmp = val.comp.real * val.comp.real + val.comp.img * val.comp.img;
            return Number(val.comp.real / tmp, -val.comp.img / tmp);

        case NUM_C_EULER:
            return Number(NUM_C_EULER, 1.0 / val.comp.real, - val.comp.img);

    }

}

long pgcd(long a, long b) {

    if (!(a % b)) return b;
    return pgcd(b, a % b);

}

void Number::simplfyFraction() {

    //std::cout << "simplifying " << *this << std::endl;

    if (type != NUM_Q) return;
    long f = pgcd(abs(val.frac.p), abs(val.frac.q));

    long t = val.frac.p * val.frac.q;

    val.frac.p = abs(val.frac.p) / f;
    val.frac.q = abs(val.frac.q) / f;

    if (t < 0) {
        val.frac.p *= -1;
    }

}

Number::operator int() {
    if (type >= NUM_Q) {
        throw std::runtime_error("Cannot convert to int");
    }
    switch (type) {
        case NUM_N:
            return val.n;
        case NUM_Z:
            return val.frac.p;
        default:
            return 0;
    }
}

Number::operator double() {
    /*if (type >= NUM_C) {
        throw std::runtime_error("Cannot convert to int");
    }*/
    switch (type) {
        case NUM_N:
            return val.n;
        case NUM_Z:
            return val.frac.p;
        case NUM_Q:
            return (double) val.frac.p / (double) val.frac.q;
        case NUM_R:
            return val.comp.real;
        default:
            return val.comp.real;
    }
}

Number::operator bool() {
    return !(*this == Number(0));
}

void Number::toStream(std::ostream& stream) {

    switch (type) {

        case NUM_N:
            stream << val.n;
            break;

        case NUM_Z:
            stream << val.frac.p;
            break;

        case NUM_Q:
            if (val.frac.q == 1)
                stream << val.frac.p;
            else
                stream << val.frac.p << "/" << val.frac.q;
            break;

        /*case NUM_ROOT:
            stream << "sqrt(" << val.frac.p << "/" << val.frac.q << ")";*/

        case NUM_R:
            stream.precision(std::numeric_limits<double>::max_digits10);
            stream << val.comp.real;
            break;

        case NUM_C:
            stream.precision(std::numeric_limits<double>::max_digits10);
            if (abs(val.comp.img) <= 1e-16)
                stream << val.comp.real;
            else
                stream << val.comp.real << (val.comp.img >= 0 ? "+" : "") << val.comp.img << "i" ;
            break;

        case NUM_C_EULER:
            stream.precision(std::numeric_limits<double>::max_digits10);
            stream << val.comp.real << "e^(" << val.comp.img << "i)";
            break;

        default:
            throw std::runtime_error("Unknown number type");


    }

}

std::string Number::toTeXFormat() {

    std::stringstream stream;

    switch (type) {

        case NUM_N:
            stream << val.n;
            break;

        case NUM_Z:
            stream << val.frac.p;
            break;

        case NUM_Q:
            if (val.frac.q == 1)
                stream << val.frac.p;
            else
                stream << "\\frac{" << val.frac.p << "}{" << val.frac.q << "}";
            break;

        /*case NUM_ROOT:
            stream << "sqrt(" << val.frac.p << "/" << val.frac.q << ")";*/

        case NUM_R:
            stream.precision(std::numeric_limits<double>::digits10);
            stream << val.comp.real;
            break;

        case NUM_C:
            stream.precision(std::numeric_limits<double>::digits10);
            if (abs(val.comp.img) <= 1e-16)
                stream << val.comp.real;
            else
                stream << val.comp.real << (val.comp.img >= 0 ? "+" : "") << val.comp.img << "i" ;
            break;

        case NUM_C_EULER:
            stream.precision(std::numeric_limits<double>::digits10);
            stream << val.comp.real << "e^(" << val.comp.img << "i)";
            break;

        default:
            throw std::runtime_error("Unknown number type");


    }

    return stream.str();
}

Number Number::fromString(std::string str) {

    if (str[0] >= 'a' && str[0] <= 'z') {

        if (!str.compare("pi")) {
            return Number(M_PI);
        } else if (!str.compare("e")) {
            Number n = Number(exp(1));
            return n;
        } else if (!str.compare("i")) {
            return Number(0.0, 1.0);
        }

    }

    if (str.find('.') != std::string::npos || str.find('e') != std::string::npos) {

        char * oldLocale = setlocale(LC_NUMERIC, NULL);
        setlocale(LC_NUMERIC, "en_US");

        Number tmp((double)atof(str.c_str()));

        setlocale(LC_NUMERIC, oldLocale);

        return tmp;
    }

    long tmp = atol(str.c_str());

    //std::cout << "tmpVal = " << tmp << std::endl;

    return Number(tmp);

}

Number pow(Number x, unsigned long int y) {
    if (!y) return Number((unsigned long) 1);
    return x * pow(x, y-1);
}

Number pow(Number x, long int y) {
    if (y < 0) return pow(x.inverse(), -y);
    if (!y) return Number((unsigned long) 1);
    return x * pow(x, y-1);
}

double pow(double d, double y) {
    double tmp = exp(log(d) * y);
    return tmp;
}

Number Number::power(Number x, Number y) {

    Number n;

    switch (y.getType()) {

        case NUM_N:
            return pow(x, y.val.n);

        case NUM_Z:
            return pow(x, y.val.frac.p);

        case NUM_Q:
            y.toType(NUM_R);
        case NUM_R:
            if (x.type < NUM_R)
                x.toType(NUM_R);
            return Number(pow(x.val.comp.real, y.val.comp.real));

        case NUM_C:
            if (x.type < NUM_R)
                x.toType(NUM_R);
            n = Number(NUM_C_EULER, exp(log(x.val.comp.real) * y.val.comp.real), log(x.val.comp.real) * y.val.comp.img);
            return n;

    }

    return Number((unsigned long) 1);

}

Number pow(Number x, Number y) {

    return Number::power(x, y);

}

std::ostream& operator<<(std::ostream& stream, Number num) {

    num.toStream(stream);
    return stream;

}

std::shared_ptr<Expression> Number::asExpression() {

    std::shared_ptr<Expression> ptr;

    char * oldLocale = setlocale(LC_NUMERIC, NULL);
    setlocale(LC_NUMERIC, "en_US");

    switch (this->type) {

        case NUM_N:
        case NUM_Z:
        case NUM_R:
            ptr = std::shared_ptr<Expression>(new Expression(nullptr, std::string("#").append(this->toTeXFormat()).c_str()));
            break;

        case NUM_Q:
            ptr = std::shared_ptr<Expression>(new Expression(Operator::getFromSymbol("/"), new Expression(nullptr, std::string("#").append(std::to_string(val.frac.p)).c_str()), new Expression(nullptr, std::string("#").append(std::to_string(val.frac.q)).c_str())));
            break;

        case NUM_C:
            ptr = std::shared_ptr<Expression>(new Expression(Operator::getFromSymbol("+"), new Expression(nullptr, std::string("#").append(std::to_string(val.comp.real)).c_str()), new Expression(nullptr, std::string("#").append(std::to_string(val.comp.img)).c_str())));
            break;

    }

    setlocale(LC_NUMERIC, oldLocale);

    return ptr;

}

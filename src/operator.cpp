#include "operator.h"

#include <iostream>
#include <ostream>
#include <sstream>
#include "expression.h"
#include "function.h"
#include "set.h"
#include <string.h>
#include "computeresult.h"
#include "implementation.h"

std::map<std::string, Operator*> Operator::opsBySymbol;

Operator::Operator(std::string symbol, int opCount, bool il) : Operator(symbol, opCount, 1, 1, il) {

    this->implementation = nullptr;

}

Operator::Operator(std::string symbol, int opCount, int precedence, int comp, bool il) : Operator(symbol, opCount, precedence, comp, il, true){

}

Operator::Operator(std::string symbol, int opCount, int precedence, int comp, bool il, bool addToMap) {
    this->symbol = symbol;
    this->isInline = il;
    this->opCount = opCount;
    this->precedence = precedence;
    this->complexity = comp;

    if (addToMap)
        opsBySymbol[symbol] = this;

    this->implementation = nullptr;

}

Operator::~Operator(){
    opsBySymbol[this->symbol] = nullptr;
}

Operator * Operator::getFromSymbol(std::string symbol) {

    if (opsBySymbol.find(symbol) == opsBySymbol.end())
        return nullptr;

    return opsBySymbol[symbol];
}

void Operator::createDefaults() {
    //Operator * op = new Operator("+", 2, 1, true);
    //Operator * mu = new Operator("*", 2, 1, true);
}

void Operator::setPrecedence(int p) {
    this->precedence = p;
}

void Operator::deleteDefaults() {
    for (auto it = opsBySymbol.begin(); it != opsBySymbol.end(); ++it) {
        if (it->second)
            delete it->second;
    }
}

int Operator::getOperandCount() {
    return opCount;
}

int Operator::getPrecedence() {
    return precedence;
}

bool Operator::getInline() {
    return isInline;
}

void Operator::writeToStream(std::ostream& stream, std::vector<std::shared_ptr<Expression>> operandSymbols) {

    if (getOperandCount() >= 0 && operandSymbols.size() != this->getOperandCount())
        throw std::runtime_error(std::string("Operator mismatch: wrong argument count for '").append(this->symbol).append("' ").append(std::to_string(operandSymbols.size())).append(" != ").append(std::to_string(this->getOperandCount())));

    if (!isInline) {

        stream << symbol << "(";
        if (operandSymbols.size()){
            stream << operandSymbols[0];
            for (int i = 1; i < operandSymbols.size(); ++i)
                stream << ", " << operandSymbols[i];
        }
        stream << ")";
        return;

    }

    stream << "(" << operandSymbols[0] << " " << symbol << " " << operandSymbols[1] << ")";

}

std::string& Operator::getSymbol() {
    return symbol;
}

void Operator::setTexFormat(std::string format) {
    this->texFormat = format;
}

void Operator::setImplementation(Implementation::Function * impl) {
    this->implementation = impl;
}

std::string Operator::formatTeX(std::vector<std::shared_ptr<Expression>> operands) {

    std::ostringstream stream;

    formatTeX(operands, stream);

    return stream.str();

}

void formatExpression(std::shared_ptr<Expression> exp, std::ostream& stream) {

    if (!exp->getOperator()) {
        //std::cout << "formating non-operator expression" << std::endl;
        stream << exp->format();
        //std::cout << std::string("\\").append(exp->getSymbol().substr(1));
        //std::cout << exp->format();
        return;
    }

    exp->getOperator()->formatTeX(exp->getOperands(), stream);

}

int Operator::getVisualComplexity() {
    return complexity;
}

void Operator::formatTeX(std::vector<std::shared_ptr<Expression>> operands, std::ostream& stream) {

    //std::cout << symbol << " " << std::endl;//<< texFormat << std::endl;
    std::string rest = texFormat;
    size_t pos;
    while ((pos = rest.find("%")) != std::string::npos) {

        stream << rest.substr(0, pos);
        //std::cout << rest.substr(0, pos);
        rest = rest.substr(pos);
        pos = 1;
        while (rest[pos] >= '0' && rest[pos] <= '9') pos++;

        std::string numStr = rest.substr(1, pos);
        int argNum = atoi(numStr.c_str());

        if (argNum < 0 || argNum >= operands.size())
            throw std::runtime_error("Wrong argument index in format string");

        if (isInline && operands[argNum]->getOperator() && operands[argNum]->getOperator()->precedence <= this->precedence && operands[argNum]->getOperator() != this)
            stream << "\\left(";
        formatExpression(operands[argNum], stream);
        if (isInline && operands[argNum]->getOperator() && operands[argNum]->getOperator()->precedence <= this->precedence && operands[argNum]->getOperator() != this)
            stream << "\\right)";

        if (pos == std::string::npos)
            break;

        rest = rest.substr(pos);

    }

    stream << rest;

}

ComputeResult Operator::compute(std::vector<std::shared_ptr<Expression>> operands, std::map<std::string, ComputeResult>& varValues) {

    if (!symbol.compare("abs")) {

        ComputeResult tmp = operands[0]->getValue(varValues);
        if (tmp.isVector) return tmp;

        return ComputeResult(tmp.n.isNegative() ? Number(-1) * tmp.n : tmp.n);

    } else if (!symbol.compare("sum")) {
        ComputeResult res;
        res = Number((unsigned long int) 0);
        Number upperLimmit = operands[2]->getValue(varValues).n;
        upperLimmit.toType(Number::NUM_Z);
        for (int i = operands[1]->getValue(varValues).n; i <= (int) upperLimmit; ++i) {
            varValues[operands[0]->getSymbol()] = Number((long)i);
            res = res + operands[3]->getValue(varValues);
        }
        varValues.erase(operands[0]->getSymbol());
        return res;
    } else if (!symbol.compare("in")) {
        ComputeResult res;
        res = Number(((Set*) operands[1].get())->contains(operands[0]) ? (unsigned long) 1 : (unsigned long) 0);
        return res;
    }

    if (this->implementation) {
        std::vector<ComputeResult> args(operands.size());
        for (int i = 0; i < operands.size(); ++i) {
            args[i] = operands[i]->getValue(varValues);
        }
        return (*this->implementation)(args);
    }

    throw std::runtime_error("Unknown operator");

}

std::map<std::string, ComputeResult>& Operator::getVars(std::map<std::string, ComputeResult>& vars, int it, std::vector<std::shared_ptr<Expression>> operands) {

    if (!symbol.compare("sum")) {

        if (it == 3 || it == 0) {
            vars[operands[0]->getSymbol()] = operands[1]->getValue(vars);
        }

    }

    return vars;

}

void Operator::cleanupVars(std::map<std::string, ComputeResult>& vars, int it, std::vector<std::shared_ptr<Expression>> operands) {

    if (!symbol.compare("sum")) {

        if (it == 3 || it == 0) {
            vars.erase(operands[0]->getSymbol());
        }

    }

}

bool Operator::canCompute(std::map<std::string, ComputeResult>& varValues, std::vector<std::shared_ptr<Expression>>& operands) {

    if (!this->hasImplementation()) {
        return false;
    }

    bool b = true;
    for (int i = 0; i < operands.size(); ++i) {
        b &= operands[i]->canCompute(this->getVars(varValues, i, operands));
        //std::cout << "Testing " << i << " yields " << b << std::endl;
        this->cleanupVars(varValues, i, operands);
    }
    return b;

}

bool Operator::hasImplementation() {

    if (!symbol.compare("<"))
        return true;
    else if (!symbol.compare("sum"))
        return true;
    else if (!symbol.compare("abs"))
        return true;
    else if (!symbol.compare("in"))
        return true;

    if (this->implementation) return true;

    return false;

}

int getElementType(char * rData, void ** retVal) {

    std::string str(rData);

    /*Function * fn = Function::getByName(str);
    if (fn) {
        *retVal = (void *) fn;
        return ELEM_FUNCTION;
    }*/

    Operator * op = Operator::getFromSymbol(str);

    if (op) {
        *retVal = (void *) op;
        if (op->getInline())
            return ELEM_OP_INLINE;
        return ELEM_OP;
    }

    *retVal = (void *) strdup(rData);
    return ELEM_SYMBOL;

}

void * declareOperator(char * symbol, int opCount, int prec, int complexity, int isInline) {

    return new Operator(std::string(symbol), opCount, prec, complexity, isInline);

}

void operatorSetFormat(void * tmp, char * str) {
    std::string format(str);
    Operator * op = (Operator *) tmp;
    op->setTexFormat(format.substr(1, format.length()-2));
}

void symbolSetFormat(char * tname, char * str) {

    std::string name(tname);
    free(tname);

    Operator * op = Operator::getFromSymbol(name);
    if (!op) {
        op = Function::getByName(name);
        if (!op) return;
    }

    std::string format(str);
    op->setTexFormat(format.substr(1, format.length()-2));
}

std::shared_ptr<Expression> Operator::expand(std::shared_ptr<Expression> exp, std::vector<std::shared_ptr<Expression>> operands) {
    return exp;
}

extern Implementation::Script * getImplementationScript();

void operatorAddImplementation(char * symbol, char * name) {

    Implementation::Script * sc = getImplementationScript();

    //std::cerr << "Setting implementation of " << std::string(symbol) << " to " << std::string(name).substr(1,strlen(name)-2) << std::endl;

    Operator * op = Operator::getFromSymbol(std::string(symbol));
    op->setImplementation(sc->getFunction(std::string(name).substr(1,strlen(name)-2)));

}

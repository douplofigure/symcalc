#include "vector.h"

#include "dvector.h"

Vector::Vector(std::vector<std::shared_ptr<Expression>> coords) {

    this->operands = coords;

}

Vector::Vector(std::vector<Expression *> coords) {

    this->operands = std::vector<std::shared_ptr<Expression>>(coords.size());
    for (int i = 0; i < coords.size(); ++i) {
        operands[i] = std::shared_ptr<Expression>(coords[i]);
    }

}

Vector::~Vector() {
    //dtor
}

std::shared_ptr<Expression> Vector::simplfy(std::vector<Rule*> rules) {

    std::vector<std::shared_ptr<Expression>> res(this->operands.size());
    for (int i = 0; i < operands.size(); ++i) {

        res[i] = operands[i]->simplfy(rules);

    }
    return std::shared_ptr<Expression>(new Vector(res));

}

Expression::Type Vector::getType() {
    return Expression::VECTOR;
}

std::string Vector::format() {

    std::string res = "\\begin{pmatrix}[1.5]";

    for (int i = 0; i < operands.size(); ++i) {
        res.append(operands[i]->format()).append(" \\\\ ");
    }

    res.append(" \\end{pmatrix}");

    return res;

}

int Vector::getComplexityScore() {

    int res = 0;
    for (int i = 0; i < operands.size(); ++i) {
        res += operands[i]->getComplexityScore();
    }
    return res;

}

bool Vector::canCompute(std::map<std::string, ComputeResult>& varValues) {

    bool b = true;
    for (int i = 0; i < operands.size(); ++i) {
        b &= operands[i]->canCompute(varValues);
    }
    return b;

}
ComputeResult Vector::getValue(std::map<std::string, ComputeResult>& varValues) {
    using namespace Numeric;
    std::vector<Number> data(this->operands.size());
    for (int i = 0; i < this->operands.size(); ++i) {
        data[i] = this->operands[i]->getValue(varValues).n;
    }

    dVector<Number> resVec = dVector<Number>(data.size(), data.data());

    return ComputeResult(true, resVec);

}

void Vector::writeToStream(std::ostream& stream) {

    stream << "[" << operands[0];
    for (int i = 1; i < operands.size(); ++i) {
        stream << ", " << operands[i];
    }
    stream << "]";

}

int Vector::getDimension() {
    return operands.size();
}

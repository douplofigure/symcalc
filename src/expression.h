#ifndef EXPRESSION_H
#define EXPRESSION_H

#include <string>
#include <vector>
#include <memory>
#include "operator.h"
#include "number.h"
#include "dvector.h"
#include "computeresult.h"
#include "cheader.h"

class Rule;

class Expression {

    public:

        typedef enum exp_type_e {

            NONE,
            SET,
            VECTOR,
            CONSTANT,

        } Type;

        Expression(Operator * op, ...);
        Expression(Operator * op, std::vector<std::shared_ptr<Expression>> operands);
        Expression(Operator * op, std::vector<Expression*> operands);
        Expression(std::string op, ...);
        Expression(ComputeResult val);
        //Expression(Expression * exp);
        virtual ~Expression();

        /**Accepts RPN (reverse polish notation)**/
        static std::shared_ptr<Expression> fromString(std::string str);

        virtual void writeToStream(std::ostream& stream);
        virtual int getComplexityScore();

        virtual std::shared_ptr<Expression> simplfy(std::vector<Rule *> rules);

        Operator * getOperator();
        std::string getSymbol();
        std::vector<std::shared_ptr<Expression>> getOperands();

        bool operator== (Expression e);

        bool isSame(std::shared_ptr<Expression> e);
        bool isSame(Expression * e);
        bool functionOf(std::string symbol);
        bool functionOf(std::shared_ptr<Expression> exp);
        bool functionOf(Expression * exp);
        bool isConstant();
        bool isValueExp();
        ComputeResult getConstValue();
        virtual bool applyRules();

        virtual Type getType();

        int getOpPrecedence();
        void setOpPrecedence(int prec);

        virtual ComputeResult getValue(std::map<std::string, ComputeResult>& varValues);
        virtual bool canCompute(std::map<std::string, ComputeResult>& varValues);

        std::shared_ptr<Expression> expand();

        virtual std::string format();
        virtual std::shared_ptr<Expression> getBestNeighbor(std::vector<Rule *> rules);
        std::shared_ptr<Expression> replaceChild(std::shared_ptr<Expression> toReplace, std::shared_ptr<Expression> exp);
        std::shared_ptr<Expression> replaceChild(Expression * toReplace, std::shared_ptr<Expression> exp);
        virtual std::vector<std::shared_ptr<Expression>> getNeighbors(std::vector<Rule *> rules);
        static std::shared_ptr<Expression> buildExpression(std::shared_ptr<Expression> exp, std::map<std::string, std::shared_ptr<Expression>>& relation);
        static std::shared_ptr<Expression> buildExpression(std::shared_ptr<Expression> exp, std::map<std::string, Expression *>& relation);

        virtual std::shared_ptr<Expression> createCopy();

    protected:

        Expression();

        std::vector<std::shared_ptr<Expression>> operands;
        Operator * op;
        std::string symbol;
        bool isValue;
        ComputeResult value;

    private:
        int opPrecendence;

};

std::ostream& operator<< (std::ostream& stream, Expression * exp);

typedef struct expression_list_t {
    std::vector<Expression *> expressions;
} EXP_LIST;

#endif // EXPRESSION_H

#ifndef FUNCTION_H
#define FUNCTION_H
#include <map>
#include <vector>
#include <memory>
#include <functional>

#include "number.h"
#include "operator.h"
#include "computeresult.h"

class Expression;
class Rule;
class Set;

class Function : public Operator {

    public:
        Function(std::string name, Set * domain, Set * images);
        Function(std::string name, std::vector<std::string> vars);
        Function(std::string name, std::vector<std::string> vars, std::shared_ptr<Expression> exp);
        virtual ~Function();


        virtual ComputeResult compute(std::vector<std::shared_ptr<Expression>> operands, std::map<std::string, ComputeResult>& varValues);
        virtual int getOperandCount() override;
        virtual bool checkArgumentCount(int inCount);
        bool hasImplementation() override;

        std::shared_ptr<Expression> getExpression();
        std::shared_ptr<Expression> getCall();
        void setExpression(std::shared_ptr<Expression> exp);
        void setExpression(Expression * exp);
        std::map<std::string, Expression *> buildCorrespondance(std::vector<Expression *> args);

        void addSpecialCase(std::vector<Expression *> args, Expression * val);
        std::shared_ptr<Expression> expand(std::shared_ptr<Expression> exp, std::vector<std::shared_ptr<Expression>> operands);

        static Function * getByName(std::string name);
        static void deleteAll();
        static bool isFunction(std::shared_ptr<Expression> exp);
        static ComputeResult eval(std::shared_ptr<Expression> f, std::vector<std::shared_ptr<Expression>> args, std::map<std::string, ComputeResult>& varValues);
        void setVars(std::vector<std::string> vars);

    protected:

        Function(std::string name, std::vector<std::string> vars, bool addToMap);
        Function(std::string name, int varCount, bool addToMap);
        Function(std::string name, std::vector<std::string> vars, std::shared_ptr<Expression> exp, bool addToMap);

    private:

        static std::map<std::string, Function *> functionsByName;

        std::string name;
        std::vector<std::string> vars;
        std::vector<std::vector<ComputeResult>> specialCases;
        std::vector<ComputeResult> specialValues;
        std::shared_ptr<Expression> expression;
        Set * domain;
        Set * images;

};

class TempFunction : public Function {

    public:
        TempFunction(std::string name, std::vector<std::string> vars);

};

class BuiltinFunction : public Function {

    public:
        BuiltinFunction(std::string name, int varCount, std::function<ComputeResult(std::vector<std::shared_ptr<Expression>>, std::map<std::string, ComputeResult>& varValues)> body);

        ComputeResult compute(std::vector<std::shared_ptr<Expression>> operands, std::map<std::string, ComputeResult>& varValues) override;
        bool checkArgumentCount(int inCount) override;
        virtual int getOperandCount() override;
        virtual bool hasImplementation() override;
        virtual bool canCompute(std::map<std::string, ComputeResult>& varValues, std::vector<std::shared_ptr<Expression>>& operands) override;

        static void createBuiltins();

    private:
        std::function<ComputeResult(std::vector<std::shared_ptr<Expression>>, std::map<std::string, ComputeResult>& varValues)> impl;
        int varCount;

        static std::vector<BuiltinFunction*> builtins;

};

#endif // FUNCTION_H

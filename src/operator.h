#ifndef OPERATOR_H
#define OPERATOR_H

#include <string>
#include <vector>
#include <map>
#include <memory>

#include "number.h"
#include "computeresult.h"
#include "implfunction.h"

class Expression;
std::ostream& operator<< (std::ostream& stream, Expression * exp);

class Operator {
    public:
        Operator(std::string symbol, int opCount, bool inLine);
        Operator(std::string symbol, int opCount, int precedence, int complexity, bool inLine);
        virtual ~Operator();

        virtual int getOperandCount();
        virtual int getPrecedence();
        virtual int getVisualComplexity();
        virtual void writeToStream(std::ostream& stream, std::vector<std::shared_ptr<Expression>> operandSymbols);

        bool getInline();
        std::string& getSymbol();

        static Operator * getFromSymbol(std::string symbol);
        static void createDefaults();
        static void deleteDefaults();

        void setTexFormat(std::string format);
        std::string formatTeX(std::vector<std::shared_ptr<Expression>> operands);
        void formatTeX(std::vector<std::shared_ptr<Expression>> operands, std::ostream& stream);

        virtual ComputeResult compute(std::vector<std::shared_ptr<Expression>> operands, std::map<std::string, ComputeResult>& varValues);
        virtual std::map<std::string, ComputeResult>& getVars(std::map<std::string, ComputeResult>& vars, int it, std::vector<std::shared_ptr<Expression>> operands);
        virtual void cleanupVars(std::map<std::string, ComputeResult>& vars, int it, std::vector<std::shared_ptr<Expression>> operands);
        virtual bool hasImplementation();
        virtual bool canCompute(std::map<std::string, ComputeResult>& vars, std::vector<std::shared_ptr<Expression>>& operands);
        virtual std::shared_ptr<Expression> expand(std::shared_ptr<Expression> exp, std::vector<std::shared_ptr<Expression>> operands);

        void setImplementation(Implementation::Function * impl);

    protected:

        Operator(std::string symbol, int opCount, int precedence, int comp, bool il, bool addToMap);

        void setPrecedence(int p);

    private:
        bool isInline;
        std::string symbol;
        std::string texFormat;

        int precedence;
        int opCount;
        int complexity;

        Implementation::Function * implementation;

        static std::map<std::string, Operator*> opsBySymbol;

};

#endif // OPERATOR_H

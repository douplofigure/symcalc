#include "rule.h"

#include <map>
#include <iostream>
#include "expression.h"
#include "ioctl.h"

std::vector<Rule *> Rule::rules;

Rule::Rule(std::vector<std::shared_ptr<Expression>> expr) {

    this->equivExpr = std::vector<std::shared_ptr<Expression>>(expr.size());
    for (int i = 0; i < expr.size(); ++i) {
        equivExpr[i] = expr[i];
    }

    rules.push_back(this);

}

Rule::Rule(std::vector<Expression *> expr) {

    this->equivExpr = std::vector<std::shared_ptr<Expression>>(expr.size());
    for (int i = 0; i < expr.size(); ++i) {
        equivExpr[i] = std::shared_ptr<Expression>(expr[i]);
    }

    rules.push_back(this);

}

Rule::Rule() {
    rules.push_back(this);
}

Rule::Rule(std::vector<std::shared_ptr<Expression>> expr, bool b) {

    this->equivExpr = std::vector<std::shared_ptr<Expression>>(expr.size());
    for (int i = 0; i < expr.size(); ++i) {
        equivExpr[i] = expr[i];
    }

    if (b)
        rules.push_back(this);

}

Rule::~Rule() {

}

bool checkDependencies(std::string symbol, Expression * exp, std::map<std::string, Expression *>& relation) {

    if (exp->isValueExp()) return true;

    size_t lsBegin = symbol.find('[');
    if (lsBegin == std::string::npos) return true; //No dependencies to check

    size_t endPos = 0;
    std::string data = symbol.substr(lsBegin + 1);

    bool b = true;

    do {
        endPos = data.find(',');

        if (endPos == std::string::npos) {
            endPos = data.find(']');
            std::string varStr = data.substr(0, endPos);

            if (varStr[0] == '!') {
                if (relation.find(varStr.substr(1)) != relation.end())
                    b &= !exp->functionOf(relation[varStr.substr(1)]);
            } else {
                if (relation.find(varStr) != relation.end())
                    b &= exp->functionOf(relation[varStr]);
            }

            break;
        }

        std::string varStr = data.substr(0, endPos);

        if (varStr[0] == '!') {
            if (relation.find(varStr.substr(1)) != relation.end())
                b &= !exp->functionOf(relation[varStr.substr(1)]);
        } else {
            if (relation.find(varStr) != relation.end())
                b &= exp->functionOf(relation[varStr]);
        }

        data = data.substr(0, endPos);
    } while (endPos != std::string::npos);

    return b;

}

bool matchSymbolExpression(std::string symbol, Expression * exp, std::map<std::string, Expression*>& relation) {

    auto pos = relation.find(symbol);
    if (pos == relation.end()) {
        //std::cout << "Symbol not in use" << std::endl;

        if (checkDependencies(symbol, exp, relation))
            relation[symbol] = exp;
        else
            return false;
        //std::cout << "Returning" << std::endl;
        return true;
    } else if (relation[symbol]->isSame(exp)) {
        return true;
    }
    return false;

}

bool checkDependencies(std::string symbol, std::shared_ptr<Expression> exp, std::map<std::string, std::shared_ptr<Expression>>& relation) {

    if (exp->isValueExp()) return true;

    size_t lsBegin = symbol.find('[');
    if (lsBegin == std::string::npos) return true; //No dependencies to check

    size_t endPos = 0;
    std::string data = symbol.substr(lsBegin + 1);

    bool b = true;

    do {
        endPos = data.find(',');

        if (endPos == std::string::npos) {
            endPos = data.find(']');
            std::string varStr = data.substr(0, endPos);

            if (varStr[0] == '!') {
                if (relation.find(varStr.substr(1)) != relation.end())
                    b &= !exp->functionOf(relation[varStr.substr(1)]);
            } else {
                if (relation.find(varStr) != relation.end())
                    b &= exp->functionOf(relation[varStr]);
            }

            break;
        }

        std::string varStr = data.substr(0, endPos);

        if (varStr[0] == '!') {
            if (relation.find(varStr.substr(1)) != relation.end())
                b &= !exp->functionOf(relation[varStr.substr(1)]);
        } else {
            if (relation.find(varStr) != relation.end())
                b &= exp->functionOf(relation[varStr]);
        }

        data = data.substr(0, endPos);
    } while (endPos != std::string::npos);

    return b;

}

bool matchSymbolExpression(std::string symbol, std::shared_ptr<Expression> exp, std::map<std::string, std::shared_ptr<Expression>>& relation) {

    auto pos = relation.find(symbol);
    if (pos == relation.end()) {
        //std::cout << "Symbol not in use" << std::endl;

        if (checkDependencies(symbol, exp, relation))
            relation[symbol] = exp;
        else
            return false;
        //std::cout << "Returning" << std::endl;
        return true;
    } else if (relation[symbol]->isSame(exp)) {
        return true;
    }
    return false;

}

bool getCorrespondance(Expression * mask, Expression * exp, std::map<std::string, Expression *>& relation) {

    //std::cout << "matching " << mask << " and " << exp << std::endl;

    if (!mask->getOperator()) {

        if (mask->isValueExp()) {
            if (!exp->isValueExp()) return false;
            return mask->getConstValue() == exp->getConstValue();
        }

        if (mask->getSymbol()[0] == '#') {

            if (!exp->getOperator() && !(mask->getSymbol().compare(exp->getSymbol()))) return true;
            return false;

        }

        return matchSymbolExpression(mask->getSymbol(), exp, relation);
    }

    if (!exp->getOperator()) return false;

    if (mask->getOperands().size() != exp->getOperands().size())
        return false;

    bool b = true;

    for (int i = 0; i < mask->getOperands().size(); ++i) {
        b &= getCorrespondance(mask->getOperands()[i].get(), exp->getOperands()[i].get(), relation);
    }

    if (mask->getOperator()->getSymbol()[0] == '_') {
        //std::cout << "mask has operator " << mask->getOperator()->getSymbol() << " matched with " << exp->getOperator()->getSymbol() << std::endl;

        relation[mask->getOperator()->getSymbol()] = new Expression(nullptr, exp->getOperator()->getSymbol().c_str());

        return b;

    }
    /*std::cout << "exp has operator ";
    exp->getOperator()->writeToStream(std::cout, exp->getOperands());
    std::cout << std::endl;*/

    if (!(mask->getOperator() == exp->getOperator())) return false;

    //std::cout << "Same operator" << std::endl;

    // Different amount of operands


    //std::cout << "Returning " << b << std::endl;
    return b;

}

bool getCorrespondance(std::shared_ptr<Expression> mask, std::shared_ptr<Expression> exp, std::map<std::string, std::shared_ptr<Expression>>& relation) {

    //std::cout << "matching " << mask << " and " << exp << std::endl;

    if (!mask->getOperator()) {

        if (mask->isValueExp()) {
            if (!exp->isValueExp()) return false;
            return mask->getConstValue() == exp->getConstValue();
        }

        if (mask->getSymbol()[0] == '#') {

            if (!exp->getOperator() && !(mask->getSymbol().compare(exp->getSymbol()))) return true;
            return false;

        }

        return matchSymbolExpression(mask->getSymbol(), exp, relation);
    }

    if (!exp->getOperator()) return false;

    if (mask->getOperands().size() != exp->getOperands().size())
        return false;

    bool b = true;

    for (int i = 0; i < mask->getOperands().size(); ++i) {
        b &= getCorrespondance(mask->getOperands()[i], exp->getOperands()[i], relation);
    }

    if (mask->getOperator()->getSymbol()[0] == '_') {
        //std::cout << "mask has operator " << mask->getOperator()->getSymbol() << std::endl;

        relation[mask->getOperator()->getSymbol()] = std::shared_ptr<Expression>(new Expression(nullptr, exp->getOperator()->getSymbol().c_str()));

        return b;

    }
    if (!(mask->getOperator() == exp->getOperator())) return false;

    //std::cout << "Same operator" << std::endl;

    // Different amount of operands

    //std::cout << "Returning " << b << std::endl;
    return b;

}

bool Rule::conditionsMet(std::map<std::string, Expression *>& correspondance) {

    //std::cout << "Testing" << std::endl;

    if (!condition) return true;

    //std::cout << "Testing condition " << this->condition << std::endl;

    std::map<std::string, ComputeResult> varValues;

    //std::cout << "Map: " << std::endl;
    /*for (auto it = correspondance.begin(); it != correspondance.end(); ++it) {
        std::cout << it->first << " -> " << it->second << std::endl;
    }*/

    std::shared_ptr<Expression> tmp = Expression::buildExpression(condition, correspondance);
    //std::cout << tmp << " " << tmp->canCompute(varValues) << std::endl;
    if (!tmp->canCompute(varValues)) return false;
    ComputeResult r = tmp->getValue(varValues);
    //std::cout << r.n.getType() << std::endl;
    return (bool) r.n;

}

bool Rule::conditionsMet(std::map<std::string, std::shared_ptr<Expression>>& correspondance) {

    //std::cout << "Testing" << std::endl;

    if (!condition) return true;

    //std::cout << "Testing condition " << this->condition << std::endl;

    std::map<std::string, ComputeResult> varValues;
    std::shared_ptr<Expression> tmp = Expression::buildExpression(condition, correspondance);
    //std::cout << tmp << " " << tmp->canCompute(varValues) << std::endl;
    if (!tmp->canCompute(varValues)) return false;
    ComputeResult r = tmp->getValue(varValues);
    //std::cout << r << std::endl;
    return (bool) r.n;

}

void Rule::setCondition(std::shared_ptr<Expression> exp) {
    //std::cout << "Setting rule condition " << exp << std::endl;
    this->condition = exp;
}

bool Rule::matches(std::shared_ptr<Expression> exp) {

    for (int i = 0; i < this->equivExpr.size(); ++i) {
        std::map<std::string, Expression *> correspondance;
        //std::cout << "Trying " << equivExpr[i] << std::endl;
        if (getCorrespondance(this->equivExpr[i].get(), exp.get(), correspondance)){


            return this->conditionsMet(correspondance);

        }

    }
    //std::cout << "Did not find match" << std::endl;
    return false;

}

bool Rule::matches(Expression * exp) {

    for (int i = 0; i < this->equivExpr.size(); ++i) {
        std::map<std::string, Expression*> correspondance;
        //std::cout << "Trying " << equivExpr[i] << std::endl;
        if (getCorrespondance(this->equivExpr[i].get(), exp, correspondance))
            return this->conditionsMet(correspondance);

    }
    //std::cout << "Did not find match" << std::endl;
    return false;

}

std::vector<std::shared_ptr<Expression>> Rule::applyAll(std::shared_ptr<Expression> exp) {

    std::map<std::string, std::shared_ptr<Expression>> correspondance;

    for (int i = 0; i < this->equivExpr.size(); ++i) {
        correspondance = std::map<std::string, std::shared_ptr<Expression>>();
        if (getCorrespondance(this->equivExpr[i], exp, correspondance))
            break;

    }

    std::vector<std::shared_ptr<Expression>> res(equivExpr.size());

    for (int i = 0; i < equivExpr.size(); ++i) {

        res[i] = Expression::buildExpression(equivExpr[i], correspondance);

    }

    return res;

}

std::vector<std::shared_ptr<Expression>> Rule::applyAll(Expression * exp) {

    std::map<std::string, Expression *> correspondance;

    for (int i = 0; i < this->equivExpr.size(); ++i) {
        correspondance = std::map<std::string, Expression *>();
        if (getCorrespondance(this->equivExpr[i].get(), exp, correspondance))
            break;

    }

    std::vector<std::shared_ptr<Expression>> res(equivExpr.size());

    for (int i = 0; i < equivExpr.size(); ++i) {

        res[i] = Expression::buildExpression(equivExpr[i], correspondance);

    }

    return res;

}

void Rule::deleteAll() {
    for (int i = 0; i < rules.size(); ++i) {
        delete rules[i];
    }
}

WritingRule::WritingRule(std::shared_ptr<Expression> left, std::shared_ptr<Expression> right) {

    this->left = left;
    this->right = right;

}

WritingRule::WritingRule(Expression * left, Expression * right) {

    this->left = std::shared_ptr<Expression>(left);
    this->right = std::shared_ptr<Expression>(right);

}

WritingRule::~WritingRule() {

}

bool WritingRule::matches(std::shared_ptr<Expression> exp) {

    std::map<std::string, Expression *> corr;

    //std::cerr << "matching " << left << " to " << exp << std::endl;
    bool b = getCorrespondance(left.get(), exp.get(), corr);

    if (!b) return b;

    /*std::cout << "Map: " << std::endl;
    for (auto it = corr.begin(); it != corr.end(); ++it) {
        std::cout << it->first << " -> " << it->second << std::endl;
    }*/

    //std::cerr << "Testing for conditions" << std::endl;
    b &= conditionsMet(corr);

    return b;
}

bool WritingRule::matches(Expression * exp) {

    std::map<std::string, Expression *> corr;

    //std::cerr << "matching " << left << " to " << exp << std::endl;
    bool b = getCorrespondance(left.get(), exp, corr);

    if (!b) return b;

    /*std::cout << "Map: " << std::endl;
    for (auto it = corr.begin(); it != corr.end(); ++it) {
        std::cout << it->first << " -> " << it->second << std::endl;
    }*/

    //std::cerr << "Testing for conditions" << std::endl;
    b &= conditionsMet(corr);

    return b;
}

std::vector<std::shared_ptr<Expression>> WritingRule::applyAll(std::shared_ptr<Expression> exp) {

    std::map<std::string, std::shared_ptr<Expression>> correspondance;
    getCorrespondance(left, exp, correspondance);

    std::vector<std::shared_ptr<Expression>> res(1);
    res[0] = Expression::buildExpression(right, correspondance);

    return res;

}

std::vector<std::shared_ptr<Expression>> WritingRule::applyAll(Expression * exp) {

    std::map<std::string, Expression *> correspondance;
    getCorrespondance(left.get(), exp, correspondance);

    std::vector<std::shared_ptr<Expression>> res(1);
    res[0] = Expression::buildExpression(right, correspondance);

    return res;

}

void ruleAddCondition(void * r, void * tmp) {

    std::shared_ptr<Expression> exp((Expression*) tmp);
    Rule * rule = (Rule*) r;

    rule->setCondition(exp);

}

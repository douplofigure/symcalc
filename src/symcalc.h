#ifndef SYMCALC_H_INCLUDED
#define SYMCALC_H_INCLUDED

#include <queue>
#include <string>
#include <functional>

#include "ioctl.h"
#include "expression.h"

namespace symcalc {

void init(bool loadBuiltins=true);
void cleanup();
void loadFile(std::string fname);

void executeCommand(std::string command);
void pushCommand(std::string command);
void executeBuffer();

void setOutputTeX(bool TeX=true);

void setPrintCallback(std::function<void(void)> callBack);
void clearBuffer();

};

std::queue<char>& operator<<(std::queue<char>& q, std::string str);

#endif // SYMCALC_H_INCLUDED

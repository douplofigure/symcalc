%name-prefix "sym"

%{
#include <stdio.h>
#include <stdlib.h>

#include "cheader.h"

extern FILE * yyin;
extern int symlex();
void symerror(char * msg);

extern char * symtext;
extern int line;
extern void logError(int, char*, char*);

%}

%union {
    void * op;
    void * exp;
    void * expList;
    char * symbol;
    long int i;
    double d;
}

%token<op> OPERATOR_INLINE
%token<op> OPERATOR
%token<op> FUNCTION
%token KW_RULE
%token KW_OPERATOR
%token KW_SIMPLIFY
%token KW_EXPAND
%token<symbol> SYMBOL
%token<i> CONST_INT
%token<i> CONST_BOOL
%token<d> CONST_FLOAT
%token OP_DEF
%token<symbol> CONST_STR
%token KW_TEX_FORMAT
%token KW_NUMSET
%token KW_LET
%token OP_TO
%token KW_PLOT
%token KW_IF
%token KW_CONDITIONAL

%type <exp> expression
%type <expList> expList
%type <expList> expSet
%type <exp> functionDef
%type <exp> set
%type <op> funcHead
%type <exp> functionDeclare
%type <exp> condition
%type <op> rule
%type <exp> value


%left OPERATOR_INLINE

%%

file:
    opDeclare
    |rule
    |conditionalRule
    |simplify
    |texFormat
    |functionDef
    |functionDeclare
    |plotCmd
    |file opDeclare
    |file rule
    |file functionDef
    |file texFormat
    |file simplify
    |file functionDeclare
    |file conditionalRule
    |file plotCmd
    ;

opDeclare:
     KW_OPERATOR '(' SYMBOL ',' CONST_INT ')'                            	                                {declareOperator($3, $5, 1, 1, 0);}
    |KW_OPERATOR '(' SYMBOL ',' CONST_INT ',' CONST_INT ')'                                                 {declareOperator($3, $5, $7, 1, 0);}
    |KW_OPERATOR '(' SYMBOL ',' CONST_INT ',' CONST_INT ',' CONST_INT ')'                                   {declareOperator($3, $5, $7, $9, 0);}
    |KW_OPERATOR '(' SYMBOL ',' CONST_INT ',' CONST_INT ',' CONST_INT ',' CONST_BOOL ')'                    {declareOperator($3, $5, $7, $9, $11);}
    |KW_OPERATOR '(' SYMBOL ',' CONST_INT ',' CONST_INT ',' CONST_INT ',' CONST_BOOL  ',' CONST_STR ')'     {declareOperator($3, $5, $7, $9, $11); operatorAddImplementation($3, $13);}
    ;

texFormat:
	 KW_TEX_FORMAT '(' OPERATOR ',' CONST_STR ')'				{operatorSetFormat($3, $5);}
	|KW_TEX_FORMAT '(' OPERATOR_INLINE ',' CONST_STR ')'		{operatorSetFormat($3, $5);}
	|KW_TEX_FORMAT '(' SYMBOL ',' CONST_STR ')'				    {symbolSetFormat($3, $5);}
	;

simplify:
	KW_SIMPLIFY ':' expression 									{expressionSimplify($3);}
                                                                //|KW_EXPAND ':' expression 									{expressionExpand($3);}
	;

conditionalRule:
    KW_CONDITIONAL rule KW_IF condition                         {ruleAddCondition($2, $4);}
    ;

condition:
    expression                                                  {$$ = $1;}
    ;

rule:
     KW_RULE ':' expSet    										{$$ = createRule($3);}
    |KW_RULE ':' expression OP_TO expression                    {$$ = createWritingRule($3, $5);}
    ;

set:
	'{' expression ':' expression '}'							{$$ = setCreateCondition($2, $4);}
	|expSet														{$$ = setCreateList($1);}
	|set OPERATOR_INLINE set									{$$ = setApplyOperator($2, $1, $3);}
	|KW_NUMSET '(' SYMBOL ')'									{$$ = numsetCreate($3);}
	;

expSet:
    '{' expList '}'												{$$ = $2;}
    |'{' expression '}'											{$$ = expListCreate(1, $2);}
    ;

expList:
    expression ',' expression									{$$ = expListCreate(2, $1, $3);}
    |expList ',' expression										{expListAdd($1, 1, $3); $$ = $1;}
    ;

functionDef:
     SYMBOL '(' expList ')'								{$<op>$ = declareFunction($1, $3);}
	 OP_DEF expression									{setFunctionExpressionOrValue($<op>5, $3, $7);}
    |SYMBOL '(' expression ')'							{$<op>$ = declareFunction($1, expListCreate(1, $3));}
     OP_DEF expression									{setFunctionExpressionOrValue($<op>5, expListCreate(1, $3), $7);}
	;

functionDeclare:
    KW_LET ':' SYMBOL ':' set OP_TO set                                     {$$ = declareFunctionSets($3, $5, $7);}
    |KW_LET ':' SYMBOL ':' set OP_TO set ',' expression OP_TO expression    {$$ = declareFunctionSets($3, $5, $7); setFunctionExpression($$, expListCreate(1, $9), $11);}
    |KW_LET ':' SYMBOL ':' set OP_TO set ',' expList OP_TO expression       {$$ = declareFunctionSets($3, $5, $7); setFunctionExpression($$, $9, $11);}
    ;

plotCmd:
    KW_PLOT SYMBOL ',' expression ',' expression ':' expression     {expressionPlot($2, $4, $6, $8);}
    |KW_PLOT SYMBOL ',' expression ',' expression ':' expList        {expressionPlotList($2, $4, $6, $8);}
    ;

varDef:
	SYMBOL OP_DEF expression
	;

funcHead:

	;

expression:
    SYMBOL														{$$ = createExpressionSymbol($1);}
    |value                                                      {$$ = $1;}
    |'(' expression ')'											{$$ = createExpressionPar($2);}
    |'[' expList ']'                                            {$$ = createVectorExpression($2);}
    |expression OPERATOR_INLINE expression						{$$ = createExpressionInline($2, $1, $3);}
    |OPERATOR_INLINE expression									{$$ = createExpressionInline($1, "", $2);}
    |OPERATOR '(' expList ')'									{$$ = createExpression($1, $3);}
    |OPERATOR '(' expression ')'								{$$ = createExpression($1, expListCreate(1, $3));}
    |SYMBOL '(' expList ')'									    {$$ = createExpressionFunc($1, $3);}
    |SYMBOL '(' expression ')'								    {$$ = createExpressionFunc($1, expListCreate(1, $3));}
    |expression OPERATOR_INLINE set								{$$ = createExpressionInline($2, $1, $3);}
    |set														{$$ = $1;}
    ;

value:
    CONST_INT                                                   {$$ = createExpressionInt($1);}
    |CONST_BOOL                                                 {$$ = createExpressionInt($1);}
    |CONST_FLOAT                                                {$$ = createExpressionDouble($1);}
    ;

%%

void symerror(char * msg) {

    logError(line, msg, symtext);

    //fprintf(stderr, "Error on line %d: '%s' near '%s'\n", line, msg, yytext);
    //exit(1);
}

int symwrap() {
    return 1;
}

void parseFile(FILE * file) {
    //yyin = file;
    //while (1) {
        symparse();
        //printf("Parser finished\n");
    //}
}


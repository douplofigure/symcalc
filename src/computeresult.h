#ifndef COMPUTERESULT_H_INCLUDED
#define COMPUTERESULT_H_INCLUDED

#include "dvector.h"
#include "number.h"

class Expression;

class ComputeResult {

    public:

        ComputeResult(bool b, ...);
        ComputeResult(Number n);
        ComputeResult();
        //ComputeResult(ComputeResult& val);
        virtual ~ComputeResult();

        //ComputeResult operator=(const ComputeResult val);
        ComputeResult operator=(const Number val);
        ComputeResult operator=(const bool val);

        ComputeResult operator+(const Number val);
        ComputeResult operator+(const ComputeResult val);

        ComputeResult operator-(const ComputeResult val);
        ComputeResult operator*(const ComputeResult val);
        ComputeResult operator/(const ComputeResult val);

        operator bool();

        bool operator==(ComputeResult val);
        bool operator<(ComputeResult val);
        bool operator>(ComputeResult val);

        bool operator<=(ComputeResult val);
        bool operator>=(ComputeResult val);

        bool isApprox();
        std::string toTeXFormat();

        std::shared_ptr<Expression> asExpression();


        bool isVector;
        Number n;
        Numeric::dVector<Number> vec;

};

ComputeResult pow(ComputeResult n, ComputeResult p);

inline ComputeResult  operator+(Number n, ComputeResult val) {
    return val + n;
}

std::ostream& operator<<(std::ostream& stream, ComputeResult& val);

#endif // COMPUTERESULT_H_INCLUDED

#include "computeresult.h"

#include "expression.h"
#include "vector.h"

#include <sstream>

ComputeResult::ComputeResult(bool b, ...) {

    va_list vl;
    va_start(vl, b);

    this->isVector = b;
    if (isVector) {

        this->vec = va_arg(vl, Numeric::dVector<Number>);

    } else {
        this->n = va_arg(vl, Number);
    }

}

/*ComputeResult::ComputeResult(ComputeResult& val) {

    this->isVector = val;
    this->value = val.value;

}*/

ComputeResult::ComputeResult(Number n) {
    this->isVector = false;
    this->n = n;
}

ComputeResult::ComputeResult() {
    this->isVector = false;
    this->n = Number(0);
}

ComputeResult::~ComputeResult() {

}

/*ComputeResult::numeric_value_u::numeric_value_u() {
    n = Number();
    vec = Numeric::dVector<Number>();
}*/

/*ComputeResult::numeric_value_u::~numeric_value_u() {
}*/

ComputeResult::operator bool () {
    return !this->isVector && (bool) n;
}

bool ComputeResult::isApprox() {
    if (this->isVector) {
        bool b = false;
        for (int i = 0; i < this->vec.getDim(); ++i) {
            b |= this->vec[i].getType() >= Number::NUM_R;
        }
        return b;
    } else {
        return n.getType() >= Number::NUM_R;
    }
}

std::string ComputeResult::toTeXFormat() {

    if (!isVector) return n.toTeXFormat();
    std::stringstream stream;

    stream << "\\begin{pmatrix}[1.5] ";

    stream << this->vec[0].toTeXFormat();
    for (int i = 1; i < this->vec.getDim(); ++i) {
        stream << " \\\\ " << this->vec[i].toTeXFormat();
    }
    stream << " \\end{pmatrix}";// << std::endl;

    return stream.str();

}

ComputeResult ComputeResult::operator=(const Number val) {
    this->isVector = false;
    this->n = val;
    return *this;
}

ComputeResult ComputeResult::operator=(const bool val) {
    this->isVector = false;
    this->n = Number((unsigned long)(val ? 1 : 0));
    return *this;
}

ComputeResult ComputeResult::operator+(const Number val) {
    if (this->isVector) throw std::runtime_error("Adding number and vector");
    return ComputeResult(false, this->n + val);
}

ComputeResult ComputeResult::operator+(const ComputeResult val) {
    if (this->isVector != val.isVector) throw std::runtime_error("Adding number and vector");
    if (isVector)
        return ComputeResult(isVector, this->vec + val.vec);
    else
        return ComputeResult(isVector, n + val.n);
}

ComputeResult ComputeResult::operator-(const ComputeResult val) {
    if (this->isVector != val.isVector) throw std::runtime_error("Adding number and vector");
    if (isVector)
        return ComputeResult(isVector, this->vec - val.vec);
    else
        return ComputeResult(isVector, n - val.n);
}

ComputeResult ComputeResult::operator*(const ComputeResult val) {
    if (this->isVector != val.isVector) throw std::runtime_error("Adding number and vector");
    //std::cout << "Computing " << this->n << "*" << val.n << std::endl;
    if (isVector)
        return ComputeResult(isVector, this->vec * val.vec);
    else
        return ComputeResult(isVector, n * val.n);
}

ComputeResult ComputeResult::operator/(ComputeResult val) {
    if (this->isVector != val.isVector) throw std::runtime_error("Adding number and vector");
    //std::cout << "Computing " << this->n << "/" << val.n << std::endl;
    if (isVector)
        throw std::runtime_error("Unsupported operation");
        //return ComputeResult(isVector, this->vec / val.vec);
    else
        return ComputeResult(isVector, n / val.n);
}

ComputeResult pow(ComputeResult n, ComputeResult p) {

    if (n.isVector || p.isVector) throw std::runtime_error("Unsupported operation");

    Number res = pow(n.n, p.n);
    ComputeResult cRes;
    cRes = res;
    return cRes;

}

std::shared_ptr<Expression> ComputeResult::asExpression() {

    return std::shared_ptr<Expression>(new Expression(*this));

}

bool ComputeResult::operator==(ComputeResult val) {

    if (this->isVector != val.isVector) return false;
    if (isVector) {
        return vec == val.vec;
    } else {
        return n == val.n;
    }

}

bool ComputeResult::operator<(ComputeResult val) {
    if (isVector) {
        return false;
    } else {
        return n < val.n;
    }
}

bool ComputeResult::operator>(ComputeResult val) {
    if (isVector) {
        return false;
    } else {
        return n > val.n;
    }
}

bool ComputeResult::operator<=(ComputeResult val) {
    if (isVector) {
        return false;
    } else {
        return n <= val.n;
    }
}

bool ComputeResult::operator>=(ComputeResult val) {
    if (isVector) {
        return false;
    } else {
        return n >= val.n;
    }
}

std::ostream& operator<<(std::ostream& stream, ComputeResult& val) {

    if (val.isVector) {
        stream << val.vec;
    } else {
        stream << val.n;
    }

    return stream;
}

#ifndef PLOTTING_H
#define PLOTTING_H

#include <vector>
#include <string>

#include "computeresult.h"
#include "expression.h"
#include "gnuplot_i/gnuplot_i.h"

class Plotter
{
    public:

        static void plotExpression(std::string symbol, ComputeResult xMin, ComputeResult xMax, int lines, Expression * exp);
        static void plotExpressions(std::string symbol, ComputeResult xMin, ComputeResult xMax, int lines, std::vector<Expression *> exp);

        static void cleanup();

    protected:

    private:

        static gnuplot_ctrl * gnuplotCtl;
        static int graphCount;
        static std::vector<std::string> filesToDelete;

};

#endif // PLOTTING_H

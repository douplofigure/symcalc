#include "function.h"

#include <map>
#include <vector>
#include <iostream>

#include "expression.h"
#include "operator.h"
#include "rule.h"
#include "set.h"

std::map<std::string, Function *> Function::functionsByName;

Function::Function(std::string name, Set * domain, Set * images) : Operator(name, domain->getDimension(), 4, 1, false, false) {

    this->name = name;
    this->domain = domain;
    this->images = images;
    functionsByName[name] = this;

}

Function::Function(std::string name, std::vector<std::string> vars) : Function(name, vars, nullptr) {

}

Function::Function(std::string name, std::vector<std::string> vars, bool b) : Function(name, vars, nullptr, b) {

}

Function::Function(std::string name, std::vector<std::string> vars, std::shared_ptr<Expression> exp) : Function(name, vars, exp, true) {

}

Function::Function(std::string name, int varCount, bool addToMap) : Operator(name, varCount, 4, 1, false, false) {

    this->name = name;

    if (addToMap) {
        if (functionsByName.find(name) != functionsByName.end()) {
            throw std::runtime_error(std::string("redifinition of function ").append(name));
        }
        functionsByName[name] = this;
    }

    std::string format = this->getSymbol();
    format.append("(%0");
    for (int i = 1; i < vars.size(); ++i) {
        format.append(", %");
        format.append(std::to_string(i));
    }
    format.append(")");

    this->setTexFormat(format);

    this->domain = new NumSet(Number::NUM_R);
    this->images = new NumSet(Number::NUM_R);

}

Function::Function(std::string name, std::vector<std::string> vars, std::shared_ptr<Expression> exp, bool addToMap) : Operator(name, vars.size(), 4, 1, false, false) {
    this->name = name;
    this->vars = vars;
    this->expression = std::shared_ptr<Expression>(exp);

    if (addToMap) {
        if (functionsByName.find(name) != functionsByName.end()) {
            throw std::runtime_error(std::string("redifinition of function ").append(name));
        }
        functionsByName[name] = this;
    }

    std::string format = this->getSymbol();
    format.append("(%0");
    for (int i = 1; i < vars.size(); ++i) {
        format.append(", %");
        format.append(std::to_string(i));
    }
    format.append(")");

    this->setTexFormat(format);

    this->domain = new NumSet(Number::NUM_R);
    this->images = new NumSet(Number::NUM_R);

    /*if(exp)
        this->setPrecedence(exp->getComplexityScore() + 1);*/

}

Function::~Function() {

    if (domain) delete domain;
    if (images) delete images;

}

void Function::setVars(std::vector<std::string> vars) {
    this->vars = vars;
}

Function * Function::getByName(std::string name) {
    if (functionsByName.find(name) == functionsByName.end())
        return nullptr;
    return functionsByName[name];
}

std::shared_ptr<Expression> Function::getCall() {

    std::vector<Expression *> args(vars.size());
    for (int i = 0; i < vars.size(); ++i) {
        args[i] = new Expression(nullptr, vars[i].c_str());
    }

    return std::shared_ptr<Expression>(new Expression(this, args));

}

std::shared_ptr<Expression> Function::getExpression() {
    if (!expression) return nullptr;
    return expression;
}

void Function::setExpression(std::shared_ptr<Expression> exp) {

    //std::cout << "Setting expression for function " << name << " " << exp << std::endl;

    this->expression = exp;
}

void Function::setExpression(Expression * exp) {

    //std::cout << "Setting expression for function " << name << " " << exp << std::endl;

    this->expression = std::shared_ptr<Expression>(exp);
}

bool Function::hasImplementation() {
    return true;
}

bool Function::checkArgumentCount(int inCount) {
    return inCount == vars.size();
}

std::map<std::string, Expression *> Function::buildCorrespondance(std::vector<Expression *> args) {

    if (args.size() != vars.size())
        throw std::runtime_error(std::string("Wrong argument count for function ").append(this->getSymbol()));

    std::map<std::string, Expression *> corr;
    for (int i = 0; i < vars.size(); ++i) {
        corr[vars[i]] = args[i];
    }

    return corr;

}

int Function::getOperandCount() {
    return vars.size();
}

void Function::addSpecialCase(std::vector<Expression *> args, Expression * val) {

    //std::cout << "Adding special case to " << name << " as " << val << std::endl;

    std::map<std::string, ComputeResult> m;

    std::vector<ComputeResult> data(args.size());
    for (int i = 0; i < args.size(); ++i) {
        data[i] = args[i]->getValue(m);
    }

    this->specialCases.push_back(data);
    this->specialValues.push_back(val->getValue(m));

}

ComputeResult Function::compute(std::vector<std::shared_ptr<Expression>> x, std::map<std::string, ComputeResult>& varValues) {

    //std::cout << "computing: " << this->name << "(" << x[0] << ")" << std::endl;

    if (x.size() != vars.size())
        throw std::runtime_error(std::string("Wrong argument count in function call to ").append(this->getSymbol()));

    std::map<std::string, ComputeResult> varData;
    for (int i = 0; i < vars.size(); ++i) {
        //std::cout << "Setting value of variable " << vars[i] << " to " << x[i]->getValue(varValues).n << std::endl;
        varData[vars[i]] = x[i]->getValue(varValues);
    }

    for (int i = 0; i < specialCases.size(); ++i) {

        bool match = true;
        for (int j = 0; j < specialCases[i].size(); ++j) {
            match &= specialCases[i][j] == varData[vars[j]];
        }

        if (match) return specialValues[i];

    }

    if (!expression->canCompute(varData)) {
        std::cerr << "Unable to compute " << expression << " with variables:" << std::endl;
        for (auto it = varData.begin(); it != varData.end(); ++it)
            std::cerr << it->first << " -> " << it->second << std::endl;
        throw std::runtime_error(std::string("Problem in arguments to function ").append(this->name));
    }

    return expression->getValue(varData);

}

std::shared_ptr<Expression> Function::expand(std::shared_ptr<Expression> exp, std::vector<std::shared_ptr<Expression>> operands) {

    std::vector<Expression *> tmpOps(operands.size());

    for (int i = 0; i < operands.size(); ++i)
        tmpOps[i] = operands[i].get();

    auto m = buildCorrespondance(tmpOps);
    return Expression::buildExpression(this->expression, m);

}

void Function::deleteAll() {
    for (auto it = functionsByName.begin(); it != functionsByName.end(); ++it) {
        delete it->second;
    }
}

bool Function::isFunction(std::shared_ptr<Expression> op) {

    if (op->getOperator()) {

        if (!op->getOperator()->getSymbol().compare(".")) {

            std::vector<std::shared_ptr<Expression>> operands = op->getOperands();

            return isFunction(operands[0]) && isFunction(operands[1]);

        }

        return false;

    }

    return Function::getByName(op->getSymbol()) != nullptr;

}

ComputeResult Function::eval(std::shared_ptr<Expression> f, std::vector<std::shared_ptr<Expression>> args, std::map<std::string, ComputeResult>& varValues) {

    if (!isFunction(f))
        throw std::runtime_error("Cannot evaluate non-function expression");

    if (f->getOperator()){

        std::vector<std::shared_ptr<Expression>> operands = f->getOperands();

        ComputeResult res1 = eval(operands[1], args, varValues);

        std::vector<std::shared_ptr<Expression>> newArgs;

        if (res1.isVector) {

            for (int i = 0; i < res1.vec.getDim(); ++i) {
                newArgs.push_back(res1.vec[i].asExpression());
            }

        } else {

            newArgs.push_back(res1.asExpression());

        }

        return eval(operands[0], newArgs, varValues);

    }

    return getByName(f->getSymbol())->compute(args, varValues);

}

TempFunction::TempFunction(std::string name, std::vector<std::string> vars) : Function(name, vars, false) {

}

std::vector<BuiltinFunction*> BuiltinFunction::builtins;

BuiltinFunction::BuiltinFunction(std::string name, int varCount, std::function<ComputeResult(std::vector<std::shared_ptr<Expression>>, std::map<std::string, ComputeResult>& varValues)> body) : Function(name, varCount, true) {

    this->impl = body;
    this->varCount = varCount;
    builtins.push_back(this);

}

ComputeResult BuiltinFunction::compute(std::vector<std::shared_ptr<Expression>> operands, std::map<std::string, ComputeResult>& varValues) {

    /*if (operands.size() != this->varCount) {
        throw std::runtime_error(std::string("Wrong argument count to builtin function ").append(this->getSymbol()));
    }*/

    return this->impl(operands, varValues);

}

void BuiltinFunction::createBuiltins() {

    BuiltinFunction * isFunction = new BuiltinFunction("IS_FUNCTION", 1, [](std::vector<std::shared_ptr<Expression>> operands, std::map<std::string, ComputeResult>& varValues) -> ComputeResult {

        return (ComputeResult(Number((int)(Function::isFunction(operands[0])))));

    });

    BuiltinFunction * eval = new BuiltinFunction("EVAL", -1, [] (std::vector<std::shared_ptr<Expression>> operands, std::map<std::string, ComputeResult>& varValues) -> ComputeResult {

        if (!Function::isFunction(operands[0]))
            return operands[0]->getValue(varValues);
            //throw std::runtime_error("Cannot evaluate non-function expression!");

        std::vector<std::shared_ptr<Expression>> args(operands.begin()+1, operands.end());

        return Function::eval(operands[0], args, varValues);

    });

}

bool BuiltinFunction::canCompute(std::map<std::string, ComputeResult>& varValues, std::vector<std::shared_ptr<Expression>>& operands) {

    if (!this->hasImplementation()) {
        return false;
    }

    return true;

    /*bool b = true;
    for (int i = 0; i < operands.size(); ++i) {

        if (!operands[i]->getOperator()) {
            b &= (bool) Function::getByName(operands[i]->getSymbol());
            if (b) continue;
        }

        b &= operands[i]->canCompute(this->getVars(varValues, i, operands));
        this->cleanupVars(varValues, i, operands);
    }
    return b;*/

}

bool BuiltinFunction::checkArgumentCount(int inCount) {
    return inCount == this->varCount;
}

int BuiltinFunction::getOperandCount() {
    return varCount;
}

bool BuiltinFunction::hasImplementation() {
    return true;
}

#include "gnuplot_i/gnuplot_i.h"
#include "expression.h"

#include <stdarg.h>
#include <ostream>
#include <sstream>
#include <stack>
#include <iostream>

#include "rule.h"
#include "function.h"
#include "vector.h"

#include "ioctl.h"
#include "set.h"
#include "plotting.h"

extern "C" int line;

Expression::Expression() {
    this->op = nullptr;
    this->symbol = "";
    this->opPrecendence = 0;
}

Expression::Expression(Operator * op, ...) {
    this->op = op;
    va_list vl;
    va_start(vl, op);
    if (!op) {
        this->opPrecendence = 0;
        this->symbol = std::string(va_arg(vl, char *));
    } else {
        this->opPrecendence = op->getPrecedence();
        for (int i = 0; i < op->getOperandCount(); ++i) {
            std::shared_ptr<Expression> tmp = std::shared_ptr<Expression>(va_arg(vl, Expression*));
            this->operands.push_back(tmp);
        }

    }
    va_end(vl);
    this->isValue = false;
}

Expression::Expression(std::string opSym, ...) {
    this->op = Operator::getFromSymbol(opSym);
    va_list vl;
    va_start(vl, opSym);
    if (!op) {
        this->opPrecendence = 0;
        this->symbol = std::string(va_arg(vl, char *));
    } else {
        this->opPrecendence = op->getPrecedence();
        for (int i = 0; i < op->getOperandCount(); ++i)
            this->operands.push_back(std::shared_ptr<Expression>(va_arg(vl, Expression*)));

    }
    va_end(vl);
    this->isValue = false;
}

Expression::Expression(Operator * op, std::vector<std::shared_ptr<Expression>> operands) {
    this->op = op;
    this->operands = operands;

    this->opPrecendence = op->getPrecedence();
    this->isValue = false;

}

Expression::Expression(Operator * op, std::vector<Expression*> operands) {
    this->op = op;
    this->operands = std::vector<std::shared_ptr<Expression>>(operands.size());

    for (int i = 0; i < operands.size(); ++i) {
        this->operands[i] = std::shared_ptr<Expression>(operands[i]);
    }

    this->opPrecendence = op->getPrecedence();
    this->isValue = false;

}

Expression::Expression(ComputeResult value) {

    this->op = nullptr;
    this->opPrecendence = 0;
    this->isValue = true;
    this->value = value;

}

/*Expression::Expression(std::shared_ptr<Expression> exp) {

    throw std::runtime_error("Please use createCopy instead");

    if (!exp) throw std::runtime_error("Can not create expression from nullptr");
    this->op = exp->op;
    this->opPrecendence = exp->getOpPrecedence();
    if (!op) {
        this->symbol = exp->symbol;
    } else {
        for (int i = 0; i < exp->operands.size(); ++i) {
            this->operands.push_back(new Expression(exp->operands[i]));
        }
    }
}*/

Expression::~Expression() {

    //std::cout << "Deleting " << this << std::endl;

}

bool isCharWhitespace(char c) {
    return c == ' ' || c == '\t' || c == '\n' || c == '\r' ;
}

std::shared_ptr<Expression> Expression::fromString(std::string str) {
    char * data = (char *) str.c_str();
    char * symStart = data;
    std::stack<std::shared_ptr<Expression>> symbolStack;

    while(*data) {

        // Find next whitespace
        while (*data && !isCharWhitespace(*data)) data++;
        char old = *data;
        *data = 0;
        std::string symbol(symStart);

        if (old) symStart = ++data;

        Operator * op = Operator::getFromSymbol(symbol);

        if (!op) {
            symbolStack.push(std::shared_ptr<Expression>(new Expression(nullptr, symbol.c_str())));
            continue;
        }

        std::vector<std::shared_ptr<Expression>> opData(op->getOperandCount());
        for (int i = op->getOperandCount()-1; i >= 0; --i) {
            opData[i] = symbolStack.top();
            symbolStack.pop();
        }

        symbolStack.push(std::shared_ptr<Expression>(new Expression(op, opData)));


    }

    return symbolStack.top();

}

int Expression::getComplexityScore() {

    if (!op) {

        if (this->isValue) return 1;
        else if (this->symbol[0] == '#') return 1;

        return 2;

    }

    int s = 0;
    for (int i = 0; i < this->operands.size(); ++i) {
        s += operands[i]->getComplexityScore();
    }

    return (op->getVisualComplexity() + 1) * s;

}

void Expression::writeToStream(std::ostream& stream) {

    if (!op) {
        if (isValue)
            stream << value;
        else
            stream << symbol;
        return;
    }
    //stream << "(";
    this->op->writeToStream(stream, operands);
    //stream << ")";
}

std::shared_ptr<Expression> Expression::replaceChild(std::shared_ptr<Expression> toReplace, std::shared_ptr<Expression> exp) {

    //std::cout << "Replacing " << toReplace << " with " << exp << " in " << this << std::endl;

    if (this == toReplace.get()) return exp;
    if (!this->op) return this->createCopy();

    std::vector<std::shared_ptr<Expression>> nOperands(this->operands.size());
    for (int i = 0; i < operands.size(); ++i)
        nOperands[i] = operands[i]->replaceChild(toReplace, exp);

    return std::shared_ptr<Expression>( new Expression(this->op, nOperands));

}

std::shared_ptr<Expression> Expression::replaceChild(Expression * toReplace, std::shared_ptr<Expression> exp) {

    //std::cout << "Replacing " << toReplace << " with " << exp << " in " << this << std::endl;

    if (this == toReplace) return exp;
    if (!this->op) return this->createCopy();

    std::vector<std::shared_ptr<Expression>> nOperands(this->operands.size());
    for (int i = 0; i < operands.size(); ++i)
        nOperands[i] = operands[i]->replaceChild(toReplace, exp);

    return std::shared_ptr<Expression>( new Expression(this->op, nOperands));

}

std::shared_ptr<Expression> Expression::getBestNeighbor(std::vector<Rule *> rules) {

    /*std::map<std::string, ComputeResult> tmp;
    if (this->canCompute(tmp)) {
        ComputeResult res = this->getValue(tmp);
        if (!res.isApprox()) {
            //std::cout << "Best neighbor for " << this << " is " << res.toTeXFormat() << std::endl;
            return res.asExpression();
        }
    }*/

    std::shared_ptr<Expression> minExp = nullptr;

    std::stack<Expression *> toVisit;
    toVisit.push(this);
    while (!toVisit.empty()) {

        Expression * current = toVisit.top();
        toVisit.pop();
        if (current->getOperator()) {
            std::vector<std::shared_ptr<Expression>> children = current->getOperands();
            for (int i = children.size()-1; i >= 0; --i) {
                toVisit.push(children[i].get());
            }
        }

        //if (current == this) continue;
        std::map<std::string, ComputeResult> vars;
        if (current->isConstant() && current->canCompute(vars)) {
            ComputeResult res = current->getValue(vars);
            if (!res.isApprox()) {
                std::shared_ptr<Expression> ptr = current->getValue(vars).asExpression();
                //std::cout << this << " => " <<  ptr << std::endl;
                minExp = this->replaceChild(current, ptr);
                continue;
            }
        }
        if (!current->applyRules()) continue;

        for (int i = 0; i < rules.size(); ++i) {

            if (rules[i]->matches(current) && current->applyRules()) {

                std::vector<std::shared_ptr<Expression>> res = rules[i]->applyAll(current);
                for (int j = 0; j < res.size(); ++j) {
                    std::shared_ptr<Expression> exp = this->replaceChild(current, res[j]);
                    //std::cout << exp << " : " << exp->getComplexityScore() << std::endl;
                    if (!minExp) {
                        minExp = exp;
                    } else if (exp->getComplexityScore() < minExp->getComplexityScore()) {
                        minExp = exp;
                    } else {
                        //delete exp;
                    }

                }
            }

        }

    }

    return minExp;

}

bool Expression::applyRules() {
    return true;
}

std::vector<std::shared_ptr<Expression>> Expression::getNeighbors(std::vector<Rule *> rules) {

    std::vector<std::shared_ptr<Expression>> nData;

    std::stack<Expression *> toVisit;
    toVisit.push(this);
    while (!toVisit.empty()) {

        Expression * current = toVisit.top();
        toVisit.pop();
        if (current->getOperator()) {
            std::vector<std::shared_ptr<Expression>> children = current->getOperands();
            for (int i = children.size()-1; i >= 0; --i) {
                toVisit.push(children[i].get());
            }
        }

        //if (current == this) continue;

        std::map<std::string, ComputeResult> vars;
        if (current->isConstant() && current->canCompute(vars)) {
            ComputeResult res = current->getValue(vars);
            if (!res.isApprox()) {
                std::shared_ptr<Expression> ptr = current->getValue(vars).asExpression();
                //std::cout << this << " => " <<  ptr << std::endl;
                nData.push_back(this->replaceChild(current, ptr));
                continue;
            }
        }

        if (!current->applyRules()) continue;

        for (int i = 0; i < rules.size(); ++i) {

            if (rules[i]->matches(current) && current->applyRules()) {

                std::vector<std::shared_ptr<Expression>> res = rules[i]->applyAll(current);
                for (int j = 0; j < res.size(); ++j) {
                    //std::cout << "replacing " << current->format() << " with " << res[j]->format() << std::endl;
                    std::shared_ptr<Expression> exp = this->replaceChild(current, res[j]);
                    nData.push_back(exp);

                }

            }

        }

    }

    return nData;

}

std::shared_ptr<Expression> Expression::simplfy(std::vector<Rule *> rules) {

    //This is a single symbol, cannot be simplified
    if (!op) return this->createCopy();

    std::shared_ptr<Expression> best = this->createCopy();
    std::shared_ptr<Expression> nBest = nullptr;
    std::vector<std::shared_ptr<Expression>> exps = getNeighbors(rules);
    for (int i = 0; i < exps.size(); ++i) {

        std::shared_ptr<Expression> tmpN = exps[i]->getBestNeighbor(rules);

        if (!nBest) {
            best = exps[i];
            nBest = best->getBestNeighbor(rules);
        } else if (tmpN->getComplexityScore() + exps[i]->getComplexityScore() < best->getComplexityScore() + nBest->getComplexityScore()) {

            best = exps[i];
            nBest = tmpN;

        } else {
        }

    }

    return best;

}

Operator * Expression::getOperator() {
    return op;
}

bool Expression::operator==(Expression e) {

    if (!op) {
        if (e.op)
            return false;
        else if (e.isValue && isValue) {
            return value == e.value;
        } else if (!e.isValue && !isValue) {
            return !this->symbol.compare(e.symbol);
        }
        return false;
    }

    if (!(op == e.op)) return false;

    if (operands.size() != e.operands.size()) return false;

    bool b = true;
    for (int i = 0; i < operands.size(); ++i) {
        b &= (operands[i] == e.operands[i]);
    }

    return b;

}

bool Expression::isSame(std::shared_ptr<Expression> e) {

    if (!op) {
        if (e->op)
            return false;
        else if (e->isValue && isValue) {
            return value == e->value;
        } else if (!e->isValue && !isValue) {
            return !this->symbol.compare(e->symbol);
        }
        return false;
    }

    if (!(op == e->op)) return false;

    if (operands.size() != e->operands.size()) return false;

    bool b = true;
    for (int i = 0; i < operands.size(); ++i) {
        b &= (operands[i] == e->operands[i]);
    }

    return b;

}

bool Expression::isSame(Expression * e) {

    if (!op) {
        if (e->op)
            return false;
        return !this->symbol.compare(e->symbol);
    }

    if (!(op == e->op)) return false;

    if (operands.size() != e->operands.size()) return false;

    bool b = true;
    for (int i = 0; i < operands.size(); ++i) {
        b &= (operands[i] == e->operands[i]);
    }

    return b;

}

bool Expression::functionOf(std::string symbol) {

    if(isValue) return false;

    if (!op) {
        return !this->symbol.compare(symbol);
    }

    bool b = false;
    for (int i = 0; i < operands.size(); ++i) {
        b |= operands[i]->functionOf(symbol);
    }

    return b;

}

bool Expression::functionOf(std::shared_ptr<Expression> exp) {

    if(isValue) return false;

    if (!exp) return false;
    //std::cout << "Testing " << this << " and " << exp << std::endl;
    if (!exp->op) return this->functionOf(exp->symbol);

    if (this->isSame(exp)) return true;

    bool b = false;
    for (int i = 0; i < operands.size(); ++i) {
        b |= operands[i]->functionOf(exp);
    }
    //std::cout << b << std::endl;
    return b;

}

bool Expression::functionOf(Expression * exp) {

    if(isValue) return false;

    if (!exp) return false;
    //std::cout << "Testing " << this << " and " << exp << std::endl;
    if (!exp->op) return this->functionOf(exp->symbol);

    if (this->isSame(exp)) return true;

    bool b = false;
    for (int i = 0; i < operands.size(); ++i) {
        b |= operands[i]->functionOf(exp);
    }
    //std::cout << b << std::endl;
    return b;

}

bool Expression::isConstant() {

    if(isValue) return true;

    if (!op) {
        if (symbol[0] == '#')
            return true;
        return false;
    }

    bool b = true;
    for (int i = 0; i < operands.size(); ++i) {
        b &= operands[i]->isConstant();
    }
    return b;

}

ComputeResult Expression::getValue(std::map<std::string, ComputeResult>& varValues) {


    if (!op) {

        if (isValue)
            return value;

        if (symbol[0] == '#') {
            Number n = Number::fromString(symbol.substr(1));

            //std::cout << "Number " << n << std::endl;
            ComputeResult res(n);

            return res;

        }

        if (varValues.find(symbol) != varValues.end()) {
            return varValues[symbol];
        }

        std::string msg = "No such variable '";
        msg.append(symbol).append("'");

        //std::cout << this->operands[0] << std::endl;

        throw std::runtime_error(msg);

    }

    //std::cout << "op: " << op->getSymbol() << std::endl;// " " << operands[0] << std::endl; // << " " << (op->getOperandCount() >= 2 ? operands[1] : operands[0]) << std::endl;

    return op->compute(operands, varValues);

}

bool Expression::canCompute(std::map<std::string, ComputeResult>& varValues) {

    if (!op) {

        if (isValue) return true;

        if (symbol[0] == '#') {
            return true;
        }
        bool b = varValues.find(symbol) != varValues.end();
        return b;
    }

    return op->canCompute(varValues, operands);

}

std::string Expression::getSymbol() {
    return symbol;
}

std::vector<std::shared_ptr<Expression>> Expression::getOperands() {
    return operands;
}

int Expression::getOpPrecedence() {
    return opPrecendence;
}

std::string Expression::format() {

    if (!op) {

        if (isValue) return value.toTeXFormat();

        if (symbol[0] == '#') {

            if ((symbol[1] >= '0' && symbol[1] <= '9') || symbol[1] == '-')
                return symbol.substr(1);

            if (symbol.length() >= 3)
                return std::string("\\").append(symbol.substr(1));

            return symbol.substr(1);

        }

        return symbol;
    }

    return op->formatTeX(operands);

}

void Expression::setOpPrecedence(int val) {
    this->opPrecendence = val;
}

std::ostream& operator<< (std::ostream& stream, Expression * exp) {

    exp->writeToStream(stream);
    return stream;

}

void * createExpressionInline(void * tmpOp, ...) {

    //std::cout << "createExpressionInline" << std::endl;

    Operator * op = (Operator *) tmpOp;
    std::vector<std::shared_ptr<Expression>> args(op->getOperandCount());
    va_list vl;
    va_start(vl, tmpOp);
    for (int i = 0; i < op->getOperandCount(); ++i) {
        args[i] = std::shared_ptr<Expression>(va_arg(vl, Expression *));
    }

    if (args.size() != 2) {
        //std::cerr << "line = " << line << std::endl;
        throw std::runtime_error("Wrong argument count for inline operator");
    }

    if (args[0]->getOperator() && args[0]->getOperator()->getInline()) {

        if (op->getPrecedence() > args[0]->getOpPrecedence()) {

            std::vector<std::shared_ptr<Expression>> oldOps = args[0]->getOperands();
            std::shared_ptr<Expression> a1 = args[1];
            std::vector<std::shared_ptr<Expression>> tmpOps(2);
            tmpOps[0] = oldOps[1];
            tmpOps[1] = args[1];
            args[1] = std::shared_ptr<Expression>(new Expression(op, tmpOps));
            op = args[0]->getOperator();
            args[0] = oldOps[0];

            //delete a1;


        }

    }

    //Expression * exp = new Expression(op, args);
    //std::cout << exp << std::endl;
    return new Expression(op, args);

}

void * createExpressionSymbol(char * symbol) {

    Expression * exp = new Expression(nullptr, symbol);

    free(symbol);

    return exp;
}

void * createExpressionInt(long int val) {
    return new Expression(ComputeResult(Number(val)));
}

void * createExpressionDouble(double val) {
    return new Expression(ComputeResult(Number(val)));
}

void * createExpression(void * tmpOp, void * expList) {

    //std::cout << "createExpression" << std::endl;

    Operator * op = (Operator *) tmpOp;
    EXP_LIST * ls = (EXP_LIST *) expList;

    Expression * exp = new Expression(op, ls->expressions);
    delete ls;

    //std::cout << exp << std::endl;

    return exp;

}

bool Expression::isValueExp() {
    return isValue;
}

ComputeResult Expression::getConstValue() {
    return value;
}

void * createExpressionFunc(char * tmpFn, void * expList) {

    //std::cout << "createExpressionFunc " << expList << std::endl;

    Function * op = Function::getByName(std::string(tmpFn));
    EXP_LIST * ls = (EXP_LIST *) expList;
    if (!op) {

        ///Testing for temporary function (Used to declare rules)
        if (tmpFn[0] == '_') {

            std::vector<std::string> vars(ls->expressions.size());
            for (int i = 0; i < ls->expressions.size(); ++i)
                vars[i] = ls->expressions[i]->getSymbol();

            op = new TempFunction(std::string(tmpFn), vars);

        } else {

            throw std::runtime_error(std::string("No such function ").append(std::string(tmpFn)));

        }
    }

    //std::cout << ls->expressions[0] << std::endl;

    //auto m = op->buildCorrespondance(ls->expressions);
    op->checkArgumentCount(ls->expressions.size());

    Expression * res;
    //if (!op->getExpression())

    res = new Expression(op, ls->expressions);
    /*else
        res = Expression::buildExpression(op->getExpression(), m);*/

    delete ls;

    //std::cout << "Result" << res << std::endl;

    return res;

}

std::shared_ptr<Expression> Expression::buildExpression(std::shared_ptr<Expression> exp, std::map<std::string, std::shared_ptr<Expression>>& relation) {

    if (!exp) throw std::runtime_error("Null-Expression in function def");

    //Expression is terminal => return equivalent construct via copy ctor
    if (!exp->getOperator()) {

        if (exp->isValue) {
            return std::shared_ptr<Expression>(new Expression(exp->value));
        }

        if (exp->getSymbol()[0] == '#') {

            //std::cout << "Creating new expression from " << exp->getSymbol() << std::endl;
            return std::shared_ptr<Expression>(new Expression(nullptr, exp->getSymbol().c_str()));

        }

        std::shared_ptr<Expression> tmp = relation[exp->getSymbol()];
        if (!tmp) {

            std::string nSymbol = exp->getSymbol();
            nSymbol.append("_temp");
            return std::shared_ptr<Expression>(new Expression(nullptr, nSymbol));

        }

        return tmp->createCopy();

    }

    std::vector<std::shared_ptr<Expression>> operands(exp->getOperands().size());
    for (int i = 0; i < exp->getOperands().size(); ++i) {
        operands[i] = buildExpression(exp->getOperands()[i], relation);
    }

    Operator * op = exp->getOperator();
    if (op->getSymbol()[0] == '_') {

        std::string symbol = relation[op->getSymbol()]->getSymbol();

        if (op = Operator::getFromSymbol(symbol));
        else (op = Function::getByName(symbol));

        //op = relation[op->getSymbol()];
        //std::cout << "Swapped operator for " << op->getSymbol() << std::endl;

    }

    return std::shared_ptr<Expression>(new Expression(op, operands));

}

std::shared_ptr<Expression> Expression::buildExpression(std::shared_ptr<Expression> exp, std::map<std::string, Expression *>& relation) {

    if (!exp) throw std::runtime_error("Null-Expression in function def");

    //Expression is terminal => return equivalent construct via copy ctor
    if (!exp->getOperator()) {

        if (exp->isValue) {
            return std::shared_ptr<Expression>(new Expression(exp->value));
        }

        if (exp->getSymbol()[0] == '#') {

            //std::cout << "Creating new expression from " << exp->getSymbol() << std::endl;
            return std::shared_ptr<Expression>(new Expression(nullptr, exp->getSymbol().c_str()));

        }

        Expression * tmp = relation[exp->getSymbol()];
        if (!tmp) {

            std::string nSymbol = exp->getSymbol();
            nSymbol.append("_temp");
            return std::shared_ptr<Expression>(new Expression(nullptr, nSymbol));

        }

        return tmp->createCopy();

    }

    std::vector<std::shared_ptr<Expression>> operands(exp->getOperands().size());
    for (int i = 0; i < exp->getOperands().size(); ++i) {
        operands[i] = buildExpression(exp->getOperands()[i], relation);
    }

    Operator * op = exp->getOperator();
    if (op->getSymbol()[0] == '_') {

        std::string symbol = relation[op->getSymbol()]->getSymbol();

        if (op = Operator::getFromSymbol(symbol));
        else (op = Function::getByName(symbol));

        //op = relation[op->getSymbol()];
        //std::cout << "Swapped operator for " << op->getSymbol() << std::endl;

    }

    return std::shared_ptr<Expression>(new Expression(op, operands));

}

std::shared_ptr<Expression> Expression::expand() {
    if (!op) {
        return std::shared_ptr<Expression>(this);
    }

    for (int i = 0; i < operands.size(); ++i) {
        operands[i] = operands[i]->expand();
    }

    return op->expand(std::shared_ptr<Expression>(this), operands);
}

std::shared_ptr<Expression> Expression::createCopy() {

    std::shared_ptr<Expression> exp;
    if (!op) {
        if (isValue) {
            exp = std::shared_ptr<Expression>(new Expression(value));
        } else {
            exp = std::shared_ptr<Expression>(new Expression(nullptr, symbol.c_str()));
        }
    } else {
        std::vector<std::shared_ptr<Expression>> ops(operands.size());
        for (int i = 0; i < operands.size(); ++i) {
            ops[i] = operands[i]->createCopy();
        }
        exp = std::shared_ptr<Expression>(new Expression(this->op, ops));
    }

    ///Can be changed by parenthesis
    exp->opPrecendence = this->opPrecendence;


    return exp;

}

void * expListCreate(int c, ...) {

    EXP_LIST * ls = new EXP_LIST;
    ls->expressions = std::vector<Expression *>(c);

    va_list vl;
    va_start(vl, c);
    for (int i = 0; i < c; ++i) {
        ls->expressions[i] =va_arg(vl, Expression *);
        //std::cout << ls->expressions[i] << std::endl;
    }

    return (void *) ls;
}

void expListAdd(void * expList, int c, ...) {

    EXP_LIST * ls = (EXP_LIST *) expList;

    va_list vl;
    va_start(vl, c);
    for (int i = 0; i < c; ++i) {
        ls->expressions.push_back(va_arg(vl, Expression *));
    }

}

void * createRule(void * tmp) {

    EXP_LIST * ls = (EXP_LIST *) tmp;
    Rule * r = new Rule(ls->expressions);
    delete ls;
    return (void *) r;

}

void * createExpressionPar(void * exp) {

    Expression * tmp = (Expression *) exp;

    tmp->setOpPrecedence(0x7fffffff);

    //std::cout << tmp << std::endl;

    return tmp;

}

void expPrint(void * tmp) {

    //std::shared_ptr<Expression> exp = std::shared_ptr<Expression>((Expression *) tmp);
    std::cout << ((Expression *) tmp) << std::endl;

}

namespace symcalc {
    void onPrint();
}

void expressionSimplify(void * tmp) {

    std::shared_ptr<Expression> exp = std::shared_ptr<Expression>((Expression *) tmp);

    std::stringstream sstream;

    int c = 0;
    int lastComplex = 0;
    int newComplex = exp->getComplexityScore();
    //ioctl::out() << "\\begin{lstlisting}[breaklines]" << std::endl << "simplify : " << exp << std::endl << "\\end{lstlisting}" << std::endl;
    if (ioctl::cli()) {
        ioctl::out() << "\\begin{align*}" << std::endl;
        ioctl::out() << exp->format();
        symcalc::onPrint();
    }

    ///Simplifying the expression
    std::shared_ptr<Expression> lastExp = std::shared_ptr<Expression>(exp);
    do {
        if (ioctl::cli())
            ioctl::out() << (!c ? "" : "\\\\ ") << std::endl << " &= " << lastExp->format();// << " \\quad & " << lastExp->getComplexityScore();
        else
            ioctl::out() << lastExp << std::endl;
        symcalc::onPrint();
        lastComplex = newComplex;
        std::shared_ptr<Expression> res = lastExp->simplfy(Rule::rules);
        newComplex = res->getComplexityScore();
        //delete lastExp;
        lastExp = res;
    } while (c++ < 1000 && newComplex < lastComplex);

    ///Computing final result if possible or non constant
    std::map<std::string, ComputeResult> vars;
    if (lastExp->canCompute(vars) && lastExp->getComplexityScore() > 1) {
        ComputeResult val = lastExp->getValue(vars);
        if (ioctl::cli())
            ioctl::out() << "\\\\" << std::endl << " &" << (val.isApprox() ? "\\approx " : "= ") << val.toTeXFormat();// << std::endl;
        else
            ioctl::out() << val << std::endl;
    }
    else if (ioctl::cli())
        ioctl::out() << std::endl;
    symcalc::onPrint();

    if (ioctl::cli()) {
        ioctl::out() << "\\end{align*}" << std::endl;
        symcalc::onPrint();
    }

}

void expressionExpand(void * tmp) {

    std::shared_ptr<Expression> exp = std::shared_ptr<Expression>((Expression *)tmp);

    std::stringstream sstream;

    int c = 0;
    int lastComplex = 0;
    int newComplex = exp->getComplexityScore();
    //ioctl::out() << "\\begin{lstlisting}[breaklines]" << std::endl << "expand : " << exp << std::endl << "\\end{lstlisting}" << std::endl;
    ioctl::out() << "\\begin{align*}" << std::endl;
    ioctl::out() << exp->format();
    std::shared_ptr<Expression> lastExp = exp->expand();
    do {
        //ioctl::out() << "Step: " << lastExp << " " << newComplex << std::endl;
        ioctl::out() << (!c ? "" : "\\\\ ") << std::endl << " &= " << lastExp->format();// << " \\quad & " << lastExp->getComplexityScore();
        sstream << lastExp << std::endl;
        symcalc::onPrint();
        //sstream << (!c ? "" : "\\\\ ") << std::endl << " &= " << lastExp->format();
        lastComplex = newComplex;
        //ioctl::out() << "simplifying" << std::endl;
        std::shared_ptr<Expression> res = lastExp->simplfy(Rule::rules);
        //ioctl::out() << "getting complexity" << std::endl;
        newComplex = res->getComplexityScore();
        //delete lastExp;
        lastExp = res;
    } while (c++ < 1000 && newComplex < lastComplex);

    std::map<std::string, Number> vars;
    /*if (lastExp->canCompute(vars)) {
        Number val = lastExp->getValue(vars);
        ioctl::out() << "\\\\" << std::endl << " &" << (val.getType() >= Number::NUM_R ? "\\approx " : "= ") << val << std::endl;
    }
    else*/
    ioctl::out() << std::endl;
    //ioctl::out() << lastExp->format() << std::endl;

    symcalc::onPrint();

    ioctl::out() << "\\end{align*}" << std::endl;

}

/*void declareFunction(char * name, void * varTmp, void * tmp) {

    std::cout << "Declaring " << std::string(name) << std::endl;

    std::shared_ptr<Expression> exp = (std::shared_ptr<Expression>) tmp;
    std::shared_ptr<Expression> varExp = (std::shared_ptr<Expression>) varTmp;

    std::vector<std::string> vars(1);
    vars[0] = varExp->getSymbol();

    std::cout << vars[0] << std::endl;

    Function * f = new Function(std::string(name), vars, exp);
    std::cout << "Function ok" << std::endl;

}*/

void * declareFunction(char * name, void * expList) {

    Function * f = Function::getByName(std::string(name));
    if (f) {
        free(name);
        return f;
    }

    EXP_LIST * ls = (EXP_LIST *) expList;

    std::vector<std::string> vars(ls->expressions.size());
    for (int i = 0; i < ls->expressions.size(); ++i)
        vars[i] = ls->expressions[i]->getSymbol();

    f = new Function(std::string(name), vars);
    delete ls;
    free(name);
    return f;

}

void setFunctionExpressionOrValue(void * tmpF, void * expLs, void * exp) {

    Function * fn = (Function *) tmpF;

    if (fn->getExpression()) {
        functionSetValue(tmpF, expLs, exp);
    } else {
        functionSetExpression(tmpF, exp);
    }

}

void functionSetExpression(void * tmpFn, void * tmp) {

    Function * fn = (Function *) tmpFn;
    Expression * exp = (Expression *) tmp;

    //std::shared_ptr<Expression> ptr = std::shared_ptr<Expression>(exp);

    //std::cout << exp << std::endl;

    fn->setExpression(exp);
    std::map<std::string, Expression *> m;// = op->buildCorrespondance(ls->expressions);
    if (fn->getExpression()) {
        std::vector<std::shared_ptr<Expression>> rData(2);
        rData[0] = fn->getCall();
        rData[1] = fn->getExpression();

        Rule * nRule = new Rule(rData);
        //std::cout << "RULE: " << /*rData[0] <<*/ " <=> " << rData[1] << std::endl;
    }

}

void functionSetValue(void * tmpFunc, void * expList, void * tmpExp) {

    Function * fn = (Function *) tmpFunc;
    EXP_LIST * ls = (EXP_LIST *) expList;
    Expression * exp = (Expression *) tmpExp;

    fn->addSpecialCase(ls->expressions, exp);

    delete ls;

}

void * createVectorExpression(void * expList) {

    EXP_LIST * ls = (EXP_LIST *) expList;

    Vector * vec = new Vector(ls->expressions);
    delete ls;
    return vec;

}

void * declareFunctionSets(char * name, void * s1, void * s2) {

    Set * set1 = (Set *) s1;
    Set * set2 = (Set *) s2;

    Function * f = new Function(std::string(name), set1, set2);

    free(name);

    return f;

}

void setFunctionExpression(void * func, void * expList, void * exp) {

    Function * f = (Function *) func;
    EXP_LIST * ls = (EXP_LIST *) expList;

    std::vector<std::string> vars(ls->expressions.size());
    for (int i = 0; i < ls->expressions.size(); ++i)
        vars[i] = ls->expressions[i]->getSymbol();

    f->setVars(vars);

    functionSetExpression(func, exp);

    delete ls;

}

Expression::Type Expression::getType() {
    return NONE;
}

void * setApplyOperator(void * o, void * l, void * r) {

    Operator * op = (Operator *) o;
    std::shared_ptr<Set> left = std::shared_ptr<Set>((Set*) l);
    std::shared_ptr<Set> right = std::shared_ptr<Set>((Set*) r);

    if (op == Operator::getFromSymbol("+")) {
        return Set::unite(left, right);
    } else if (op == Operator::getFromSymbol("-")) {
        return Set::diff(left, right);
    } else if (op == Operator::getFromSymbol("*")) {
        return Set::cartesian(left, right);
    }

}

void * createWritingRule(void * e1, void * e2) {

    return new WritingRule((Expression *) e1, (Expression *) e2);

}

#define PLOT_POINTS 150

#include <locale.h>

void expressionPlot(char * s, void * l, void * h, void * e) {

    std::string symbol(s);
    Expression * lexp = (Expression *) l;
    Expression * hexp = (Expression *) h;
    Expression * exp = (Expression *) e;

    std::map<std::string, ComputeResult> varValues;

    if (!lexp->canCompute(varValues) || ! hexp->canCompute(varValues)) {
        free(s);
        delete lexp;
        delete hexp;
        delete exp;
        return;
    }

    ComputeResult xMin = lexp->getValue(varValues);
    ComputeResult xMax = hexp->getValue(varValues);

    Plotter::plotExpression(symbol, xMin, xMax, PLOT_POINTS, exp);

    free(s);
    delete lexp;
    delete hexp;
    delete exp;

}

void expressionPlotList(char * s, void * l, void * h, void * e) {

    std::string symbol(s);
    Expression * lexp = (Expression *) l;
    Expression * hexp = (Expression *) h;
    EXP_LIST * exp = (EXP_LIST *) e;

    std::map<std::string, ComputeResult> varValues;

    if (!lexp->canCompute(varValues) || ! hexp->canCompute(varValues)) {
        free(s);
        delete lexp;
        delete hexp;
        delete exp;
        return;
    }

    ComputeResult xMin = lexp->getValue(varValues);
    ComputeResult xMax = hexp->getValue(varValues);

    Plotter::plotExpressions(symbol, xMin, xMax, PLOT_POINTS, exp->expressions);

    free(s);
    delete lexp;
    delete hexp;
    delete exp;

}

#ifndef DVECTOR_H
#define DVECTOR_H

#include <iostream>

#include <cstdarg>
#include <cmath>

namespace Numeric {

template <typename T = double> class dVector {

    public:
        /**This will copy data, you can free it after the call*/
        dVector(unsigned int dim, T * data){
            this->dim = dim;
            this->data = new T[dim];
            for (int i = 0; i < dim; ++i) {
                this->data[i] = data ? data[i] : (T) 0;
            }
        }

        dVector(unsigned int dim, ...) {
            this->dim = dim;
            this->data = new T[dim];
            va_list vl;
            va_start(vl, NULL);
            for (int i = 0; i < dim; ++i)
                this->data[i] = va_arg(vl, double);
            va_end(vl);

        }

        dVector(){
            this->dim = 0;
            this->data = nullptr;
        }

        dVector(const dVector& vec) {
            this->dim = vec.dim;
            //std::cout << "Creating new Array of size " << dim << std::endl;
            if (dim != -1) {
                this->data = new T[dim];
                for (int i = 0; i < dim; ++i) {
                    data[i] = vec.data[i];
                }
            } else {
                this->data = nullptr;
            }
        }

        virtual ~dVector() {
            if (data)
                delete[] data;

        }

        T& operator[](int i) {
            if (i < 0 || i >= dim) {
                std::cerr << i << std::endl;
                throw std::runtime_error("No such coordinate");
            }
            return data[i];
        }

        T operator*(dVector<T> vec) {
            T val = (T) 0;
            for (int i = 0; i < dim; ++i) {
                val = val + this->data[i] * vec.data[i];
            }
            return val;
        }

        dVector<T> operator+(dVector<T> vec) {

            T tmp[dim];
            for (int i = 0; i < dim; ++i) {
                tmp[i] = data[i] + vec.data[i];
            }
            return dVector<T>(dim, tmp);

        }

        dVector<T> operator*(T v) {

            T tmp[dim];
            for (int i = 0; i < dim; ++i) {
                tmp[i] = data[i] *v;
            }
            return dVector<T>(dim, tmp);

        }

        dVector<T> operator-(dVector<T> vec) {

            T tmp[dim];
            for (int i = 0; i < dim; ++i) {
                tmp[i] = data[i] - vec.data[i];
            }
            return dVector<T>(dim, tmp);

        }

        dVector<T> operator/(T v) {

            T tmp[dim];
            for (int i = 0; i < dim; ++i) {
                tmp[i] = data[i] / v;
            }
            return dVector<T>(tmp, dim);

        }

        dVector<T>& operator=(dVector<T> vec) {
            this->dim = vec.dim;
            this->data = new T[dim];
            for (int i = 0; i < dim; ++i) {
                data[i] = vec.data[i];
            }
            return *this;
        }

        dVector<T> normalize() {
            T val = this->length();
            if (val == 0.0)
                return *this;
            for (int i = 0; i < dim; ++i) {
                this->data[i] /= val;
            }
            return *this;
        }

        T length() {
            T sum = (T) 0;
            for (int i = 0; i < dim; ++i) {
                sum += this->data[i] * this->data[i];
            }
            return (T) sqrt(sum);
        }

        unsigned int getDim() {
            return dim;
        }

        bool operator==(dVector<T> vec) {
            bool b = true;
            for (int i = 0; i < dim; ++i) {
                b &= data[i] == vec.data[i];
            }
            return b;
        }

    protected:

    private:
        unsigned int dim;
        T* data;

};

template <typename T> inline dVector<T> operator*(T a, dVector<T> vec) {
    unsigned int dim = vec.getDim();
    T tmp[dim];
    for (int i = 0; i < dim; ++i) {
        tmp[i] = vec[i] * a;
    }
    return dVector<T>(dim, tmp);
}

template <typename T> inline std::ostream& operator<<(std::ostream& stream, dVector<T> vec) {
    unsigned int dim = vec.getDim();
    stream << "(";
    stream << vec[0];
    for (int i = 1; i < dim; ++i) {
        stream << ", " << vec[i];
    }
    stream << ")";
    return stream;
}

}

#endif // DVECTOR_H

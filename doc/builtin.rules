operator(+, 2, 1, 1, true, "add")
operator(-, 2, 1, 1, true, "substract")
operator(*, 2, 2, 1, true, "multiply")
operator(/, 2, 2, 2, true, "divide")
operator(^, 2, 3, 1, true, "pow")
operator(mod, 2, 3, 3, true, "modulo")
operator(sum, 4, 3, 2)
operator(abs, 1, 3, 1)
operator(int, 4, 3, 4)
operator(grad, 1, 3, 10)
operator(lim, 3, 3, 2)
operator(&, 2, 1, 1, true)
operator(|, 2, 1, 1, true)
operator(not, 1, 2, 1)
operator(in, 2, 7, 1, true)
operator(==, 2, 7, 1, true, "isEq")
operator(=, 2, 7, 1, true)
operator(<=, 2, 7, 1, true, "isLeq")
operator(>=, 2, 7, 1, true, "isGeq")
operator(<, 2, 7, 1, true, "isLess")
operator(>, 2, 7, 1, true, "isGreater")

operator(diff, 2, 3, 50, false)

fac(n) := n * fac(n - 1)

fac(0) := 1

binom(n, k) := fac(n) / (fac(n-k) * fac(k))
exp(x) := #e ^ x
sqrt(x) := x ^ (1/2)
sqrt(4) := 2
cos(x) := (#e^(#i * x) + #e^(-1 * #i * x)) / 2
sin(x) := (#e^(#i * x) - #e^(-1 * #i * x)) / (2 * #i)
tan(x) := sin(x) / cos(x)

TeX_format(+, "%0 + %1")
TeX_format(*, "%0 \cdot %1")
TeX_format(-, "%0 - %1")
TeX_format(/, "\frac{%0}{%1}")
TeX_format(sum, "\sum_{%0 = %1}^{%2} %3")
TeX_format(abs, "\left | %0 \right|")
TeX_format(sqrt, "\sqrt{%0}\,")
TeX_format(^, "{%0}^{%1}")
TeX_format(exp, "e^{%0}")
TeX_format(int, "\int_{%1}^{%2} %3 \,d%0")
TeX_format(binom, "\binom{%0}{%1}")
TeX_format(fac, "%0!")
TeX_format(cos, "\cos\left(%0\right)")
TeX_format(sin, "\sin\left(%0\right)")
TeX_format(tan, "\tan\left(%0\right)")
TeX_format(grad, "\overrightarrow{\nabla} %0")
TeX_format(lim, "\lim_{%0 \to %1} %2")
TeX_format(&, "%0 \land %1")
TeX_format(|, "%0 \lor %1")
TeX_format(not, "\lnot (%0)")
TeX_format(in, "%0 \in %1")
TeX_format(==, "%0 = %1")
TeX_format(=, "%0 = %1")
TeX_format(<=, "%0 \leq %1")
TeX_format(>=, "%0 \geq %1")
TeX_format(<, "%0 < %1")
TeX_format(>, "%0 > %1")
TeX_format(diff, "\left ( %0 \right )'")

rule : {a * (b + c),
		a * b + a * c,
		(b + c) * a}

rule : {a+b, b+a}

rule : {a * (b - c),
		a * b - a * c,
		(b - c) * a}

rule : {(a / b) + (c / b),
		(a + c) / b}

rule : {(a / b) - (c / b),
		(a - c) / b}

rule : {(a * b) / c,
		a / c * b}

rule : {a / a,
		1}

rule : {1 * a,
		a,
		a * 1}

rule : {2 * a,
		a + a,
		a * 2}

rule : {
    (a-b)*(a+b),
    a^2 - b^2
}

rule : {
    (a + b)^2,
    a^2 + 2 * a * b + b^2
}

rule : {
    (a - b)^2,
    a^2 - 2 * a * b + b^2
}

rule : {sum(i, A, B, x[!i] * y),
		sum(i, A, B, y * x[!i]),
		x[!i] * sum(i, A, B, y)}

rule : {a - b,
		0 - b + a}

rule : {(a - b) * c,
		c * a - c * b,
		c * a - b * c,
		a * c - c * b,
		a * c - b * c,
		c * (a - b),
		c * (0 -b + a),
		(0 -b + a) * c}

rule : { A - (a + b),
		A - a - b}

rule : {a * a,
		a ^ 2}

rule : {a ^ b,
		a ^ (b - 1) * a}

rule : {a ^ b * a,
		a ^(b + 1)}

rule : {1 / b * a,
		a / b,
		a * (1 / b)}

rule : {c / b * a,
		(a*c) / b,
		a * (c / b)}

rule : {sqrt(a) / a,
        1 / sqrt(a)}
rule : {0 - b,
        -1 * b}
rule : {a ^ (b * c),
        (a^b)^c}

rule : {sqrt(x)^2, x}

rule : #i^2 -> -1
rule : x * x -> x^2
rule : x^n * x -> x ^ (n+1)

rule : a^b / c^b -> (a/c)^b
rule : a^1 -> a

rule : {int(x, A, B, a * x ^ b), a * int(x, A, B, x^b), a * ((1/(b+1)) * B^(b+1) - (1/(b+1)) * A^(b+1))}

rule : {lim(x, A, y[!x] * z), y[!x] * lim(x, A, z), lim(x, A, z * y[!x])}
rule : {lim(n, #infty, sum(i, A, n, b)), sum(i, A, #infty, b)}

rule : {sum(k, 0, #infty, x ^ k / fac(k)), exp(x)}
rule : {binom (n, k), (n / k) * binom(n - 1, k - 1)}

rule : sin(x)^2 + cos(x)^2 -> 1
rule : cos(x)^2 + sin(x)^2 -> 1
rule : {sum(i, A, B, x / y[!i]), 1 / y[!i] * sum(i, A, B, x)}
rule : {a * (b/c), b * (a/c)}
rule : {(a/b) / (c /d), (a * d) / (b * c)}
rule : cos(#pi / 4) -> sqrt(2) / 2
rule : cos(#pi / 3) -> 1 / 2
rule : cos(#pi / 6) -> sqrt(3) / 2
rule : cos(#pi / 2)-> 0

rule : sin(#pi / 6) -> 1 / 2
rule : sin(#pi / 3) -> sqrt(3) / 2
rule : sin(#pi / 4) -> sqrt(2) / 2
rule : sin(#pi / 2) -> 1

rule : (a / b) * (c / d) -> (a * c) / (b * d)
rule : a / (a * b) -> 1 / b
rule : 1 / sqrt(a) -> sqrt(a) / a
rule : a / (b / c) -> (a * c) / b

rule : {a & b, b & a}
rule : {a | b, b | a}
rule : {(a & b) & c, a & (c & b)}
rule : {(a | b) | c, a | (c | b)}
rule : {a & (b | c), a & b | a & c}
rule : a & a -> a
rule : a | a -> a
rule : a | not(a) -> 1
rule : not(a) | a -> 1
rule : not(not(a)) -> a
rule : not(a) & a -> 0
rule : a & not(a) -> 0
rule : a & 0 -> 0
rule : 0 & a -> 0
rule : a | 0 -> a
rule : 0 | a -> a
rule : a & 1 -> a
rule : 1 & a -> a
rule : a | 1 -> 1
rule : 1 | a -> 1
rule : not(0) -> 1
rule : not(1) -> 0
rule : {not(a & b), not(a) | not(b)}
rule : {not(a | b), not(a) & not(b)}
rule : {a <= b, b >= a, a < b | a == b}
rule : a & (b | c) -> (a & b) | (a & c)
rule : a & (a | b) -> a
rule : a | (a & b) -> a

rule : not(a) | not(b) -> not(a & b)
rule : not(a) & not(b) -> not(a | b)
rule : (a | b) & (a | c) -> a | (b & c)
rule : (a & b) | (a & c) -> a & (b | c)

rule : diff(x^n, x) -> n * x^(n - 1)
rule : diff(sin(x), x) -> cos(x)
rule : diff(cos(x), x) -> -1*sin(x)
rule : diff(exp(x), x) -> exp(x)
rule : diff(sqrt(x), x) -> -1 / (2 * sqrt(x))
rule : diff(a * b, x) -> a * diff(b, x)
rule : diff(a + b, x) -> diff(a, x) + diff(b, x)
conditional rule : diff(_f(x), x) -> lim(h, 0, (_f(x+h)- _f(x))/h) IF IS_FUNCTION(_f)

rule : {a = b, b = a}
rule : {a = b = c, a = (b = c), a = c = b}
rule : {a = (b/c), (a * c) = b}
rule : {a = (b + c), (a - c) = b}

rule : {sum(n, 0, #infty, 1 / (2 ^ n)), 2}
rule : a^0 -> 1
rule : 1 * a -> a
rule : a * 1 -> a
rule : exp(0) -> 1

rule : (a / b) * b -> a

rule : sqrt(x^2) -> abs(x)

poisson(l, x) := (exp(0-l)) / (fac(x)) * l^x
bernoulli(n, p, x) := binom(n, x) * (p)^(x) * (1 - p)^(n-x)

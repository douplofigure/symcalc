\documentclass[a4paper]{article}

\usepackage{symcalc}
\usepackage [a4paper]{geometry}
\usepackage{fancyhdr}
\usepackage{hyperref}

%\pagestyle{fancy}

\begin{document}

\tableofcontents
\pagebreak

\section{Mathematical objects}

In order to solve a problem the program needs some information about the tools it is given. 
This information is provided as a list of operators, functions and rules that can be applied.

All of the items listed above can be declared in files or in the command-line interface.

\subsection{Operators}

Operators are the simplest elements that can be used to link two objects. They are analogous to the mathematical symbols of $+$ or $\leq$ for example.

Internally they are represented as (partial) functions: $\circ : \mathcal{O}^n \to \mathcal{O}$
where $\circ$ is the operator and $\mathcal{O}$ is the set of all possible expressions. Yet they do not need to have an implementation, as this is only required for numerical values to be computed.

\subsubsection{Properties}
Operators have some properties you will need to specify when creating a new one.

\begin{itemize}
	\item \textbf{a symbol} This is symbol by which the operator will be identified ``+'' as an example.
	\item \textbf{Number of operands} : The number of operands for the operator, in our example it would be 2 for ``+''.
	\item \textbf{Precedence}: The precedence of the operator, so the precedence of ``+'' should be lower than ``*''.
	\item \textbf{Visual complexity}: This is a number that indicates how important it is to remove this operator from the given expression.
	\item \textbf{Inline}: this is a boolean value indicating whether the operator can be used inline or not.
	\item \textbf{Implementation}: this is the name of a function implementing the functionality of the operator when computing a numerical result.
\end{itemize}

To declare an operator one must use the \textit{operator} function. This takes a list of the previous arguments where only the first 2 are required.
The default definition of the ``+'' operator is:
\begin{lstlisting}
	operator(+, 2, 1, 1, true, "add")
\end{lstlisting}
So it is an operator denoted by + taking 2 operands with precedence 1, complexity 1 that can be used inline and is implemented by a function called ``add''.

\begin{table}[H]

\begin{tabular}{c | c | c | c | c | c }

\textbf{Operator} & \textbf{op. count} & \textbf{prec.} & \textbf{compl.} & \textbf{inline} &	\textbf{expression} \\\hline
+ & 2 & 1 & 1 & yes & $a + b$\\
- & 2 & 1 & 1 & yes & $a - b$\\
* & 2 & 2 & 1 & yes & $a \times b$\\
/ & 2 & 2 & 1 & yes & $\frac{a}{b}$\\
\^{} & 2 & 3 & 1 & yes & $a^b$\\
\end{tabular}

\caption{Examples of operator declarations for standard operations.}

\end{table}

\subsection{Functions}

Functions are the equivalent of mathematical functions. They are elements $f : \mathcal{O}^n \to \mathcal{O}$ 
where $\mathcal{O}$ is the set of all expressions and $n \in \mathbb{N}$. Like operators functions do not need
an implementation, but unlike operators these implementations can be defined by other functions and operators.


Functions can be defined in several ways: by their expression, or via sets. Once defined a function can be used just like a non-inline operator.
\subsubsection{Definition by expression}
When defining a function by it's expression the function will always have an implementation. As an example here we define the function $f : \mathbb{R} \to \mathbb{R}$ as $f(x) = x^2 + 2x$ by writing:
\begin{lstlisting}
	f(x) := x^2 + 2 * x
\end{lstlisting}

\subsubsection{Definition via sets}
When defining a function via its sets, one is not obliged to present any implementation for the function. The definition of the function $g : \mathbb{R} \to \mathbb{R}, x \mapsto x^3 + 4$ becomes:
\begin{lstlisting}
	let: g : numset(R) -> numset(R) , x -> x^3 + 4
\end{lstlisting}
And the declaration of a function $h : \mathbb{N} \to \mathbb{N}$ with unknown implementation would be:
\begin{lstlisting}
	let: h : numset(N) -> numset(N)
\end{lstlisting}
This method for declaring functions is closer to the mathematical notation.

\subsubsection{Special values}
As functions can be defined recursively as is the case with the builtin function for $n!$:
\begin{lstlisting}
	fac(n) := n * fac(n-1)
\end{lstlisting}
a way is needed to provide special values for a function. So if a function has already been declared a value for
a special input can be defined:
\begin{lstlisting}
	fac(0) := 1
\end{lstlisting}
This system should not be used for defining special values like $sin(\frac{\pi}{4}) = \frac{\sqrt{2}}{2}$ as rules 
are faster for recognition of these patterns. This is only for the implementation.

\subsection{The set of expressions}

With operators and functions we can create the set of all correct expressions as follows:

Let $\Sigma = \{a,\dots, z, A, \dots, Z, \_, \backslash \}$ and $L \subseteq \Sigma^\ast$ be the language of all symbols. Then the set of all correct expressions $\mathcal{O}$ is given by
\begin{itemize}
\item $w \in \mathcal{O} , \forall w \in L$
\item $\left\{\#e, \#i, \#pi, \#infty\right\} \subset \mathcal{O}$
\item $d(x) \in \mathcal{O}$ where $d(x)$ is the decimal representation of $x\in\mathbb{R}$.
\item $u \circ v$ where $u, v \in \mathcal{O}$ and $\circ$ is an inline operator.
\item $\circ (u_1, \dots ,u_n)$ where $u_1 \dots u_n \in \mathcal{O}$ and $\circ$ is an operator.
\item $f(u_1, \dots ,u_n)$ where $u_1 \dots u_n \in \mathcal{O}$ and $\circ$ is a function.
\end{itemize}

\subsection{Rules}

Rules can be split up into two main types: Non-directional and directional.

\subsubsection{Non-directional rules}

Non-directional rules are finite sets of expressions that are all equivalent to one another. So such a rule can be defined by $R =\left \{ e_1, e_2, ..., e_n \right \}$ where $e_i \in \mathcal{O}$ and $e_i \equiv e_j$,
$i,j = 1,..., n$.

As all of the expressions in the set are equal the program will try to match all expressions to the current one when trying to simplify. This may take a long time. But non-directional rules can sometimes be used to express simple concepts much more effectively, for example comutativity can be expressed very effectively:
\begin{lstlisting}
	rule : {a + b , b + a}
\end{lstlisting}

\subsubsection{Directional rules}

Directional rules can only be applied in one direction. Therefor they can be seen as partial functions $r:\mathcal{O}
\dashrightarrow \mathcal{O}$. Such a rule can be declared as follows:
\begin{lstlisting}
	rule : sin(x)^2 + cos(x)^2 -> 1
\end{lstlisting}
This rule states that if the program sees an expression $\sin(x)^2 + \cos(x)^2$ it may replace it by $1$. These rules are much
faster when matching as only a single expression has to be checked for each rule. They are also useful when declaring special
values for functions (not for computation, but for simplification).

\subsubsection{Conditions}

Every rule can be made conditional. This allow for certain rules to only be applied in certain circumstances. One could imagine
a rule to allow for the simplification of $\sin(x)$ to $x$ for small values of $|x|$. This rule would be written as:
\begin{lstlisting}
	conditional rule : sin(x) -> x IF abs(x) < 0.01
\end{lstlisting}

The conditions used are expressions that will be evaluated every time the rule is used. They need to provide an implementation
as no implementation is seen as an unmet condition.

\section{Examples}

\subsection{Simplifying an expression}

Here we will look at how to try to simplify expressions.

\subsubsection{Simple expression}

For our first expression we will chose something simple: $2\sin(\frac{\pi}{3})$. This should not cause any problems as a rule
is provided for this case ($\sin(\frac{\pi}{3}) \to \frac{\sqrt{3}}{2}$).

So our input command is:
\begin{lstlisting}
simplify : 2 * sin(#pi/3)
\end{lstlisting}
Which yield as output:

\begin{minipage}{0.45\textwidth}
\begin{lstlisting}
(2 * sin((#pi / 3)))
(2 * (sqrt(3) / 2))
(sqrt(3) * (2 / 2))
(sqrt(3) * 1)
sqrt(3)
1.7320508075688774
\end{lstlisting}
\end{minipage}
\hfill
\begin{minipage}{0.45\textwidth}
\symcalc{simplify : 2 * sin(;pi / 3)}
\end{minipage}

\end{document}
